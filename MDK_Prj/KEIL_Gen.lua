workspace "Relay_13_2.0"
  configurations { "Debug" }
project"Relay_13_2.0"
  kind "ConsoleApp"
  language "C"
filter "configurations:Debug"
  sysincludedirs  {"$(VC_IncludePath)"}
  defines { "_IAR_", "__ICCARM__", "_Pragma(x)=", "__interrupt=", "STM32F10X_MD", "USE_STDPERIPH_DRIVER", "USE_FULL_ASSERT" }
  forceincludes { "" }
  includedirs { "../LX_Driver", "../LX_Driver/inc", "../Peripheral_Driver", "../LX_Driver/EasyLogger/inc", "../LX_CanDevice" }
  files { "../User/main.cpp", "../LX_Driver/str/bsp.cpp", "../LX_Driver/str/can.cpp", "../LX_Driver/str/gtime.cpp", "../LX_Driver/str/printf.cpp", "../LX_Driver/str/pwm.cpp", "../LX_Driver/str/stmflash.cpp", "../LX_Driver/str/uart.cpp", "../LX_Driver/LX_OS.cpp", "../Peripheral_Driver/device.cpp", "../Peripheral_Driver/relay.cpp", "../Peripheral_Driver/key.cpp", "../Peripheral_Driver/led.cpp", "../LX_Driver/EasyLogger/port/elog_port.cpp", "../LX_Driver/EasyLogger/src/elog.cpp", "../LX_Driver/EasyLogger/src/elog_utils.c", "../LX_CanDevice/LXCanDevice.cpp", "../LX_CanDevice/LXCanAddress.cpp", "../LX_CanDevice/LXCabDatabase.cpp", "../LX_CanDevice/LXCanAssocicationl.cpp", "../LX_CanDevice/LXCanObject.cpp", "../LX_CanDevice/LXCanPort.cpp", "../LX_CanDevice/LXCanTelegram.cpp" }
  vpaths {["USER"] = { "../User/main.cpp" } , ["LX_Drive"] = { "../LX_Driver/str/bsp.cpp" , "../LX_Driver/str/can.cpp" , "../LX_Driver/str/gtime.cpp" , "../LX_Driver/str/printf.cpp" , "../LX_Driver/str/pwm.cpp" , "../LX_Driver/str/stmflash.cpp" , "../LX_Driver/str/uart.cpp" , "../LX_Driver/LX_OS.cpp" } , ["Device"] = { "../Peripheral_Driver/device.cpp" , "../Peripheral_Driver/relay.cpp" , "../Peripheral_Driver/key.cpp" , "../Peripheral_Driver/led.cpp" } , ["EasyLog"] = { "../LX_Driver/EasyLogger/port/elog_port.cpp" , "../LX_Driver/EasyLogger/src/elog.cpp" , "../LX_Driver/EasyLogger/src/elog_utils.c" } , ["LX_CanDevice"] = { "../LX_CanDevice/LXCanDevice.cpp" , "../LX_CanDevice/LXCanAddress.cpp" , "../LX_CanDevice/LXCabDatabase.cpp" , "../LX_CanDevice/LXCanAssocicationl.cpp" , "../LX_CanDevice/LXCanObject.cpp" , "../LX_CanDevice/LXCanPort.cpp" , "../LX_CanDevice/LXCanTelegram.cpp" } , ["::CMSIS"] = { "" } , ["::Device"] = { "" } }
premake.override(premake.vstudio.vc2010, "includePath", function(base,cfg)
   local dirs = premake.vstudio.path(cfg, cfg.sysincludedirs)
    if #dirs > 0 then
    premake.vstudio.vc2010.element("IncludePath", nil, "%s", table.concat(dirs, ";"))
    end
end)