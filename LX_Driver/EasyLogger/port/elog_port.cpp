/*
 * This file is part of the EasyLogger Library.
 *
 * Copyright (c) 2015, Armink, <armink.ztl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * 'Software'), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Function: Portable interface for each platform.
 * Created on: 2015-04-28
 */
 
#include <elog.h>
#include "uart.h"
/**
 * EasyLogger port initialize
 *
 * @return result
 */
ElogErrCode elog_port_init(void) {
    ElogErrCode result = ELOG_NO_ERR;

    /* add your code here */
    
    return result;
}

/**
 * output log port interface
 *
 * @param log output of log
 * @param size log size
 */
void elog_port_output(const char *log, uint16_t size) {
    
    /* add your code here */
	#ifdef USART1_USE 
	uart1.printf_length(log,size);
	#endif
    //easyflash_output(log,size);
}

/**
 * output lock
 */
void elog_port_output_lock(void) {
    
    /* add your code here */
    //__disable_irq();
}

/**
 * output unlock
 */
void elog_port_output_unlock(void) {
    
    /* add your code here */
     //__enable_irq();
}

/**
 * get current time interface
 *
 * @return current time
 */
#define LOG_TIME_SIZE 10
const char *elog_port_get_time(void) {
	uint8_t i;
	static char buf[LOG_TIME_SIZE+2] = {0};
	long time = millis();
	for ( i=0;i<3;i++)
	{
		buf[LOG_TIME_SIZE-i] = (time%10)+'0';
		time /= 10;
	}
	buf[LOG_TIME_SIZE-i] = '.';
	for(i=4;i<LOG_TIME_SIZE+1;i++)
	{
		buf[LOG_TIME_SIZE-i] = (time%10)+'0';
		time /= 10;
	}
	buf[LOG_TIME_SIZE+1] = '\0';
	
    /* add your code here */
     return buf;
}

/**
 * get current process name interface
 *
 * @return current process name
 */
const char *elog_port_get_p_info(void) {
    
    /* add your code here */
    return "pid:1008";
}

/**
 * get current thread name interface
 *
 * @return current thread name
 */
const char *elog_port_get_t_info(void) {
    
    /* add your code here */
    return "tid:24";
}


/***********����*********/
//#include "LX_OS.h"
//#include "elog.h"
//
//#define log_a(...) elog_a("main.test.a", __VA_ARGS__)
//#define log_e(...) elog_e("main.test.e", __VA_ARGS__)
//#define log_w(...) elog_w("main.test.w", __VA_ARGS__)
//#define log_i(...) elog_i("main.test.i", __VA_ARGS__)
//#define log_d(...) elog_d("main.test.d", __VA_ARGS__)
//#define log_v(...) elog_v("main.test.v", __VA_ARGS__)
//
//GPIO led0(GPIOC,GPIO_Pin_13);
//CAN can1(CAN1,&PB8,&PB9);
//void fun()
//{
//	uart1.printf_length(uart1.receive_buf,UART_MAX_RECEVE_BUF-DMA_GetCurrDataCounter(DMA1_Channel5));
//}
//void can_fun()
//{
//	//can1.write(Rx0_Message.ExtId,Rx0_Message.Data,Rx0_Message.DLC);
//	uart1.printf_length((char*)Rx0_Message.Data,Rx0_Message.DLC);
//}
//void setup()
//{
//	LX_OS_init();
//	led0.mode(OUTPUT_PP);
//	uart1.begin(115200);
//	uart1.attach_rx_interrupt(fun);
//	can1.begin(BSP_CAN_125KBPS);
//	/*****************************/
//	elog_init();
//	/* set EasyLogger log format */
//	elog_set_fmt(ELOG_LVL_ASSERT, ELOG_FMT_ALL);
//	elog_set_fmt(ELOG_LVL_ERROR , ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
//	elog_set_fmt(ELOG_LVL_WARN  , ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
//	elog_set_fmt(ELOG_LVL_INFO  , ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
//	elog_set_fmt(ELOG_LVL_DEBUG , ELOG_FMT_ALL & ~(ELOG_FMT_FUNC | ELOG_FMT_T_INFO | ELOG_FMT_P_INFO));
//	elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~(ELOG_FMT_FUNC | ELOG_FMT_T_INFO | ELOG_FMT_P_INFO));
//	/* start EasyLogger */
//	elog_start();
//
//	/* dynamic set enable or disable for output logs (true or false) */
//	//    elog_set_output_enabled(false);
//	/* dynamic set output logs's level (from ELOG_LVL_ASSERT to ELOG_LVL_VERBOSE) */
//	    //elog_set_filter_lvl(ELOG_LVL_WARN);
//	/* dynamic set output logs's filter for tag */
//	//    elog_set_filter_tag("main.test.a");
//	/* dynamic set output logs's filter for keyword */
//	//    elog_set_filter_kw("Hello1");
//}
//
//int main()
//{
//	u16 i = 0;
//	u8 data[] = "1234";
//	u8 data11;
//	u16 data2;
//	setup();
//	uart1.printf("Hello World\n");
//	easyflash_printf("12345\n");
//	can1.write(0x00018b30,data,4);
//	can1.attach_interrupt_rx0(can_fun);
//	while (1)
//	{
//		float temp = 145;
//		log_a("Hello %f %d EasyLogger!",(float)0.009,(uint16_t)temp);
//		/*log_e("Hello EasyLogger!");
//		log_w("Hello EasyLogger!");
//		log_i("Hello EasyLogger!");
//		log_d("Hello EasyLogger!");
//		log_v("Hello EasyLogger!");*/
//		//IIC.write_byte(0x11,0,0xa5);
//		led0.toggle();
//		delay_ms(4000);
//	}
//}
