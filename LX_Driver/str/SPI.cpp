#include "SPI.h"
SOFTSPI::SOFTSPI(GPIO* sck,GPIO*miso,GPIO* mosi)
{
	this->sck_pin = sck;
	this->mosi_pin = mosi;
	this->miso_pin = miso;
};
void SOFTSPI::begin(SPI_CONFIG_TYPE *spi_config)
{
	sck_pin->mode(OUTPUT_PP);
	mosi_pin->mode(OUTPUT_PP);
	miso_pin->mode(INPUT_PD);
}
/////////////////////////////////////////////////////////////////////
//功    能：SPI读数据
//输    入： 无
//返    回:	 无
///////////////////////////////////////////////////////////////////// 
uint8_t SOFTSPI::read()
{
	unsigned char SPICount,i;
	unsigned char  SPIData;                  
	SPIData = 0;                    //下降沿有效 
	for (SPICount = 0; SPICount < 8; SPICount++)                  // Prepare to clock in the data to be read
	{
		SPIData <<=1;                                               // Rotate the data

		sck_pin->reset(); 		
		for(i=0;i<15;i++);                                       // Raise the clock to clock the data out of the MAX7456
		if(miso_pin->read())
		{
			SPIData|=0x01;
		}  
		sck_pin->set(); 	
		for(i=0;i<15;i++); 
		// Drop the clock ready for the next bit
	}                                                            // and loop back
	return (SPIData); 
}
/////////////////////////////////////////////////////////////////////
//功    能：SPI写数据
//输    入： 无
// 无返回值
///////////////////////////////////////////////////////////////////// 
int8_t SOFTSPI::write(uint8_t data)
{
	u8 count=0,i;     
	for(count=0;count<8;count++)  
	{ 	  
		if(data&0x80)
			mosi_pin->set();  
		else 
			mosi_pin->reset();   
		data<<=1;    
		sck_pin->reset();//上升沿有效 
		for(i=0;i<15;i++);	 
		sck_pin->set();
		for(i=0;i<15;i++);   
	} 
}
int8_t SOFTSPI::write(uint8_t* data,uint16_t length)
{
	return 0;
}