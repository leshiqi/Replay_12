#include "gtime.h"
typedef struct
{
	TIM_TypeDef* TIMx;
	uint32_t RCC_APB;
	uint16_t period;
	uint16_t prescaler;
	uint8_t IRQChannel;
	u8 PreemptionPriority;
	u8 SubPriority;
}TIM_def_;
const TIM_def_ tim_def[] = 
{
	{TIM1,RCC_APB2Periph_TIM1,1000,72,TIM1_UP_IRQn,1,1},
	{TIM2,RCC_APB1Periph_TIM2,1000,72,TIM2_IRQn   ,1,1},
	{TIM3,RCC_APB1Periph_TIM3,1000,72,TIM3_IRQn,1,1},
	{TIM4,RCC_APB1Periph_TIM4,1000,72,TIM4_IRQn,1,1}
#ifdef STM32F10X_HD
	,{TIM5,RCC_APB1Periph_TIM5,1000,72,TIM5_IRQn,1,1},
	{TIM6,RCC_APB1Periph_TIM6,1000,72,TIM6_IRQn,1,1},
	{TIM7,RCC_APB1Periph_TIM7,1000,72,TIM7_IRQn,1,1},
	{TIM8,RCC_APB2Periph_TIM8,1000,72,TIM8_UP_IRQn,1,1}
#endif/* STM32F10X_HD */ 
};
void (*tim_callback[8])();
TIM::TIM(TIM_TypeDef *TIMx)
{
	this->TIMx = TIMx;
}

void TIM::base_init(uint16_t period,uint16_t prescaler)
{
	 TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	

	 TIM_TimeBaseStructure.TIM_Period = period; //重载值
	 TIM_TimeBaseStructure.TIM_Prescaler =prescaler - 1;//分频系数
	 TIM_TimeBaseStructure.TIM_ClockDivision = 0; 
	 TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; /*向上计数*/
	 TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure); 
}
void TIM::set_reload(uint16_t auto_reload)
{

}
void TIM::clear_count()
{

}
void TIM::begin(uint32_t frq)
{
	NVIC_InitTypeDef NVIC_InitStructure; 
	u8 i;
	TIM_def_ tim_def_temp;
	for(i=0;i<8;i++)
	{
		if(TIMx == tim_def[i].TIMx)
		{
			tim_def_temp = tim_def[i];
			break;
		}
	}
	RCC_APB1PeriphClockCmd(tim_def_temp.RCC_APB, ENABLE); //时钟使能
	base_init(1000000/frq,tim_def_temp.prescaler);
	
	NVIC_InitStructure.NVIC_IRQChannel = tim_def_temp.IRQChannel;  //TIM3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = tim_def_temp.PreemptionPriority;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = tim_def_temp.SubPriority;  //从优先级3级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器
}
void TIM::attach_interrupt(Time_callback callback)
{
	u8 i;
	for(i=0;i<8;i++)
	{
		if(TIMx == tim_def[i].TIMx)
		{
			tim_callback[i] = callback;
			break;
		}
	}
}
void TIM::interrupt(FunctionalState enable)
{
	TIM_ITConfig(  //使能或者失能指定的TIM中断
		TIMx, //TIMx
		TIM_IT_Update  |  //TIM 中断源
		TIM_IT_Trigger,   //TIM 触发中断源 
		enable //使能
		);	
}
void TIM::start()
{
	TIM_Cmd(TIMx, ENABLE);  //使能TIMx外设
}
void TIM::stop()
{
	TIM_Cmd(TIMx, DISABLE);
}
void TIM::reset_frq(uint32_t frq)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	TIM_TimeBaseStructure.TIM_Period = 1000000/frq; //重载值
	TIM_TimeBaseStructure.TIM_Prescaler =72 - 1;//分频系数
	TIM_TimeBaseStructure.TIM_ClockDivision = 0; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; /*向上计数*/
	TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);
}





#ifdef __cplusplus
extern "C" {
#endif
void TIM1_IRQHandler(void);
void TIM2_IRQHandler(void);
void TIM3_IRQHandler(void);
void TIM4_IRQHandler(void);
#ifdef STM32F10X_HD
void TIM5_IRQHandler(void);
void TIM6_IRQHandler(void);
void TIM7_IRQHandler(void);
void TIM8_IRQHandler(void);
#endif // STM32F10X_HD
#ifdef __cplusplus
}
#endif
void TIM1_IRQHandler(void)   //TIM1中断
{
	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIM1, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
		tim_callback[0]();
	}
}
void TIM2_IRQHandler(void)   //TIM2中断
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
		tim_callback[1]();
	}
}
void TIM3_IRQHandler(void)   //TIM3中断
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
		tim_callback[2]();
	}
}
void TIM4_IRQHandler(void)   //TIM4中断
{
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
		tim_callback[3]();
	}
}
#ifdef STM32F10X_HD
void TIM5_IRQHandler(void)   //TIM5中断
{
	if (TIM_GetITStatus(TIM5, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIM5, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
		tim_callback[4]();
	}
}
void TIM6_IRQHandler(void)   //TIM6中断
{
	if (TIM_GetITStatus(TIM6, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIM6, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
		tim_callback[5]();
	}
}
void TIM7_IRQHandler(void)   //TIM7中断
{
	if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIM7, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
		tim_callback[6]();
	}
}
void TIM8_IRQHandler(void)   //TIM8中断
{
	if (TIM_GetITStatus(TIM8, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIM8, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
		tim_callback[7]();
	}
}
#endif
