#include "pwm.h"
PWM::PWM(GPIO *pwm_pin)
{
	this->pwm_pin = pwm_pin;
	init_info(pwm_pin);
	while(1);
}
void PWM::begin(uint32_t frq,uint16_t duty,u8 polarity)
{
	RCC_APB1PeriphClockCmd(rcc, ENABLE);
	oc_polarity = polarity;
	this->duty = duty;
	period = (1000000/frq);
	pwm_pin->mode(AF_PP);
	base_init( period,72);
	TIM_ARRPreloadConfig(TIMx, ENABLE); //使能TIMx在ARR上的预装载寄存器
	TIM_Cmd(TIMx, ENABLE);  //使能TIMx外设
}
void PWM::set_duty(uint16_t duty)
{
	this->duty = duty;
	base_init( period,72);
}
void PWM::set_frq(uint32_t frq)
{
	period = (1000000/frq);
	base_init( period,72);
}
void PWM::init_info(GPIO *pwm_pin)
{
	/*if(GPIOA == pwm_pin->port)
	{
		switch (pwm_pin->pin)
		{
		case GPIO_Pin_0:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM2;
			ch = 1;
			break;
		case GPIO_Pin_1:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM2;
			ch = 2;
			break;
		case GPIO_Pin_2:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM2;
			ch = 3;
			break;
		case GPIO_Pin_3:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM2;
			ch = 4;
			break;
		case GPIO_Pin_6:
			TIMx = TIM3;
			rcc = RCC_APB1Periph_TIM3;
			ch = 1;
			break;
		case GPIO_Pin_7:
			TIMx = TIM3;
			rcc = RCC_APB1Periph_TIM3;
			ch = 2;
			break;
		case GPIO_Pin_15:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM2;
			ch = 1;
			break;
		default:
			break;
		}
	}*/
	/*else if(GPIOB == pwm_pin->port)
	{
		switch (pwm_pin->pin)
		{
		case GPIO_Pin_0:
			TIMx = TIM3;
			rcc = RCC_APB1Periph_TIM3;
			ch = 3;
			break;
		case GPIO_Pin_1:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM3;
			ch = 4;
			break;
		case GPIO_Pin_3:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM2;
			ch = 2;
			break;
		case GPIO_Pin_10:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM2;
			ch = 3;
			break;
		case GPIO_Pin_11:
			TIMx = TIM2;
			rcc = RCC_APB1Periph_TIM2;
			ch = 4;
			break;
		case GPIO_Pin_6:
			TIMx = TIM4;
			rcc = RCC_APB1Periph_TIM4;
			ch = 1;
			break;
		case GPIO_Pin_7:
			TIMx = TIM4;
			rcc = RCC_APB1Periph_TIM4;
			ch = 2;
			break;
		case GPIO_Pin_8:
			TIMx = TIM4;
			rcc = RCC_APB1Periph_TIM4;
			ch = 3;
			break;
		case GPIO_Pin_9:
			TIMx = TIM4;
			rcc = RCC_APB1Periph_TIM4;
			ch = 4;
			break;
		default:
			break;
		}
	}*/
}
void PWM::base_init(uint16_t Period,uint16_t Prescaler)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	
	TIM_TimeBaseStructure.TIM_Period = period - 1; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	 80K
	TIM_TimeBaseStructure.TIM_Prescaler =Prescaler-1; //设置用来作为TIMx时钟频率除数的预分频值  不分频
	TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位


	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = (period*duty)/1000 - 1; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = oc_polarity; //输出极性:TIM输出比较极性高
	switch (ch)
	{
	case 1:
		TIM_OC1Init(TIMx, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
		TIM_OC1PreloadConfig(TIMx, TIM_OCPreload_Enable);  //使能TIMx在CCR2上的预装载寄存器
		break;
	case 2:
		TIM_OC2Init(TIMx, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
		TIM_OC2PreloadConfig(TIMx, TIM_OCPreload_Enable);  //使能TIMx在CCR2上的预装载寄存器
		break;
	case 3:
		TIM_OC3Init(TIMx, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
		TIM_OC3PreloadConfig(TIMx, TIM_OCPreload_Enable);  //使能TIMx在CCR2上的预装载寄存器
		break;
	case 4:
		TIM_OC4Init(TIMx, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
		TIM_OC4PreloadConfig(TIMx, TIM_OCPreload_Enable);  //使能TIMx在CCR2上的预装载寄存器
		break;
	default:
		break;
	}
}
