#include "uart.h"
#include "printf.h"
static uint8_t  USART1_TX_Finish=1;
static uint8_t  USART2_TX_Finish=1;
static uint8_t  USART3_TX_Finish=1;

static uint8_t  USART1_RX_Finish = 0;
static uint8_t  USART2_RX_Finish = 0;
static uint8_t  USART3_RX_Finish = 0;

char send_temp[UART_MAX_SEND_BUF] = {0};
#ifdef USART1_USE
USART uart1(USART1,&PA9,&PA10);
#endif // USART1_USE
#ifdef USART2_USE
USART uart2(USART2,&PA2,&PA3);
#endif // usart2
#ifdef USART3_USE
USART uart3(USART3,&PB10,&PB11);
#endif // USART3_USE

void (*uart_hard[5])();
//uint8_t (*dma_tx_uart[5])();
//char  USART::send_buf[UART_MAX_SEND_BUF] = {0};
USART::USART(USART_TypeDef *USARTx,GPIO *tx_pin,GPIO *rx_pin)
{
//	GPIO_InitTypeDef GPIO_InitStructure;
	this->_USARTx = USARTx;
	this->tx_pin = tx_pin;
	this->rx_pin = rx_pin;
	init_info(USARTx);
}
void USART::begin(uint32_t baud_rate)
{
	USART_InitTypeDef USART_InitStructure;
//	NVIC_InitTypeDef NVIC_InitStructure;
	rx_pin->mode(INPUT);
	tx_pin->mode(AF_PP);
	if(USART1 == _USARTx)
		RCC_APB2PeriphClockCmd(rcc_uart, ENABLE);
	else
		RCC_APB1PeriphClockCmd(rcc_uart, ENABLE);
	USART_InitStructure.USART_BaudRate = baud_rate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(_USARTx, &USART_InitStructure);
	USART_Cmd(_USARTx, ENABLE); 
	//空闲中断
	USART_ITConfig(_USARTx, USART_IT_IDLE , ENABLE);
	///* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
 // 	如下语句解决第1个字节无法正确发送出去的问题 */
  	USART_ClearFlag(_USARTx, USART_FLAG_TC);     /* 清发送外城标志，Transmission Complete flag */
//	uart_dma_init();
	tx_node_init();
	/*dma_tx_uart = &tx_send_fun;*/
}
void USART::begin(uint32_t baud_rate,uint8_t data_bit,uint8_t parity,uint8_t stop_bit)
{
	USART_InitTypeDef USART_InitStructure;
	/* 第4步：配置USART参数*/
	RCC_APB2PeriphClockCmd(rcc_uart, ENABLE);
	USART_InitStructure.USART_BaudRate = baud_rate;
	USART_InitStructure.USART_WordLength =  data_bit;
	USART_InitStructure.USART_StopBits =stop_bit;
	USART_InitStructure.USART_Parity = parity;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(_USARTx, &USART_InitStructure);
	//空闲中断
	USART_ITConfig(_USARTx, USART_IT_IDLE , ENABLE);
	/* 第5步：使能 USART， 配置完毕 */
  	 
	/* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
  	如下语句解决第1个字节无法正确发送出去的问题 */
  	USART_ClearFlag(_USARTx, USART_FLAG_TC);     /* 清发送外城标志，Transmission Complete flag */
	USART_Cmd(_USARTx, ENABLE); 
	uart_dma_init();
}
int USART::put_char(uint16_t ch)
{ 
	while((_USARTx->SR&0X40)==0);//循环发送,直到发送完毕  
	_USARTx->DR = (uint8_t) ch;	
	return 0;
}
void USART::put_string(const char *str)
{
#if !USE_DMA
	while (0 != *str)
	{
		put_char(*str++);
	}
#else
	uint16_t n = 0;
	while (0 != str[n])
	{
		n++;
	}
	printf_length(str,n+1);
#endif

}
void USART::printf_length(const char*str,uint16_t n)
{
#if !USE_DMA
	
	 while(n--)
	{
	put_char(*str++);
	}
#else
	uint16_t i;
	/********************等待缓冲区可以装载新数据***************************************/
	uint32_t temp = 0xffff;
	while(((n+tx_block.num) > UART_MAX_SEND_BUF)&&(temp--))
	{
		;
	}
	/********************************************************************/
	for(i=0;i<n;i++)
	{
		send_buf[(tx_block.end+i)%UART_MAX_SEND_BUF]=str[i];
	}
	tx_block.end += n;
	tx_block.end %= UART_MAX_SEND_BUF;
	tx_block.num += n;
/***************删除字符串后面的 '\0' ,防止连续发送上位机不显示****************/
	if((0 == send_buf[ tx_block.end - 1]))
	{
	if(tx_block.end >0)
	tx_block.end --;
	else
	tx_block.end = 0;
	tx_block.num--;
	}
/*****************************************************/
	if(1 == *tx_finish)
	{
		__set_PRIMASK(1);
		DMA_Cmd(dma_channel_tx, DISABLE); //
		if((tx_block.sta + tx_block.num) <= UART_MAX_SEND_BUF)
		{
			tx_block.sned_len = tx_block.num;
			dma_channel_tx->CNDTR = tx_block.sned_len; //
			dma_channel_tx->CMAR = (uint32_t)(&send_buf[tx_block.sta]);
			/*tx_block.sta += tx_block.num;
			tx_block.num = 0;*/
		}
		else
		{
			tx_block.sned_len = UART_MAX_SEND_BUF-tx_block.sta;
			dma_channel_tx->CNDTR = tx_block.sned_len; //
			dma_channel_tx->CMAR = (uint32_t)(&send_buf[tx_block.sta]);
			/*tx_block.num = (tx_block.sta + tx_block.num) - UART_MAX_SEND_BUF;
			tx_block.sta = 0;*/
		}
		*tx_finish = 0;
		DMA_Cmd(dma_channel_tx, ENABLE);	
		__set_PRIMASK(0);
	}	
#endif
}
void USART::printf(const char *fmt,...)
{
	va_list args;
	va_start(args, fmt);
	vsprintf(send_temp, fmt, args);
	va_end(args);
	put_string(send_temp);
}
void USART::uart_dma_init()
{
	NVIC_InitTypeDef NVIC_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;
	/* DMA clock enable */
	/*UATR1_DMA*/
	 NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); 
	NVIC_InitStructure.NVIC_IRQChannel = irq_uart;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = uart_rx_PreemptionPriority;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = uart_rx_SubPriority;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//Enable DMA Channel4 Interrupt 
	NVIC_InitStructure.NVIC_IRQChannel = irq_dma_tx;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = dma_tx_PreemptionPriority;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = dma_tx_SubPriority;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	/*Enable DMA Channel5 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = irq_dma_rx;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = dma_rx_PreemptionPriority;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = dma_rx_SubPriority;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	DMA_DeInit(dma_channel_tx);  
	DMA_InitStructure.DMA_PeripheralBaseAddr =(uint32_t)&_USARTx->DR;//外设地址  
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)send_buf;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;//外设作为目的地址  
	DMA_InitStructure.DMA_BufferSize = 0;//传输大小  
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;//外设递增模式禁止 
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;//内存地址自增  
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;//传输方式：字节   DMA_PeripheralDataSize_Word;    //字（32位）  
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;//内存存储方式：字节  DMA_MemoryDataSize_Word;  
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;//DMA_Mode_Normal 正常模式，只传送一次;  DMA_Mode_Circular:循环模式，不停的传送;  
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(dma_channel_tx, &DMA_InitStructure);

	DMA_ITConfig(dma_channel_tx, DMA_IT_TC, ENABLE);
	DMA_ITConfig(dma_channel_tx, DMA_IT_TE, ENABLE);
	/* Enable USART1 DMA TX request */
	USART_DMACmd(_USARTx, USART_DMAReq_Tx, ENABLE);
	DMA_Cmd(dma_channel_tx, DISABLE);
	/*UATR2_DMA*/
	/* DMA1 Channel5 (triggered by USART1 Rx event) Config */
	DMA_DeInit(dma_channel_rx);  
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&_USARTx->DR;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)receive_buf;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; //外设作为DMA的源端  
	DMA_InitStructure.DMA_BufferSize = UART_MAX_RECEVE_BUF;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(dma_channel_rx, &DMA_InitStructure);

	DMA_ITConfig(dma_channel_rx, DMA_IT_TC, ENABLE);
	DMA_ITConfig(dma_channel_rx, DMA_IT_TE, ENABLE);

	/* Enable USART1 DMA RX request */
	USART_DMACmd(_USARTx, USART_DMAReq_Rx, ENABLE);

	DMA_Cmd(dma_channel_rx, ENABLE);
}
void USART::attach_rx_interrupt(Uart_callback_type fun)
{
	uart_hard[uart_num] = fun;
}
void USART::init_info(USART_TypeDef *USARTx)
{
	if(USART1 == _USARTx)
	{
		rcc_uart = RCC_APB2Periph_USART1;
		irq_uart = USART1_IRQn;
		irq_dma_tx = DMA1_Channel4_IRQn;
		irq_dma_rx = DMA1_Channel5_IRQn;
		dma_channel_tx = DMA1_Channel4;
		dma_channel_rx = DMA1_Channel5;

		tx_finish = &USART1_TX_Finish;
		rx_finish = &USART1_RX_Finish;

		uart_rx_PreemptionPriority = 1;
		uart_rx_SubPriority = 1;
		dma_rx_PreemptionPriority = 2;
		dma_rx_SubPriority = 0;
		dma_tx_PreemptionPriority = 2;
		dma_tx_SubPriority = 1;

		uart_num = 0;
	}
	else if(USART2 == _USARTx)
	{
		rcc_uart = RCC_APB1Periph_USART2;
		irq_uart = USART2_IRQn;
		irq_dma_tx = DMA1_Channel7_IRQn;
		irq_dma_rx = DMA1_Channel6_IRQn;
		dma_channel_tx = DMA1_Channel7;
		dma_channel_rx = DMA1_Channel6;

		tx_finish = &USART2_TX_Finish;
		rx_finish = &USART2_RX_Finish;

		uart_rx_PreemptionPriority = 3;
		uart_rx_SubPriority = 0;
		dma_rx_PreemptionPriority = 3;
		dma_rx_SubPriority = 1;
		dma_tx_PreemptionPriority = 4;
		dma_tx_SubPriority = 0;
		uart_num = 1;
	}
	else if(USART3 == _USARTx)
	{
		rcc_uart = RCC_APB1Periph_USART3;
		irq_uart = USART3_IRQn;
		irq_dma_tx = DMA1_Channel2_IRQn;
		irq_dma_rx = DMA1_Channel3_IRQn;
		dma_channel_tx = DMA1_Channel2;
		dma_channel_rx = DMA1_Channel3;

		tx_finish = &USART3_TX_Finish;
		rx_finish = &USART3_RX_Finish;

		uart_rx_PreemptionPriority = 4;
		uart_rx_SubPriority = 1;
		dma_rx_PreemptionPriority = 5;
		dma_rx_SubPriority = 0;
		dma_tx_PreemptionPriority = 5;
		dma_tx_SubPriority = 1;

		uart_num = 2;
	}
#ifdef STM32F10X_HD
	else if(USART4 == _USARTx)
	{

	}
	else if(USART5 == _USARTx)
	{

	}
#endif
}

void USART::tx_node_init()
{
	tx_block.sta = 0;
	tx_block.end = 0;
	tx_block.num = 0;
}
uint8_t USART::tx_send_fun()
{
	tx_block.num -= tx_block.sned_len;
	if(tx_block.num == NULL)
	{
		tx_block.sta = 0;
		tx_block.end = 0;
		return 1;
	}
	else
	{
		tx_block.sta += tx_block.sned_len;
		tx_block.sta %= UART_MAX_SEND_BUF;
	}
	/*连续发字符串时添加隔离*/
	//delay_ms(8);
	__set_PRIMASK(1);
	if((tx_block.sta + tx_block.num) <= UART_MAX_SEND_BUF)
	{
		tx_block.sned_len = tx_block.num;
		dma_channel_tx->CNDTR = tx_block.sned_len; //
		dma_channel_tx->CMAR = (uint32_t)(&send_buf[tx_block.sta]);
		/*tx_block.sta += tx_block.num;
		tx_block.num = 0;*/
	}
	else
	{
		tx_block.sned_len = UART_MAX_SEND_BUF-tx_block.sta;
		dma_channel_tx->CNDTR = tx_block.sned_len;//
		dma_channel_tx->CMAR = (uint32_t)(&send_buf[tx_block.sta]);
		/*tx_block.sta = 0;
		tx_block.num = (tx_block.sta + tx_block.num) - UART_MAX_SEND_BUF;*/
	}
	DMA_Cmd(dma_channel_tx, ENABLE);	
	__set_PRIMASK(0);
	return 0;
}

#ifdef __cplusplus
extern "C" {
#endif
	void USART1_IRQHandler(void);
	void DMA1_Channel4_IRQHandler(void);
	void DMA1_Channel5_IRQHandler(void);
	void USART2_IRQHandler(void);
	void DMA1_Channel6_IRQHandler(void);
	void DMA1_Channel7_IRQHandler(void);
	void USART3_IRQHandler(void);
	void DMA1_Channel2_IRQHandler(void);
	void DMA1_Channel3_IRQHandler(void);

#ifdef __cplusplus
}
#endif
#ifdef USART1_USE
void USART1_IRQHandler(void)
{
	u16 DATA_LEN;
	u16 i;
	if(USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)
	{
		DMA_Cmd(DMA1_Channel5, DISABLE);//关闭DMA,防止处理其间有数据
		DATA_LEN=UART_MAX_RECEVE_BUF-DMA_GetCurrDataCounter(DMA1_Channel5); 
		if(DATA_LEN > 0)
		{			
			while(USART1_TX_Finish==0)
			{
				;
			}
			uart_hard[0]();
		}
		DMA_Cmd(DMA1_Channel5, DISABLE);//关闭DMA,防止处理其间有数据
		DMA_ClearFlag(DMA1_FLAG_GL5 | DMA1_FLAG_TC5 | DMA1_FLAG_TE5 | DMA1_FLAG_HT5);//清标志
		DMA1_Channel5->CNDTR = UART_MAX_RECEVE_BUF;//重装填
		DMA_Cmd(DMA1_Channel5, ENABLE);//处理完,重开DMA
		 //读SR后读DR清除Idle
		 i = USART2->SR;
		 i = USART2->DR;
		i = i;
	}
	if(USART_GetITStatus(USART1, USART_IT_PE | USART_IT_FE | USART_IT_NE) != RESET)//出错
	{
		USART_ClearITPendingBit(USART1, USART_IT_PE | USART_IT_FE | USART_IT_NE);
	}
	USART_ClearITPendingBit(USART1, USART_IT_TC);
	USART_ClearITPendingBit(USART1, USART_IT_IDLE);
}
void DMA1_Channel5_IRQHandler(void)
{
	DMA_ClearITPendingBit(DMA1_IT_TC5);
	DMA_ClearITPendingBit(DMA1_IT_TE5);
	DMA_Cmd(DMA1_Channel5, DISABLE);//关闭DMA,防止处理其间有数据
	DMA1_Channel5->CNDTR = UART_MAX_RECEVE_BUF;//重装填
	DMA_Cmd(DMA1_Channel5, ENABLE);//处理完,重开DMA
}
//USART1使用DMA发数据中断服务程序
 void DMA1_Channel4_IRQHandler(void)
{
	DMA_ClearITPendingBit(DMA1_IT_TC4);
	DMA_ClearITPendingBit(DMA1_IT_TE4);
	DMA_Cmd(DMA1_Channel4, DISABLE);//关闭DMA
	if(uart1.tx_send_fun())
		USART1_TX_Finish = 1;//置DMA传输完成
}
 #endif // USART1_USE

#ifdef USART2_USE

 void USART2_IRQHandler(void)
 {
	 u16 DATA_LEN;
	 u16 i;
	 if(USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)
	 {
		 DMA_Cmd(DMA1_Channel6, DISABLE);//关闭DMA,防止处理其间有数据
		 //USART_RX_STA = USART1->SR;//先读SR，然后读DR才能清除
		 //USART_RX_STA = USART1->DR;
		 DATA_LEN=UART_MAX_RECEVE_BUF-DMA_GetCurrDataCounter(DMA1_Channel6); 
		 if(DATA_LEN > 0)
		 {			
			 while(USART2_TX_Finish==0)
			 {
				 ;
			 }	
			 uart_hard[1]();
		}
		 DMA_Cmd(DMA1_Channel6, DISABLE);//关闭DMA,防止处理其间有数据
		 DMA_ClearFlag(DMA1_FLAG_GL6 | DMA1_FLAG_TC6 | DMA1_FLAG_TE6 | DMA1_FLAG_HT6);//清标志
		 DMA1_Channel6->CNDTR = UART_MAX_RECEVE_BUF;//重装填
		 DMA_Cmd(DMA1_Channel6, ENABLE);//处理完,重开DMA
		 //读SR后读DR清除Idle
		 i = USART2->SR;
		 i = USART2->DR;
	 }
	 if(USART_GetITStatus(USART2, USART_IT_PE | USART_IT_FE | USART_IT_NE) != RESET)//出错
	 {
		 USART_ClearITPendingBit(USART2, USART_IT_PE | USART_IT_FE | USART_IT_NE);
	 }
	 USART_ClearITPendingBit(USART2, USART_IT_TC);
	 USART_ClearITPendingBit(USART2, USART_IT_IDLE);
 }
 void DMA1_Channel6_IRQHandler(void)
 {
	 DMA_ClearITPendingBit(DMA1_IT_TC6);
	 DMA_ClearITPendingBit(DMA1_IT_TE6);
	 DMA_Cmd(DMA1_Channel6, DISABLE);//关闭DMA,防止处理其间有数据
	 DMA1_Channel6->CNDTR = UART_MAX_RECEVE_BUF;//重装填
	 DMA_Cmd(DMA1_Channel6, ENABLE);//处理完,重开DMA
 }
 //USART2使用DMA发数据中断服务程序
 void DMA1_Channel7_IRQHandler(void)
 {
	 DMA_ClearITPendingBit(DMA1_IT_TC7);
	 DMA_ClearITPendingBit(DMA1_IT_TE7);
	 DMA_Cmd(DMA1_Channel7, DISABLE);//关闭DMA
	 USART2_TX_Finish=1;//置DMA传输完成
 }

#endif // USART2_USE

#ifdef USART3_USE


 void USART3_IRQHandler(void)
 {
	 u16 DATA_LEN;
	 u16 i;
	 if(USART_GetITStatus(USART3, USART_IT_IDLE) != RESET)
	 {
		 DMA_Cmd(DMA1_Channel3, DISABLE);//关闭DMA,防止处理其间有数据
		 //USART_RX_STA = USART1->SR;//先读SR，然后读DR才能清除
		 //USART_RX_STA = USART1->DR;
		 DATA_LEN=UART_MAX_RECEVE_BUF-DMA_GetCurrDataCounter(DMA1_Channel3); 
		 if(DATA_LEN > 0)
		 {			
			 while(USART3_TX_Finish==0)
			 {
				 ;
			 }	
			 uart_hard[2]();
		 }
		 DMA_Cmd(DMA1_Channel3, DISABLE);//关闭DMA,防止处理其间有数据
		 DMA_ClearFlag(DMA1_FLAG_GL3 | DMA1_FLAG_TC3 | DMA1_FLAG_TE3 | DMA1_FLAG_HT3);//清标志
		 DMA1_Channel3->CNDTR = UART_MAX_RECEVE_BUF;//重装填
		 DMA_Cmd(DMA1_Channel3, ENABLE);//处理完,重开DMA
		 //读SR后读DR清除Idle
		 i = USART3->SR;
		 i = USART3->DR;
	 }
	 if(USART_GetITStatus(USART3, USART_IT_PE | USART_IT_FE | USART_IT_NE) != RESET)//出错
	 {
		 USART_ClearITPendingBit(USART3, USART_IT_PE | USART_IT_FE | USART_IT_NE);
	 }
	 USART_ClearITPendingBit(USART3, USART_IT_TC);
	 USART_ClearITPendingBit(USART3, USART_IT_IDLE);
 }
 void DMA1_Channel3_IRQHandler(void)
 {
	 DMA_ClearITPendingBit(DMA1_IT_TC3);
	 DMA_ClearITPendingBit(DMA1_IT_TE3);
	 DMA_Cmd(DMA1_Channel3, DISABLE);//关闭DMA,防止处理其间有数据
	 DMA1_Channel3->CNDTR = UART_MAX_RECEVE_BUF;//重装填
	 DMA_Cmd(DMA1_Channel3, ENABLE);//处理完,重开DMA
 }
 //USART2使用DMA发数据中断服务程序
 void DMA1_Channel2_IRQHandler(void)
 {
	 DMA_ClearITPendingBit(DMA1_IT_TC2);
	 DMA_ClearITPendingBit(DMA1_IT_TE2);
	 DMA_Cmd(DMA1_Channel2, DISABLE);//关闭DMA
	 USART3_TX_Finish=1;//置DMA传输完成
 }



#endif // USART3_USE

#ifdef  DEBUG
 void easyflash_output(const char *fmt,uint16_t size)
 {
	 
	uart1.printf_length(fmt,size);
 }
 void easyflash_printf(const char *fmt,...)
 {
	 va_list args;
	 va_start(args, fmt);
	 vsprintf(send_temp, fmt, args);
	 va_end(args);
	 uart1.put_string(send_temp);
 }
 //调试信息输出函数
 void Debug(const char *fmt,...)
 {
	 va_list args;
	 va_start(args, fmt);
	 vsprintf(send_temp, fmt, args);
	 va_end(args);
	 uart1.put_string(send_temp);
 }
#endif
