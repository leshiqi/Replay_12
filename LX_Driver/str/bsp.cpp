#include "bsp.h"
#include "uart.h"
#define  varName(x) #x 
void gpio_error(GPIO_TypeDef* port,uint16_t pin);
uint16_t GPIO::Port_busy_flag[] = {0};

GPIO::GPIO(GPIO_TypeDef* port,uint16_t pin)
{
	uint32_t temp;
	this->port = port;
	this->pin = pin;
	temp = (((uint32_t)port - APB2PERIPH_BASE)/0x400) - 2;
	this->id = (PIN_ID_t)(temp*16);
	for(uint8_t i = 0 ; i<16; i++)
	{
		if((0x0001 << i) == pin)
			this->id =(PIN_ID_t)((uint8_t)(id) | i);
	}
}
void GPIO::mode(PIN_MODE mode)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	gpio_check(port,pin);
	GPIO_InitStructure.GPIO_Mode = (GPIOMode_TypeDef)mode;
	GPIO_InitStructure.GPIO_Pin = pin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(port, &GPIO_InitStructure);
	pin_mode = mode;
}
void GPIO::set()
{
	GPIO_SetBits(port,pin);
	//port->ODR |= (0x0001 << pin);
}
void GPIO::reset()
{
	GPIO_ResetBits(port,pin);
	//port->ODR |= (0x0001 << pin);
}
void GPIO::write(uint8_t val)
{
	if(val)
		GPIO_SetBits(port,pin);
	else
		GPIO_ResetBits(port,pin);
}
void GPIO::toggle()
{
	port->ODR = port->ODR^pin;
}
uint8_t GPIO::read()
{
	if((OUTPUT_PP == pin_mode)||(OUTPUT_OD == pin_mode))
		return GPIO_ReadOutputDataBit(port,pin);
	else if((INPUT_PD == pin_mode)||(INPUT_PU == pin_mode))
		return GPIO_ReadInputDataBit(port,pin);
	return 0;
}
void GPIO::gpio_check(GPIO_TypeDef* port,uint16_t pin)
{

	uint16_t* flag;
	flag = &Port_busy_flag[(id/16)];
	
	if((*flag) & pin)
	{
		gpio_error(port,pin);
	}
	*flag |= pin;
		
}
void gpio_error(GPIO_TypeDef* port,uint16_t pin)
{
	switch((uint32_t)(port))
	{
	 case (uint32_t)GPIOA_BASE:
		switch (pin)
		{
		case GPIO_Pin_0: for(;;); /*PA0重复使用*/
		case GPIO_Pin_1: for(;;); /*PA1重复使用*/
		case GPIO_Pin_2: for(;;); /*PA2重复使用*/
		case GPIO_Pin_3: for(;;); /*PA3重复使用*/
		case GPIO_Pin_4: for(;;); /*PA4重复使用*/
		case GPIO_Pin_5: for(;;); /*PA5重复使用*/
		case GPIO_Pin_6: for(;;); /*PA6重复使用*/
		case GPIO_Pin_7: for(;;); /*PA7重复使用*/
		case GPIO_Pin_8: for(;;); /*PA8重复使用*/
		case GPIO_Pin_9: for(;;); /*PA9重复使用*/
		case GPIO_Pin_10: for(;;); /*PA10重复使用*/
		case GPIO_Pin_11: for(;;); /*PA11重复使用*/
		case GPIO_Pin_12: for(;;); /*PA12重复使用*/
		case GPIO_Pin_13: for(;;); /*PA13重复使用*/
		case GPIO_Pin_14: for(;;); /*PA14重复使用*/
		case GPIO_Pin_15: for(;;); /*PA15重复使用*/
		default:
			for(;;); ;/*pin参数不合法*/
		}
	case (uint32_t)GPIOB_BASE:
		switch (pin)
		{
		case GPIO_Pin_0: for(;;);/*PB0重复使用*/
		case GPIO_Pin_1: for(;;);/*PB1重复使用*/
		case GPIO_Pin_2: for(;;); /*PB2重复使用*/
		case GPIO_Pin_3: for(;;); /*PB3重复使用*/
		case GPIO_Pin_4: for(;;); /*PB4重复使用*/
		case GPIO_Pin_5: for(;;); /*PB5重复使用*/
		case GPIO_Pin_6: for(;;); /*PB6重复使用*/
		case GPIO_Pin_7: for(;;); /*PB7重复使用*/
		case GPIO_Pin_8: for(;;); /*PB8重复使用*/
		case GPIO_Pin_9: for(;;); /*PB9重复使用*/
		case GPIO_Pin_10: for(;;);/*PB10重复使用*/
		case GPIO_Pin_11: for(;;); /*PB11重复使用*/
		case GPIO_Pin_12: for(;;); /*PB12重复使用*/
		case GPIO_Pin_13: for(;;);/*PB13重复使用*/
		case GPIO_Pin_14: for(;;); /*PB14重复使用*/
		case GPIO_Pin_15: for(;;); /*PB15重复使用*/
		default:
			for(;;); ;/*pin参数不合法*/
		}
	case (uint32_t)GPIOC_BASE:
		switch (pin)
		{
		case GPIO_Pin_0: for(;;); /*PC0重复使用*/
		case GPIO_Pin_1: for(;;); /*PC1重复使用*/
		case GPIO_Pin_2: for(;;); /*PC2重复使用*/
		case GPIO_Pin_3: for(;;);/*PC3重复使用*/
		case GPIO_Pin_4: for(;;); /*PC4重复使用*/
		case GPIO_Pin_5: for(;;); /*PC5重复使用*/
		case GPIO_Pin_6: for(;;); /*PC6重复使用*/
		case GPIO_Pin_7: for(;;); /*PC7重复使用*/
		case GPIO_Pin_8: for(;;); /*PC8重复使用*/
		case GPIO_Pin_9: for(;;); /*PC9重复使用*/
		case GPIO_Pin_10: for(;;); /*PC10重复使用*/
		case GPIO_Pin_11: for(;;); /*PC11重复使用*/
		case GPIO_Pin_12: for(;;); /*PC12重复使用*/
		case GPIO_Pin_13: for(;;); /*PC13重复使用*/
		case GPIO_Pin_14: for(;;); /*PC14重复使用*/
		case GPIO_Pin_15: for(;;); /*PC15重复使用*/
		default:
			for(;;); ;/*pin参数不合法*/
		}
	case (uint32_t)GPIOD_BASE:
		switch (pin)
		{
		case GPIO_Pin_0: for(;;); /*PD0重复使用*/
		case GPIO_Pin_1: for(;;); /*PD1重复使用*/
		case GPIO_Pin_2: for(;;); /*PD2重复使用*/
		case GPIO_Pin_3: for(;;); /*PD3重复使用*/
		case GPIO_Pin_4: for(;;); /*PD4重复使用*/
		case GPIO_Pin_5: for(;;); /*PD5重复使用*/
		case GPIO_Pin_6: for(;;); /*PD6重复使用*/
		case GPIO_Pin_7: for(;;); /*PD7重复使用*/
		case GPIO_Pin_8: for(;;); /*PD8重复使用*/
		case GPIO_Pin_9: for(;;); /*PD9重复使用*/
		case GPIO_Pin_10: for(;;); /*PD10重复使用*/
		case GPIO_Pin_11: for(;;); /*PD11重复使用*/
		case GPIO_Pin_12: for(;;); /*PD12重复使用*/
		case GPIO_Pin_13: for(;;); /*PD13重复使用*/
		case GPIO_Pin_14: for(;;); /*PD14重复使用*/
		case GPIO_Pin_15: for(;;); /*PD15重复使用*/
		default:
			for(;;); ;/*pin参数不合法*/
		}
	 case (uint32_t)GPIOE_BASE:
		switch (pin)
		{
		case GPIO_Pin_0: for(;;); /*PE0重复使用*/
		case GPIO_Pin_1: for(;;); /*PE1重复使用*/
		case GPIO_Pin_2: for(;;); /*PE2重复使用*/
		case GPIO_Pin_3: for(;;); /*PE3重复使用*/
		case GPIO_Pin_4: for(;;); /*PE4重复使用*/
		case GPIO_Pin_5: for(;;); /*PE5重复使用*/
		case GPIO_Pin_6: for(;;); /*PE6重复使用*/
		case GPIO_Pin_7: for(;;); /*PE7重复使用*/
		case GPIO_Pin_8: for(;;); /*PE8重复使用*/
		case GPIO_Pin_9: for(;;); /*PE9重复使用*/
		case GPIO_Pin_10: for(;;); /*PE10重复使用*/
		case GPIO_Pin_11: for(;;); /*PE11重复使用*/
		case GPIO_Pin_12: for(;;); /*PE12重复使用*/
		case GPIO_Pin_13: for(;;); /*PE13重复使用*/
		case GPIO_Pin_14: for(;;); /*PE14重复使用*/
		case GPIO_Pin_15: for(;;); /*PE15重复使用*/
		default:
			for(;;); ;/*pin参数不合法*/
		}
	 case (uint32_t)GPIOF_BASE:
		switch (pin)
		{
		case GPIO_Pin_0: for(;;); /*PF0重复使用*/
		case GPIO_Pin_1: for(;;); /*PF1重复使用*/
		case GPIO_Pin_2: for(;;); /*PF2重复使用*/
		case GPIO_Pin_3: for(;;); /*PF3重复使用*/
		case GPIO_Pin_4: for(;;); /*PF4重复使用*/
		case GPIO_Pin_5: for(;;); /*PF5重复使用*/
		case GPIO_Pin_6: for(;;); /*PF6重复使用*/
		case GPIO_Pin_7: for(;;); /*PF7重复使用*/
		case GPIO_Pin_8: for(;;); /*PF8重复使用*/
		case GPIO_Pin_9: for(;;); /*PF9重复使用*/
		case GPIO_Pin_10: for(;;); /*PF10重复使用*/
		case GPIO_Pin_11: for(;;); /*PF11重复使用*/
		case GPIO_Pin_12: for(;;); /*PF12重复使用*/
		case GPIO_Pin_13: for(;;); /*PF13重复使用*/
		case GPIO_Pin_14: for(;;); /*PF14重复使用*/
		case GPIO_Pin_15: for(;;); /*PF15重复使用*/
		default:
			for(;;); ;/*pin参数不合法*/
		}
	 case (uint32_t)GPIOG_BASE:
		switch (pin)
		{
		case GPIO_Pin_0: for(;;); /*PG0重复使用*/
		case GPIO_Pin_1: for(;;); /*PG1重复使用*/
		case GPIO_Pin_2: for(;;); /*PG2重复使用*/
		case GPIO_Pin_3: for(;;); /*PG3重复使用*/
		case GPIO_Pin_4: for(;;); /*PG4重复使用*/
		case GPIO_Pin_5: for(;;); /*PG5重复使用*/
		case GPIO_Pin_6: for(;;); /*PG6重复使用*/
		case GPIO_Pin_7: for(;;); /*PG7重复使用*/
		case GPIO_Pin_8: for(;;); /*PG8重复使用*/
		case GPIO_Pin_9: for(;;); /*PG9重复使用*/
		case GPIO_Pin_10: for(;;); /*PG10重复使用*/
		case GPIO_Pin_11: for(;;); /*PG11重复使用*/
		case GPIO_Pin_12: for(;;); /*PG12重复使用*/
		case GPIO_Pin_13: for(;;); /*PG13重复使用*/
		case GPIO_Pin_14: for(;;); /*PG14重复使用*/
		case GPIO_Pin_15: for(;;); /*PG15重复使用*/
		default:
			for(;;); ;/*pin参数不合法*/
		}
	default:
		//#warning "GPIO port参数错误"
		for (;;);
	}
	
}

GPIO PA0(GPIOA,GPIO_Pin_0);
GPIO PA1(GPIOA,GPIO_Pin_1);
GPIO PA2(GPIOA,GPIO_Pin_2);
GPIO PA3(GPIOA,GPIO_Pin_3);
GPIO PA4(GPIOA,GPIO_Pin_4);
GPIO PA5(GPIOA,GPIO_Pin_5);
GPIO PA6(GPIOA,GPIO_Pin_6);
GPIO PA7(GPIOA,GPIO_Pin_7);
GPIO PA8(GPIOA,GPIO_Pin_8);
GPIO PA9(GPIOA,GPIO_Pin_9);
GPIO PA10(GPIOA,GPIO_Pin_10);
GPIO PA11(GPIOA,GPIO_Pin_11);
GPIO PA12(GPIOA,GPIO_Pin_12);
GPIO PA13(GPIOA,GPIO_Pin_13);
GPIO PA14(GPIOA,GPIO_Pin_14);
GPIO PA15(GPIOA,GPIO_Pin_15);

GPIO PB0(GPIOB,GPIO_Pin_0);
GPIO PB1(GPIOB,GPIO_Pin_1);
GPIO PB2(GPIOB,GPIO_Pin_2);
GPIO PB3(GPIOB,GPIO_Pin_3);
GPIO PB4(GPIOB,GPIO_Pin_4);
GPIO PB5(GPIOB,GPIO_Pin_5);
GPIO PB6(GPIOB,GPIO_Pin_6);
GPIO PB7(GPIOB,GPIO_Pin_7);
GPIO PB8(GPIOB,GPIO_Pin_8);
GPIO PB9(GPIOB,GPIO_Pin_9);
GPIO PB10(GPIOB,GPIO_Pin_10);
GPIO PB11(GPIOB,GPIO_Pin_11);
GPIO PB12(GPIOB,GPIO_Pin_12);
GPIO PB13(GPIOB,GPIO_Pin_13);
GPIO PB14(GPIOB,GPIO_Pin_14);
GPIO PB15(GPIOB,GPIO_Pin_15);


GPIO PC0(GPIOC,GPIO_Pin_0);
GPIO PC1(GPIOC,GPIO_Pin_1);
GPIO PC2(GPIOC,GPIO_Pin_2);
GPIO PC3(GPIOC,GPIO_Pin_3);
GPIO PC4(GPIOC,GPIO_Pin_4);
GPIO PC5(GPIOC,GPIO_Pin_5);
GPIO PC6(GPIOC,GPIO_Pin_6);
GPIO PC7(GPIOC,GPIO_Pin_7);
GPIO PC8(GPIOC,GPIO_Pin_8);
GPIO PC9(GPIOC,GPIO_Pin_9);
GPIO PC10(GPIOC,GPIO_Pin_10);
GPIO PC11(GPIOC,GPIO_Pin_11);
GPIO PC12(GPIOC,GPIO_Pin_12);
GPIO PC13(GPIOC,GPIO_Pin_13);
GPIO PC14(GPIOC,GPIO_Pin_14);
GPIO PC15(GPIOC,GPIO_Pin_15);


GPIO PD0(GPIOD,GPIO_Pin_0);
GPIO PD1(GPIOD,GPIO_Pin_1);
GPIO PD2(GPIOD,GPIO_Pin_2);
GPIO PD3(GPIOD,GPIO_Pin_3);
GPIO PD4(GPIOD,GPIO_Pin_4);
GPIO PD5(GPIOD,GPIO_Pin_5);
GPIO PD6(GPIOD,GPIO_Pin_6);
GPIO PD7(GPIOD,GPIO_Pin_7);
GPIO PD8(GPIOD,GPIO_Pin_8);
GPIO PD9(GPIOD,GPIO_Pin_9);
GPIO PD10(GPIOD,GPIO_Pin_10);
GPIO PD11(GPIOD,GPIO_Pin_11);
GPIO PD12(GPIOD,GPIO_Pin_12);
GPIO PD13(GPIOD,GPIO_Pin_13);
GPIO PD14(GPIOD,GPIO_Pin_14);
GPIO PD15(GPIOD,GPIO_Pin_15);

GPIO PE0(GPIOE,GPIO_Pin_0);
GPIO PE1(GPIOE,GPIO_Pin_1);
GPIO PE2(GPIOE,GPIO_Pin_2);
GPIO PE3(GPIOE,GPIO_Pin_3);
GPIO PE4(GPIOE,GPIO_Pin_4);
GPIO PE5(GPIOE,GPIO_Pin_5);
GPIO PE6(GPIOE,GPIO_Pin_6);
GPIO PE7(GPIOE,GPIO_Pin_7);
GPIO PE8(GPIOE,GPIO_Pin_8);
GPIO PE9(GPIOE,GPIO_Pin_9);
GPIO PE10(GPIOE,GPIO_Pin_10);
GPIO PE11(GPIOE,GPIO_Pin_11);
GPIO PE12(GPIOE,GPIO_Pin_12);
GPIO PE13(GPIOE,GPIO_Pin_13);
GPIO PE14(GPIOE,GPIO_Pin_14);
GPIO PE15(GPIOE,GPIO_Pin_15);

GPIO PF0(GPIOF,GPIO_Pin_0);
GPIO PF1(GPIOF,GPIO_Pin_1);
GPIO PF2(GPIOF,GPIO_Pin_2);
GPIO PF3(GPIOF,GPIO_Pin_3);
GPIO PF4(GPIOF,GPIO_Pin_4);
GPIO PF5(GPIOF,GPIO_Pin_5);
GPIO PF6(GPIOF,GPIO_Pin_6);
GPIO PF7(GPIOF,GPIO_Pin_7);
GPIO PF8(GPIOF,GPIO_Pin_8);
GPIO PF9(GPIOF,GPIO_Pin_9);
GPIO PF10(GPIOF,GPIO_Pin_10);
GPIO PF11(GPIOF,GPIO_Pin_11);
GPIO PF12(GPIOF,GPIO_Pin_12);
GPIO PF13(GPIOF,GPIO_Pin_13);
GPIO PF14(GPIOF,GPIO_Pin_14);
GPIO PF15(GPIOF,GPIO_Pin_15);

GPIO PG0(GPIOG,GPIO_Pin_0);
GPIO PG1(GPIOG,GPIO_Pin_1);
GPIO PG2(GPIOG,GPIO_Pin_2);
GPIO PG3(GPIOG,GPIO_Pin_3);
GPIO PG4(GPIOG,GPIO_Pin_4);
GPIO PG5(GPIOG,GPIO_Pin_5);
GPIO PG6(GPIOG,GPIO_Pin_6);
GPIO PG7(GPIOG,GPIO_Pin_7);
GPIO PG8(GPIOG,GPIO_Pin_8);
GPIO PG9(GPIOG,GPIO_Pin_9);
GPIO PG10(GPIOG,GPIO_Pin_10);
GPIO PG11(GPIOG,GPIO_Pin_11);
GPIO PG12(GPIOG,GPIO_Pin_12);
GPIO PG13(GPIOG,GPIO_Pin_13);
GPIO PG14(GPIOG,GPIO_Pin_14);
GPIO PG15(GPIOG,GPIO_Pin_15);
static u8  fac_us=0;//us延时倍乘数
//static u16 fac_ms=0;//ms延时倍乘数
#ifdef __cplusplus
extern "C" {
#endif
	void SysTick_Handler(void);
#ifdef __cplusplus
}
#endif
//#ifdef OS_CRITICAL_METHOD 	//如果OS_CRITICAL_METHOD定义了,说明使用ucosII了.
////systick中断服务函数,使用ucos时用到
//void SysTick_Handler(void)
//{				   
//	OSIntEnter();		//进入中断
//	OSTimeTick();       //调用ucos的时钟服务程序               
//	OSIntExit();        //触发任务切换软中断
//}
//#endif
//
//
////初始化延迟函数
////当使用ucos的时候,此函数会初始化ucos的时钟节拍
////SYSTICK的时钟固定为HCLK时钟的1/8
////SYSCLK:系统时钟
//void delay_init(u8 SYSCLK)
//{
//#ifdef OS_CRITICAL_METHOD 	//如果OS_CRITICAL_METHOD定义了,说明使用ucosII了.
//	u32 reload;
//#endif
//	SysTick->CTRL&=~(1<<2);	//SYSTICK使用外部时钟源	 
//	fac_us=SYSCLK/8;		//不论是否使用ucos,fac_us都需要使用
//
//#ifdef OS_TICKS_PER_SEC	 	//如果时钟节拍数定义了,说明要使用ucosII了.
//	reload=SYSCLK/8;		//每秒钟的计数次数 单位为K	   
//	reload*=1000000/OS_TICKS_PER_SEC;//根据OS_TICKS_PER_SEC设定溢出时间
//	//reload为24位寄存器,最大值:16777216,在72M下,约合1.86s左右	
//	fac_ms=1000/OS_TICKS_PER_SEC;//代表ucos可以延时的最少单位	   
//	SysTick->CTRL|=1<<1;   	//开启SYSTICK中断
//	SysTick->LOAD=reload; 	//每1/OS_TICKS_PER_SEC秒中断一次	
//	SysTick->CTRL|=1<<0;   	//开启SYSTICK    
//#else
//	fac_ms=(u16)fac_us*1000;//非ucos下,代表每个ms需要的systick时钟数   
//#endif
//}								    
//
//#ifdef OS_TICKS_PER_SEC	//使用了ucos
////延时nus
////nus为要延时的us数.		    								   
//void delay_us(u32 nus)
//{		
//	u32 ticks;
//	u32 told,tnow,tcnt=0;
//	u32 reload=SysTick->LOAD;	//LOAD的值	    	 
//	ticks=nus*fac_us; 			//需要的节拍数	  		 
//	tcnt=0;
//	told=SysTick->VAL;        	//刚进入时的计数器值
//	while(1)
//	{
//		tnow=SysTick->VAL;	
//		if(tnow!=told)
//		{	    
//			if(tnow<told)tcnt+=told-tnow;//这里注意一下SYSTICK是一个递减的计数器就可以了.
//			else tcnt+=reload-tnow+told;	    
//			told=tnow;
//			if(tcnt>=ticks)break;//时间超过/等于要延迟的时间,则退出.
//		}  
//	}; 									    
//}
////延时nms
////nms:要延时的ms数
//void delay_ms(u16 nms)
//{	
//	if(OSRunning == 1)//如果os已经在跑了	    
//	{		  
//		if(nms>=fac_ms)//延时的时间大于ucos的最少时间周期 
//		{
//			OSTimeDly(nms/fac_ms);//ucos延时
//		}
//		nms%=fac_ms;				//ucos已经无法提供这么小的延时了,采用普通方式延时    
//	}
//	delay_us((u32)(nms*1000));	//普通方式延时,此时ucos无法启动调度.
//}
//#else//不用ucos时
////延时nus
////nus为要延时的us数.		    								   
//void delay_us(u32 nus)
//{		
//	u32 temp;	    	 
//	SysTick->LOAD=nus*fac_us; //时间加载	  		 
//	SysTick->VAL=0x00;        //清空计数器
//	SysTick->CTRL=0x01 ;      //开始倒数 	 
//	do
//	{
//		temp=SysTick->CTRL;
//	}
//	while(temp&0x01&&!(temp&(1<<16)));//等待时间到达   
//	SysTick->CTRL=0x00;       //关闭计数器
//	SysTick->VAL =0X00;       //清空计数器	 
//}
////延时nms
////注意nms的范围
////SysTick->LOAD为24位寄存器,所以,最大延时为:
////nms<=0xffffff*8*1000/SYSCLK
////SYSCLK单位为Hz,nms单位为ms
////对72M条件下,nms<=1864 
//void delay_ms(u16 nms)
//{	 		  	  
//	u32 temp;
//	u16 n;
//	n = ((u32)nms*fac_ms)/0xffffff;
//	while(n--)
//	{
//		SysTick->LOAD=0xffffff;//时间加载(SysTick->LOAD为24bit)
//		SysTick->VAL =0x00;           //清空计数器
//		SysTick->CTRL=0x01 ;          //开始倒数  
//		do
//		{
//			temp=SysTick->CTRL;
//		}
//		while(temp&0x01&&!(temp&(1<<16)));//等待时间到达   
//		SysTick->CTRL=0x00;       //关闭计数器
//		SysTick->VAL =0X00;       //清空计数器	 
//	}
//	SysTick->LOAD=((u32)nms*fac_ms)%0xffffff;;//时间加载(SysTick->LOAD为24bit)
//	SysTick->VAL =0x00;           //清空计数器
//	SysTick->CTRL=0x01 ;          //开始倒数  
//	do
//	{
//		temp=SysTick->CTRL;
//	}
//	while(temp&0x01&&!(temp&(1<<16)));//等待时间到达   
//	SysTick->CTRL=0x00;       //关闭计数器
//	SysTick->VAL =0X00;       //清空计数器	  	    
//} 
//#endif
#define TICKS_PER_SEC 10
volatile static long long SYS_TIME = 0; 
void delay_init(u8 SYSCLK)
{
	SysTick->CTRL&=~(1<<2);	//SYSTICK使用外部时钟源	 
	fac_us=SYSCLK/8;		//不论是否使用ucos,fac_us都需要使用
	SysTick->CTRL|=1<<1;   	//开启SYSTICK中断
	SysTick->LOAD=fac_us*TICKS_PER_SEC; 	//每1/OS_TICKS_PER_SEC秒中断一次	
	SysTick->CTRL|=1<<0;   	//开启SYSTICK 
}								    

//延时nus
//nus为要延时的us数.		    								   
void delay_us(u32 nus)
{		
	long long lasttime;
	lasttime = SYS_TIME ;
	lasttime += nus/TICKS_PER_SEC;
	while(SYS_TIME < lasttime)
	{
		;
	}
}
//延时nms
//注意nms的范围
//SysTick->LOAD为24位寄存器,所以,最大延时为:
//nms<=0xffffff*8*1000/SYSCLK
//SYSCLK单位为Hz,nms单位为ms
//对72M条件下,nms<=1864 
void delay_ms(u16 nms)
{	 		  	  
	long long lasttime;
	lasttime = SYS_TIME;
	lasttime += (nms * 1000 )/TICKS_PER_SEC;
	while(SYS_TIME < lasttime)
	{
		;
	} 	    
} 
uint64_t micros(void)
{
	return SYS_TIME*TICKS_PER_SEC;
}
uint64_t millis(void)
{
	return (SYS_TIME*TICKS_PER_SEC) / 1000;
}
void SysTick_Handler(void)
{
	SysTick->LOAD=fac_us*TICKS_PER_SEC; 	//每1/OS_TICKS_PER_SEC秒中断一次	
	SysTick->VAL=0x00;        //清空计数器
	SysTick->CTRL|=1<<0;   	//开启SYSTICK 
	SYS_TIME++;
}
