#include "exti.h"
const u8 Priority[16][2] = {
	{2,1},/*EXTI0*/
	{2,1},/*EXTI1*/
	{2,1},/*EXTI2*/
	{2,1},/*EXTI3*/
	{2,1},/*EXTI4*/
	{2,1},/*EXTI5*/
	{2,1},/*EXTI6*/
	{2,1},/*EXTI7*/
	{2,1},/*EXTI8*/
	{2,1},/*EXTI9*/
	{2,1},/*EXTI10*/
	{2,1},/*EXTI11*/
	{2,1},/*EXTI12*/
	{2,1},/*EXTI13*/
	{2,1},/*EXTI14*/
	{2,1},/*EXTI15*/
};
void (*exti_callback[16])();
EXTIx::EXTIx(GPIO *exti_pin, EXTITrigger_TypeDef trigger)
{
	this->exti_pin = exti_pin;
	this->trigger = trigger;
	init_info(exti_pin);
}
void EXTIx::begin()
{
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//中断线以及中断初始化配置
	GPIO_EXTILineConfig(port_source,pin_source);

	EXTI_InitStructure.EXTI_Line=exti_line;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
	EXTI_InitStructure.EXTI_Trigger = trigger;
	EXTI_InitStructure.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStructure);	 	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器

	NVIC_InitStructure.NVIC_IRQChannel = irq;			//使能按键所在的外部中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = Priority[pin_source][0];	//抢占优先级2 
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = Priority[pin_source][1];					//子优先级2 
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;								//使能外部中断通道
	NVIC_Init(&NVIC_InitStructure);  	  //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器
}
void EXTIx::attach_interrupt(void (*callback_fun)(void))
{
	exti_callback[pin_source] = callback_fun;
}
void EXTIx::interrupt(FunctionalState enable)
{
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line=exti_line;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
	EXTI_InitStructure.EXTI_Trigger = trigger;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);	 	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器
}
void EXTIx::init_info(GPIO *exti_pin)
{
	switch (exti_pin->pin)
	{
	case GPIO_Pin_0:
		exti_line = EXTI_Line0;
		irq = EXTI0_IRQn;
		pin_source = GPIO_PinSource0;
		break;
	case GPIO_Pin_1:
		exti_line = EXTI_Line1;
		irq = EXTI1_IRQn;
		pin_source = GPIO_PinSource1;
		break;
	case GPIO_Pin_2:
		exti_line = EXTI_Line2;
		irq = EXTI2_IRQn;
		pin_source = GPIO_PinSource2;
		break;
	case GPIO_Pin_3:
		exti_line = EXTI_Line3;
		irq = EXTI3_IRQn;
		pin_source = GPIO_PinSource3;
		break;
	case GPIO_Pin_4:
		exti_line = EXTI_Line4;
		irq = EXTI4_IRQn;
		pin_source = GPIO_PinSource4;
		break;
	case GPIO_Pin_5:
		exti_line = EXTI_Line5;
		irq = EXTI9_5_IRQn;
		pin_source = GPIO_PinSource5;
		break;
	case GPIO_Pin_6:
		exti_line = EXTI_Line6;
		irq = EXTI9_5_IRQn;
		pin_source = GPIO_PinSource6;
		break;
	case GPIO_Pin_7:
		exti_line = EXTI_Line7;
		irq = EXTI9_5_IRQn;
		pin_source = GPIO_PinSource7;
		break;
	case GPIO_Pin_8:
		exti_line = EXTI_Line8;
		irq = EXTI9_5_IRQn;
		pin_source = GPIO_PinSource8;
		break;
	case GPIO_Pin_9:
		exti_line = EXTI_Line9;
		irq = EXTI9_5_IRQn;
		pin_source = GPIO_PinSource9;
		break;
	case GPIO_Pin_10:
		exti_line = EXTI_Line10;
		irq = EXTI15_10_IRQn;
		pin_source = GPIO_PinSource10;
		break;
	case GPIO_Pin_11:
		exti_line = EXTI_Line11;
		irq = EXTI15_10_IRQn;
		pin_source = GPIO_PinSource11;
		break;
	case GPIO_Pin_12:
		exti_line = EXTI_Line12;
		irq = EXTI15_10_IRQn;
		pin_source = GPIO_PinSource12;
		break;
	case GPIO_Pin_13:
		exti_line = EXTI_Line13;
		irq = EXTI15_10_IRQn;
		pin_source = GPIO_PinSource13;
		break;
	case GPIO_Pin_14:
		exti_line = EXTI_Line14;
		irq = EXTI15_10_IRQn;
		pin_source = GPIO_PinSource14;
		break;
	case GPIO_Pin_15:
		exti_line = EXTI_Line15;
		irq = EXTI15_10_IRQn;
		pin_source = GPIO_PinSource15;
		break;
	default:
		break;
	}
	if(GPIOA == exti_pin->port)
		port_source = GPIO_PortSourceGPIOA;
	else if(GPIOB == exti_pin->port)
		port_source = GPIO_PortSourceGPIOB;
	else if(GPIOC == exti_pin->port)
		port_source = GPIO_PortSourceGPIOC;
	else if(GPIOD == exti_pin->port)
		port_source = GPIO_PortSourceGPIOD;
	else if(GPIOE == exti_pin->port)
		port_source = GPIO_PortSourceGPIOE;
	else if(GPIOF == exti_pin->port)
		port_source = GPIO_PortSourceGPIOF;
	else if(GPIOG == exti_pin->port)
		port_source = GPIO_PortSourceGPIOG;
}
#ifdef __cplusplus
extern "C" {
#endif
	void EXTI0_IRQHandler(void);
	void EXTI1_IRQHandler(void);
	void EXTI2_IRQHandler(void);
	void EXTI3_IRQHandler(void);
	void EXTI4_IRQHandler(void);

	void EXTI9_5_IRQHandler(void);
	void EXTI15_10_IRQHandler(void);
#ifdef __cplusplus
}
#endif
void EXTI0_IRQHandler(void)
{	 
	if(EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
		exti_callback[0]();
	}
	EXTI_ClearITPendingBit(EXTI_Line0);  //清除EXTI线路挂起位
}
void EXTI1_IRQHandler(void)
{	 
	if(EXTI_GetITStatus(EXTI_Line1) != RESET)
	{
		exti_callback[1]();
	}
	EXTI_ClearITPendingBit(EXTI_Line1);  //清除EXTI线路挂起位
}
void EXTI2_IRQHandler(void)
{	 
	if(EXTI_GetITStatus(EXTI_Line2) != RESET)
	{
		exti_callback[2]();
	}
	EXTI_ClearITPendingBit(EXTI_Line2);  //清除EXTI线路挂起位
}
void EXTI3_IRQHandler(void)
{	 
	if(EXTI_GetITStatus(EXTI_Line3) != RESET)
	{
		exti_callback[3]();
	}
	EXTI_ClearITPendingBit(EXTI_Line3);  //清除EXTI线路挂起位
}
void EXTI4_IRQHandler(void)
{	 
	if(EXTI_GetITStatus(EXTI_Line4) != RESET)
	{
		exti_callback[4]();
	}
	EXTI_ClearITPendingBit(EXTI_Line4);  //清除EXTI线路挂起位
}
void EXTI9_5_IRQHandler(void)
{	 
	if(EXTI_GetITStatus(EXTI_Line5) != RESET)
	{
		exti_callback[5]();
	}
	if(EXTI_GetITStatus(EXTI_Line6) != RESET)
	{
		exti_callback[6]();
	}
	if(EXTI_GetITStatus(EXTI_Line7) != RESET)
	{
		exti_callback[7]();
	}
	if(EXTI_GetITStatus(EXTI_Line8) != RESET)
	{
		exti_callback[8]();
	}
	if(EXTI_GetITStatus(EXTI_Line9) != RESET)
	{
		exti_callback[9]();
	}
	EXTI_ClearITPendingBit(EXTI_Line5);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line6);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line7);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line8);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line9);  //清除EXTI线路挂起位
}
void EXTI15_10_IRQHandler(void)
{	 
	if(EXTI_GetITStatus(EXTI_Line10) != RESET)
	{
		exti_callback[10]();
	}
	if(EXTI_GetITStatus(EXTI_Line11) != RESET)
	{
		exti_callback[11]();
	}
	if(EXTI_GetITStatus(EXTI_Line12) != RESET)
	{
		exti_callback[12]();
	}
	if(EXTI_GetITStatus(EXTI_Line13) != RESET)
	{
		exti_callback[13]();
	}
	if(EXTI_GetITStatus(EXTI_Line14) != RESET)
	{
		exti_callback[14]();
	}
	if(EXTI_GetITStatus(EXTI_Line15) != RESET)
	{
		exti_callback[15]();
	}
	EXTI_ClearITPendingBit(EXTI_Line10);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line11);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line12);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line13);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line14);  //清除EXTI线路挂起位
	EXTI_ClearITPendingBit(EXTI_Line15);  //清除EXTI线路挂起位
}
//示例
/*#include "LX_OS.h"
GPIO led0(GPIOB,GPIO_Pin_8);
USART uart1(USART1,&PA9,&PA10);
EXTIx ex(&PB15,EXTI_Trigger_Rising_Falling);
void fun()
{
	uart1.printf_length(uart1.receive_buf,512-DMA_GetCurrDataCounter(DMA1_Channel5));
}
void tme_fun()
{
	static u16 i=0;
	if(i++ > 500)
	{
		led0.toggle();
		i=0;

	}uart1.printf("%d pinsurce:%d\n",i,ex.pin_source);
}
void setup()
{
	LX_OS_init();
	led0.mode(OUTPUT_PP);
	uart1.begin(115200);
	uart1.attach_rx_interrupt(fun);
	PB15.mode(INPUT_PU);
	ex.begin();
	ex.attach_interrupt(tme_fun);
	ex.interrupt(ENABLE);
}
int main()
{
	setup();
	uart1.printf("Hello World\n");
	while(1)
	{
		led0.toggle();
		delay_ms(500);
	}
}*/
