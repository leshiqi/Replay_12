#include "LX_OS.h"
void delay_init(u8 SYSCLK);
void giop_rcc_init();
void LX_OS_init()
{
	giop_rcc_init();
	delay_init(72);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
}
void giop_rcc_init()
{
	 RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO
						   |RCC_APB2Periph_GPIOA
						   |RCC_APB2Periph_GPIOB
						   |RCC_APB2Periph_GPIOC
						   |RCC_APB2Periph_GPIOD
						   |RCC_APB2Periph_GPIOE
						   |RCC_APB2Periph_GPIOF
						   |RCC_APB2Periph_GPIOG, ENABLE);
	 GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE);//PB4Ĭ��ΪIO
}                                                   
