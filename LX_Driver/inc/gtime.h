#ifndef __TIME_H
#define __TIME_H
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"

/*
	1.支持tim2,3,4
	2.提供一个1-1Mhz的定时中断
*/
typedef void (*Time_callback)();	
class TIM
{
    public:
        TIM(TIM_TypeDef *TIMx);
        void begin(uint32_t frq);
        void attach_interrupt(Time_callback);
        void interrupt(FunctionalState enable);
        void start(void);
        void stop(void);
        void reset_frq(uint32_t frq);
    private:
        void base_init(uint16_t period,uint16_t prescaler);
        void set_reload(uint16_t auto_reload);
        void clear_count(void);	
        TIM_TypeDef *TIMx;
	
};
//void TIM2_IRQHandler(void);
#ifdef __cplusplus
 }
#endif
#endif
   
 

