/*
file   : common.h
author : shentq
version: V1.0
date   : 2015/7/5

Copyright 2015 shentq. All Rights Reserved.

Copyright Notice
No part of this software may be used for any commercial activities by any form or means, without the prior written consent of shentq.

Disclaimer
This specification is preliminary and is subject to change at any time without notice. shentq assumes no responsibility for any errors contained herein.
*/
#ifndef __BSP_H
#define __BSP_H


#include "core.h"                  // Device header



////////宏定义/////////////////////////////////


#define true 0x1
#define false 0x0

#define HIGH 0x1
#define LOW  0x0


#define LSB_FIRST 0
#define MSB_FIRST 1

#define PI 3.1415926535898


typedef enum
{ 
	AIN         = 0x0,
	INPUT       = 0x04,
	INPUT_PD    = 0x28,
	INPUT_PU    = 0x48,
	OUTPUT_OD   = 0x14,
	OUTPUT_PP   = 0x10,
	AF_OD       = 0x1C,
	AF_PP       = 0x18
}PIN_MODE;
typedef enum
{
	PA0_ID = 0,     PA1_ID,     PA2_ID,     PA3_ID,     PA4_ID,     PA5_ID,     PA6_ID,     PA7_ID,
	PA8_ID,         PA9_ID,     PA10_ID,    PA11_ID,    PA12_ID,    PA13_ID,    PA14_ID,    PA15_ID,

	PB0_ID = 0x10,  PB1_ID,     PB2_ID,     PB3_ID,     PB4_ID,     PB5_ID,     PB6_ID,     PB7_ID,
	PB8_ID,         PB9_ID,     PB10_ID,    PB11_ID,    PB12_ID,    PB13_ID,    PB14_ID,    PB15_ID,

	PC0_ID = 0x20,  PC1_ID,     PC2_ID,     PC3_ID,     PC4_ID,     PC5_ID,     PC6_ID,     PC7_ID,
	PC8_ID,         PC9_ID,     PC10_ID,    PC11_ID,    PC12_ID,    PC13_ID,    PC14_ID,    PC15_ID,

	PD0_ID = 0x30,  PD1_ID,     PD2_ID,     PD3_ID,     PD4_ID,     PD5_ID,     PD6_ID,     PD7_ID,
	PD8_ID,         PD9_ID,     PD10_ID,    PD11_ID,    PD12_ID,    PD13_ID,    PD14_ID,    PD15_ID,

	PE0_ID = 0x40,  PE1_ID,     PE2_ID,     PE3_ID,     PE4_ID,     PE5_ID,     PE6_ID,     PE7_ID,
	PE8_ID,         PE9_ID,     PE10_ID,    PE11_ID,    PE12_ID,    PE13_ID,    PE14_ID,    PE15_ID,

	PF0_ID = 0x50,  PF1_ID,     PF2_ID,     PF3_ID,     PF4_ID,     PF5_ID,     PF6_ID,     PF7_ID,
	PF8_ID,         PF9_ID,     PF10_ID,    PF11_ID,    PF12_ID,    PF13_ID,    PF14_ID,    PF15_ID,

	PG0_ID = 0x60,  PG1_ID,     PG2_ID,     PG3_ID,     PG4_ID,     PG5_ID,     PG6_ID,     PG7_ID,
	PG8_ID,         PG9_ID,     PG10_ID,    PG11_ID,    PG12_ID,    PG13_ID,    PG14_ID,    PG15_ID,
}PIN_ID_t;///<引脚的名字
class GPIO
{
public:
	GPIO(GPIO_TypeDef *port, uint16_t pin);
	void mode(PIN_MODE mode);
	void set();
	void reset();
	void write(uint8_t val);
	void toggle();	
	void read(uint8_t *val);
	uint8_t read(void);
	//操作符重载
	GPIO& operator= (int value) {
		write(value);
		return *this;
	}
	operator int() {
		return read();
	}
	PIN_ID_t id;
private:
	PIN_MODE pin_mode;
	GPIO_TypeDef* port;
	uint16_t pin;
	static uint16_t Port_busy_flag[7];
	void gpio_check(GPIO_TypeDef* port,uint16_t pin);
};
class PARALLEL_GPIO
{
public:
	GPIO *bit[8];
public:
	void    all_mode(PIN_MODE mode);
	void    write(uint8_t data);
	void    write_low_4_4bit(uint8_t data);
	uint8_t read();
	uint8_t read_low_4_bit();
};
extern GPIO PA0;
extern GPIO PA1;
extern GPIO PA2;
extern GPIO PA3;
extern GPIO PA4;
extern GPIO PA5;
extern GPIO PA6;
extern GPIO PA7;
extern GPIO PA8;
extern GPIO PA9;
extern GPIO PA10;
extern GPIO PA11;
extern GPIO PA12;
extern GPIO PA13;
extern GPIO PA14;
extern GPIO PA15;

extern GPIO PB0;
extern GPIO PB1;
extern GPIO PB2;
extern GPIO PB3;
extern GPIO PB4;
extern GPIO PB5;
extern GPIO PB6;
extern GPIO PB7;
extern GPIO PB8;
extern GPIO PB9;
extern GPIO PB10;
extern GPIO PB11;
extern GPIO PB12;
extern GPIO PB13;
extern GPIO PB14;
extern GPIO PB15;

extern GPIO PC0;
extern GPIO PC1;
extern GPIO PC2;
extern GPIO PC3;
extern GPIO PC4;
extern GPIO PC5;
extern GPIO PC5;
extern GPIO PC6;
extern GPIO PC7;
extern GPIO PC8;
extern GPIO PC9;
extern GPIO PC10;
extern GPIO PC11;
extern GPIO PC12;
extern GPIO PC13;
extern GPIO PC14;
extern GPIO PC15;

extern GPIO PD0;
extern GPIO PD1;
extern GPIO PD2;
extern GPIO PD3;
extern GPIO PD4;
extern GPIO PD5;
extern GPIO PD5;
extern GPIO PD6;
extern GPIO PD7;
extern GPIO PD8;
extern GPIO PD9;
extern GPIO PD10;
extern GPIO PD11;
extern GPIO PD12;
extern GPIO PD13;
extern GPIO PD14;
extern GPIO PD15;

extern GPIO PE0;
extern GPIO PE1;
extern GPIO PE2;
extern GPIO PE3;
extern GPIO PE4;
extern GPIO PE5;
extern GPIO PE5;
extern GPIO PE6;
extern GPIO PE7;
extern GPIO PE8;
extern GPIO PE9;
extern GPIO PE10;
extern GPIO PE11;
extern GPIO PE12;
extern GPIO PE13;
extern GPIO PE14;
extern GPIO PE15;

extern GPIO PF0;
extern GPIO PF1;
extern GPIO PF2;
extern GPIO PF3;
extern GPIO PF4;
extern GPIO PF5;
extern GPIO PF5;
extern GPIO PF6;
extern GPIO PF7;
extern GPIO PF8;
extern GPIO PF9;
extern GPIO PF10;
extern GPIO PF11;
extern GPIO PF12;
extern GPIO PF13;
extern GPIO PF14;
extern GPIO PF15;

extern GPIO PG0;
extern GPIO PG1;
extern GPIO PG2;
extern GPIO PG3;
extern GPIO PG4;
extern GPIO PG5;
extern GPIO PG5;
extern GPIO PG6;
extern GPIO PG7;
extern GPIO PG8;
extern GPIO PG9;
extern GPIO PG10;
extern GPIO PG11;
extern GPIO PG12;
extern GPIO PG13;
extern GPIO PG14;
extern GPIO PG15;


#endif
