/*
file   : uartx.h
author : shentq
version: V1.0
date   : 2015/7/5

Copyright 2015 shentq. All Rights Reserved.

Copyright Notice
No part of this software may be used for any commercial activities by any form or means, without the prior written consent of shentq.

Disclaimer
This specification is preliminary and is subject to change at any time without notice. shentq assumes no responsibility for any errors contained herein.
*/

#ifndef __UARTX_H
#define __UARTX_H
#ifdef __cplusplus
extern "C" {
#endif
#include "common.h"

//#include <stdio.h>
//#include <stdarg.h>


//用户配置//////////////
#define USE_DMA 0//开启dma，只有串口1，2，3支持,4和5不支持
#define UART_MAX_SEND_BUF 2048u
#define UART_MAX_RECEVE_BUF 512u
#define UART_TX_BLOCK_NUM 32u
#ifdef DEBUG
#define 	USART1_USE  
#endif // 
#if 0
#define USART2_USE 
#endif
#if 0
  
#define USART3_USE 
#endif
 
typedef struct uart_tx_block
{
	uint16_t sta;
	uint16_t end;
	uint16_t num;
	uint16_t sned_len;
}uart_tx_block2;
typedef void (*Uart_callback_type)();
class USART
{
    public:
        USART(USART_TypeDef *USARTx,GPIO *tx_pin,GPIO *rx_pin);

        void    begin(uint32_t baud_rate);
        void    begin(uint32_t baud_rate,uint8_t data_bit,uint8_t parity,uint8_t stop_bit);
        void    attach_rx_interrupt(Uart_callback_type);
        void    attach_tx_interrupt(Uart_callback_type);

        int 	put_char(uint16_t ch);
        void 	put_string(const char *str);
        void    printf_length(const char *str,uint16_t length);
        void    printf(const char *fmt,...);//需要注意缓冲区溢出
        void    wait_busy();
    
        uint16_t    receive();
		char                receive_buf[UART_MAX_RECEVE_BUF];
		char                send_buf[UART_MAX_SEND_BUF];
			uint8_t tx_send_fun();
    private:
        USART_TypeDef       *_USARTx;
		GPIO				*tx_pin;
		GPIO				*rx_pin;
       
		uint8_t*	tx_finish;
		uint8_t*	rx_finish;
		uint32_t    rcc_uart;
		uint8_t     irq_uart;
		uint8_t     irq_dma_tx;
		uint8_t     irq_dma_rx;
		uint8_t		uart_rx_PreemptionPriority;
		uint8_t		uart_rx_SubPriority;
		uint8_t		dma_rx_PreemptionPriority;
		uint8_t		dma_rx_SubPriority;
		uint8_t		dma_tx_PreemptionPriority;
		uint8_t		dma_tx_SubPriority;
		DMA_Channel_TypeDef* dma_channel_tx;
		DMA_Channel_TypeDef* dma_channel_rx; 
        u8   uart_num;
		
        uint16_t    dma_send_string(const char *str,uint16_t length);
        void        put_string(const char *str,uint16_t length);
        void        set_busy();
        void        interrupt(FunctionalState enable);
		void        uart_dma_init();
		void        init_info(USART_TypeDef *USARTx);
		uart_tx_block tx_block;
		u8 tx_buff_next;
		void tx_node_init();
	

};
#ifdef USART1_USE
extern USART uart1;
#endif
#ifdef USART2_USE
extern USART uart2;
#endif // USART2_USE
#ifdef USART3_USE
extern USART uart3;
#endif // USART3_USE

#ifdef __cplusplus
 }
#endif

#if 1
 void easyflash_output(const char *fmt,uint16_t size);
 void easyflash_printf(const char *fmt,...);
 void Debug(const char *fmt,...);
#endif

#endif
