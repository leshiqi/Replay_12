
#ifndef __CAN_H
#define __CAN_H

#include "common.h"

typedef enum
{
	BSP_CAN_1MBPS = 0,                   // 波特率 1M
	BSP_CAN_900KBPS,                 // 波特率 900K 模式                  // 波特率 1M模式
	BSP_CAN_800KBPS,                 // 波特率 800K 模式                  // 波特率 1M模式
	BSP_CAN_666KBPS,                 // 波特率 666K 模式                  // 波特率 1M模式
	BSP_CAN_600KBPS,                 // 波特率 600K 模式                  // 波特率 1M模式
	BSP_CAN_500KBPS,                 // 波特率 500K 模式                  // 波特率 1M模式
	BSP_CAN_400KBPS,                 // 波特率 400K 模式                  // 波特率 1M模式
	BSP_CAN_300KBPS,                 // 波特率 300K 模式                  // 波特率 1M模式
	BSP_CAN_250KBPS,                 // 波特率 250K 模式                  // 波特率 1M模式
	BSP_CAN_225KBPS,                 // 波特率 225K 模式                  // 波特率 1M模式
	BSP_CAN_200KBPS,                 // 波特率 200K 模式                  // 波特率 1M模式
	BSP_CAN_160KBPS,                 // 波特率 160K 模式                  // 波特率 1M模式
	BSP_CAN_150KBPS,                 // 波特率 150K 模式                  // 波特率 1M模式
	BSP_CAN_144KBPS,                 // 波特率 144K 模式                  // 波特率 1M模式
	BSP_CAN_125KBPS,                 // 波特率 125K 模式                  // 波特率 1M模式
	BSP_CAN_120KBPS,                 // 波特率 120K 模式                  // 波特率 1M模式
	BSP_CAN_100KBPS,                 // 波特率 100K 模式                  // 波特率 1M模式
	BSP_CAN_90KBPS,                  // 波特率 90K 模式                  // 波特率 1M模式
	BSP_CAN_80KBPS,                  // 波特率 80K 模式                  // 波特率 1M模式
	BSP_CAN_75KBPS,                  // 波特率 75K 模式                  // 波特率 1M模式
	BSP_CAN_60KBPS,                  // 波特率 60K 模式                  // 波特率 1M模式
	BSP_CAN_50KBPS,                  // 波特率 50K 模式                  // 波特率 1M模式
	BSP_CAN_40KBPS,                  // 波特率 40K 模式                  // 波特率 1M模式
	BSP_CAN_30KBPS,                  // 波特率 30K 模式                  // 波特率 1M模式
	BSP_CAN_20KBPS,                  // 波特率 20K 模式 
}BSP_CAN_BAUD;
#define CAN1_RX0_PreemptionPriority   1
#define CAN1_RX0_SubPriority          1

#define CAN1_RX1_PreemptionPriority   1
#define CAN1_RX1_SubPriority          2

extern CanRxMsg Rx0_Message;
extern CanRxMsg Rx1_Message;
class CAN
{
    public:
      	CAN(CAN_TypeDef* CANx,GPIO* p_pin_rx, GPIO* p_pin_tx);
		void begin(BSP_CAN_BAUD bps);
		void set_filter(u8 Fifo,u8 nCanType,u8 num,u32 ID,u32 Mask);
        void interrupt(FunctionalState enable);
        void attach_interrupt_rx0(void (*callback_fun)(void));
		void attach_interrupt_rx1(void (*callback_fun)(void));
        uint8_t  write(uint32_t id,uint8_t *data,u8 len);
        uint8_t   read(CanRxMsg *pCanMsg, u16 WaitTime);
		CanTxMsg* tx_message;
		CanRxMsg* rx0_message;
		CanRxMsg* rx1_message;
private:
		void nvic_init();
		void filter_init();
    	void set_bps(BSP_CAN_BAUD);
		
	    CAN_TypeDef* _CANx;
        GPIO* pin_rx;           
        GPIO* pin_tx;        
		
        BSP_CAN_BAUD _bps;    
};
void CAN_SetMsg(u32 id,u8 *data,u8 n);
#endif
