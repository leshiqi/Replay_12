#ifndef __PRINTF_H
#define __PRINTF_H
//#include "printf.h"
#define isdigit(c) ((c) >= '0' && (c) <= '9')
typedef char *  va_list;
#define _ADDRESSOF(v)   ( &reinterpret_cast\
									< char &>\
								(v) )
#define _INTSIZEOF(n)   ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) )
#define va_start(ap,v)  ( ap = (va_list)_ADDRESSOF(v) + _INTSIZEOF(v) )
//#define va_start(ap,v)  ( ap = (char *) &(ap) + _INTSIZEOF(v) )
#define va_arg(ap,t)    ( *(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)) )
#define va_end(ap)      ( ap = (va_list)0 )

int vsprintf(char *buf, const char *fmt, va_list args);
int sprintf(char *buf, const char *fmt, ...);
int printf(const char *fmt, ...);

#endif

