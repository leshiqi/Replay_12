#include "LX_OS.h"
//#include "LXCanDevice.h"
#include "device.h"

#include "LXCanTelegram.h"
#include "LXCanPort.h"
#include "LXCabDatabase.h"



void test();
void sendlog();


void LXCanEvents(uint8_t index)
{
	elog_i("CanEvent","index:%d",index);
}
void setup()
{
	LX_OS_init();
//	delay_ms(300);
#ifdef DEBUG


	uart1.begin(256000);
	sendlog();
	/*****************************/
	elog_init();
	elog_start();
	/* start EasyLogger */
#endif	
	//协议中数据保存函数指针
	LXCAN_FLASH_Read = STMFLASH_Read;
	LXCAN_FLASH_Write = STMFLASH_Write;

	//device_test();
	device_init();
	elog_i("info","physical:%d",device.data_table.physical_read());
}

int main()
{
	uint32_t lasttime = 0;
	setup();
	//can1.attach_interrupt_rx0(can_fun);
	lasttime = millis();
	elog_d("Device Mode" , "mode:%d" , device.Device_state);
	while (1)
	{
		if((millis() - lasttime) >= 1)
		{
			lasttime = millis();
			relay_task();
			key_task();
		}
		led_task();
		device.system_task();
		switch (device.Device_state)
		{
		case DeviceState_Run:
			device.application_task();
			if(set_key_read() == KeyLong)
			{
				elog_d("device_mode" , "RNN_long");
				device.Device_state = DeviceState_Debug;
			}
			break;
		case DeviceState_Debug:
			led_mode_set(Debug_Led);
			switch (set_key_read())
			{
			case KeyShort:
				device.Device_state = DeviceState_Run;
				elog_d("device_mode" , "Debug_Click");
				led_mode_set(Heartbeat_Led);
				break;
			case KeyLong:
				elog_d("device_mode" , "Debug_long");
				device.Device_state = DeviceState_ApplyCfg;
				led_mode_set(Download_Led);
				device.apply_cfg();
				break;
			}
			break;
		case DeviceState_Physical:
			if(set_key_read() == KeyClick)
			{
				device.send_info(LXCan_COMMAND_INFO);
			}
			break;
		case DeviceState_Boot:
			break;
		case DeviceState_ApplyCfg:
			if (set_key_read() == KeyClick)
			{
				elog_d("device_mode" , "ApplyCfg_long");
				device.Device_state = DeviceState_Run;
				led_mode_set(Heartbeat_Led);
			}
		default:
			break;
		}
		
		for (int i = 0; i < RELAYNUM; i++)
		{
			if(key_read(i) == KeyShort)
			{
				device.physical_control(i,!relay_read(i));
				elog_d("relay" , "[%d]",i);
			}
		}
		fan_auto();
	}
}
#ifdef DEBUG
void sendlog()
{
const  char* log = "*****************************************************\n\
*                 uuuuuuu\n\
*             uu$$$$$$$$$$$uu\n\
*          uu$$$$$$$$$$$$$$$$$uu\n\
*         u$$$$$$$$$$$$$$$$$$$$$u\n\
*        u$$$$$$$$$$$$$$$$$$$$$$$u\n\
*       u$$$$$$$$$$$$$$$$$$$$$$$$$u\n\
*       u$$$$$$$$$$$$$$$$$$$$$$$$$u\n\
*       u$$$$$$'   '$$$'   '$$$$$$u\n\
*       '$$$$'      u$u       $$$$'\n\
*        $$$u       u$u       u$$$\n\
*        $$$u      u$$$u      u$$$\n\
*         '$$$$uu$$$   $$$uu$$$$'\n\
*          '$$$$$$$'   '$$$$$$$'\n\
*            u$$$$$$$u$$$$$$$u\n\
*             u$'$'$'$'$'$'$u\n\
*  uuu        $$u$ $ $ $ $u$$       uuu\n\
* u$$$$        $$$$$u$u$u$$$       u$$$$\n\
*  $$$$$uu      '$$$$$$$$$'     uu$$$$$$\n\
* $$$$$$$$$$$uu    '''''    uuuu$$$$$$$$$$\n\
*$$$$'''$$$$$$$$$$uuu   uu$$$$$$$$$'''$$$'\n\
* '''      ''$$$$$$LeTianuu ''$'''\n\
*           uuuu ''$$$$$$$$$$uuu\n\
*  u$$$uuu$$$$$$$$$uu ''$$$$$$$$$$$uuu$$$\n\
*  $$$$$$$$$$''''           ''$$$$$$$$$$$'\n\
*   '$$$$$'                      ''$$$$''\n\
*     $$$'                         $$$$'\n\
*________________________LX_CAN_Weak___________________\n\
*__________________________________________LeTian______\n\
Complie time:  " __DATE__ " - " __TIME__ "\n\
";
	uart1.put_string(log);
	uart1.printf("Boot time: %d\n",millis());
	uart1.printf("MAC: %X%X%X \n",*(uint32_t*)(0x1ffff7e8),*(uint32_t*)(0x1ffff7ec),*(uint32_t*)(0x1ffff7f0));
	uart1.put_string("\\*****************************************************\n");
//	uart1.printf("Complie time:  " __DATE__ " - " __TIME__ "\n");
}
#endif
void test()
{
	static uint8_t mode= 0;
	if(set_key_read() == KeyClick)
	{
		led_mode_set((e_LedMode)mode);
		elog_d("Led","mode:%d",mode);
		mode++;
		mode %= 4; 
	}
}
void assert_failed(uint8_t* file, uint32_t line)
{
		;
}

