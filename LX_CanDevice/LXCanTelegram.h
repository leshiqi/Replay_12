#ifndef __LXCANTELEGRAM_H
#define __LXCANTELEGRAM_H
#include "LXCanConfig.h"

enum e_FunctionalState{
	LXCan_DISABLE = 0, 
	LXCan_ENABLE = !LXCan_DISABLE
} ;
enum  e_LXCanPriority{
	LXCAN_PRIORITY_SYSTEM_VALUE = 0  ,
	LXCAN_PRIORITY_HIGH_VALUE    ,
	LXCAN_PRIORITY_ALARM_VALUE   ,
	LXCAN_PRIORITY_NORMAL_VALUE 
};
enum e_LXCanService {
	LXCan_COMMAND_ACK = 0 ,
	LXCan_COMMAND_HEATBEAT,
	LXCan_COMMAND_RUNSTATE,
	LXCan_COMMAND_REAST,
	LXCan_COMMAND_PHYSICAL,
	LXCan_COMMAND_INFO,
	LXCan_COMMAND_SYS_STATE,
	//缺省
	LXCan_COMMAND_VALUE_RESPONSE  = 0x0A,
	LXCan_COMMAND_VALUE_READ ,
	LXCan_COMMAND_VALUE_WRITE    ,
	LXCan_COMMAND_SYS_READ ,
	LXCan_COMMAND_SYS_WRITE ,
	LXCan_COMMAND_SERVIER

};
//配置数据命令字
enum e_LXCanConfigCmd{
	LXCan_CONFIG_INFO_PHYSICAL = 0,
	LXCan_CONFIG_INFO_VERSION_SOFTWARE,
	LXCan_CONFIG_INFO_VERSION_FIRMWARE,
	LXCan_CONFIG_INFO_MAC,

	LXCan_CONFIG_OBJ_TOTAL = 0x10,
	LXCan_CONFIG_OBJ_DPT_CONFIG,
	LXCan_CONFIG_OBJ_VAL_DPT,
	LXCan_CONFIG_OBJ_CONFIG,
	LXCan_CONFIG_OBJ_REGISTER,

	LXCan_CONFIG_GROUP_TOTAL = 0x20,
	LXCan_CONFIG_GROUP_ADD,

	LXCan_CONFIG_RELATE_TOTAL = 0x30,
	LXCan_CONFIG_RELATE_INDICATOR,
	LXCan_CONFIG_RELATE_GROUP_INDEX,
	LXCan_CONFIG_RELATE_OBJECT_INDEX,
	LXCan_CONFIG_RELATE_REGISTER,
	LXCan_CONFIG_RELATE_OBJ_REGISTER
};



enum e_LXCanTelegramValidity { 
	LXCan_TELEGRAM_VALID = 0 ,
	LXCan_TELEGRAM_INVALID_CONTROL_FIELD,
	LXCan_TELEGRAM_UNSUPPORTED_FRAME_FORMAT,
	LXCan_TELEGRAM_INCORRECT_PAYLOAD_LENGTH,
	LXCan_TELEGRAM_INVALID_COMMAND_FIELD,
	LXCan_TELEGRAM_UNKNOWN_COMMAND,
	LXCan_TELEGRAM_INCORRECT_CHECKSUM
};
enum e_LXCanFrameType{
	Frame_None,
	Frame_Firtst,
	Frame_Middle,
	Frame_End
};
enum e_TypeAdd{ Physical_Add,Group_Add};

typedef struct 
{
#pragma anon_unions
	union {
		uint32_t CAN_ID;
		struct {
			uint32_t : 6;
			uint32_t  _repetition_flag : 1; //重复位
			uint32_t  _ack_flag  : 1;    //应达位
			uint32_t  _dest_add : 10;  //目的地址
			uint32_t  _type_add : 1;  //物理，组地址
			uint32_t  _source_add : 8;    //源地址
			uint32_t  _priority : 2;     //优先级
			uint32_t : 3;
		};
	};
	union{
		struct{
			uint8_t             : 2;
			uint8_t _frame_flag : 2;
			uint8_t _service    : 4;
			uint8_t _value[7];
		};
		uint8_t _buff_data[8];
	};
	uint8_t length;//数据区所有数据长度	
}TelegramType;

class LXCanTelegram {
	
private:
	TelegramType* Telegram_data; 
public:
	LXCanTelegram();
	void begin(TelegramType *data);
	void clear();
	uint32_t ID_read();
	void ID_write(uint32_t ID);
	e_FunctionalState repetition_read();
	void repetition_write(e_FunctionalState newstate);
	e_FunctionalState ACK_read();
	void ACK_write(e_FunctionalState newstate);
	LXCanGroupAddrType dest_read();
	void dest_write(LXCanGroupAddrType addr);
	e_TypeAdd TypeAdd_read();
	void TypeAdd_write(e_TypeAdd newtype);
	LXCanPhysicalAddrType source_read();
	void source_write(LXCanPhysicalAddrType addr);
	e_LXCanPriority priority_read();
	void priority_write(e_LXCanPriority newpriority);

	uint8_t buff_read(uint8_t index);
	void buff_read(uint8_t &len,uint8_t*data);
	void buff_write(uint8_t index, uint8_t data);
	void buff_write(uint8_t len,uint8_t *data);
	e_LXCanService service_read();
	void servicr_write(e_LXCanService newservice);
	e_LXCanFrameType frame_read();
	void frame_write(e_LXCanFrameType newframe);
	void value_read(uint8_t &len,uint8_t*val);
	void value_write(uint8_t len,uint8_t*val);
	uint8_t datalength_read();
	void datalength_write(uint8_t len);
};
void TestWrite();
#endif
