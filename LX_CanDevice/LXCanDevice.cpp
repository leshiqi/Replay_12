#include "LXCanDevice.h"
#include "relay.h"
uint16_t u8Tou16(uint8_t* val);
void u16Tou8(uint16_t data, uint8_t* val);
LXCanDevice::LXCanDevice(CAN* port):_port(port)
{
//	data_table.physical_write(addr);
	setphysical_last_time = 0;
	Device_state = DeviceState_Run;
}

uint8_t LXCanDevice::receive_task()
{
	//接收数据处理
	// 如果有分段数据需拼接
	TelegramType telegram_temp;
	DeviceDataType device_data;
	uint8_t msg_data[64] = {0};//消息数据缓存区
	
	if( !_port.rxActionList.Pop(telegram_temp))
		return 0;
	if(telegram_temp._frame_flag == Frame_None)//接收数据未分帧
	{
		device_data.CAN_ID = telegram_temp.CAN_ID;
		device_data.service = (e_LXCanService)telegram_temp._service;
		for (int i = 0; i < telegram_temp.length; i++)
		{
			msg_data[i] = telegram_temp._value[i];
		}
		device_data.length_value = telegram_temp.length - 1;
		device_data.value = msg_data;
	}
	else//接收到分帧数据
	{

	}


	if(device_data._type_add == Physical_Add)//接收到物理地址
	{
		return receive_physical(device_data);
	}
	else //接收到组地址
	{
		return receive_group(device_data);
	}
}

void LXCanDevice::init()
{
	data_table.init();
	_port.init();
	_port.physical_write(data_table.physical_read());
	//data_table.physical_write(addr);

	data_table.Device_mac_write((uint8_t*)(0x1FFFF7EE));//截取单片机MAC
	/*uint8_t mac[6];
	data_table.Device_mac_read(mac);
	elog_d("MAC","[0-6]:%02x,%02x,%02x,%02x,%02x,%02x",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);*/
}

//接收物理地址处理
uint8_t LXCanDevice::receive_physical(DeviceDataType device_data)
{
	TelegramType telegram_temp;
	uint8_t value_len;
	uint8_t *value;
	uint8_t index;
	switch (device_data.service)
	{
	case LXCan_COMMAND_ACK:
		break;
	case LXCan_COMMAND_HEATBEAT:
		break;
	case LXCan_COMMAND_RUNSTATE:
		Device_state = (e_DeviceState)device_data.value[0];
		switch(Device_state)
		{
		case DeviceState_Run:
			led_mode_set(Heartbeat_Led);
			break;
		case DeviceState_Debug:
			data_table.savedata_time = 1;
			led_mode_set(Debug_Led);
		case DeviceState_Physical:
			uint16_t temp;
			temp = u8Tou16(&device_data.value[1]);
			if(temp == data_table.category)
			{
				setphysical_last_time = millis() + SetPhysicaWrite; 
				led_mode_set(PhysicalAdd_Led);
			}
			break;
		case DeviceState_Boot:
			
			break;
		default:
			break;
		}
		elog_d("Device","stete:%d",Device_state);
		break;
	case LXCan_COMMAND_REAST:
		//设备复位
		LXCan_Device_Reset();
		break;
	case LXCan_COMMAND_PHYSICAL:
		elog_d("Device" , "Physical mac:%x,%x,%x,%x,%x,%x,",device_data.value[0],device_data.value[1],device_data.value[2],device_data.value[3],device_data.value[4],device_data.value[5]);
		if(data_table.Device_mac_check(&device_data.value[0]))
		{
			data_table.physical_write(device_data.value[6]);
			send_info(LXCan_COMMAND_PHYSICAL);
			//delay_ms(500);
			//LXCan_Device_Reset();
		}
		break;
	//case LXCan_COMMAND_INFO:
	//	if(0 == device_data.value[0])//mac地址
	//	{
	//		uint16_t temp;
	//		temp = u8Tou16(&device_data.value[1]);
	//		if(temp == data_table.category)
	//		{
	//			telegram_temp.CAN_ID = device_data.CAN_ID;
	//			telegram_temp._value[0] = 0;
	//			data_table.Device_mac_read(&telegram_temp._value[1]);
	//		}
	//	}

		//break;
		case LXCan_COMMAND_SYS_STATE:
		switch (device_data.value[0])
		{
		case 0:
			if(device_data.value[1] != 0xff)
				data_table.systemstate.card_flag = Add_Card;
			else
				data_table.systemstate.card_flag = NO_Card;
			elog_d("SYS_STATE" , "card :%d ", data_table.systemstate.card_flag);
			break;
		default:
			break;
		}

		break;
	case LXCan_COMMAND_VALUE_RESPONSE:
		//无
		break;
	case LXCan_COMMAND_VALUE_READ:
		//读对象值，系统直接回复
		telegram_temp.CAN_ID = device_data.CAN_ID;
		telegram_temp._dest_add = device_data._source_add;//物理地址处理时，使用指令帧汇中的源地址作为回复帧的目的地址
		telegram_temp._source_add = data_table.physical_read();
		telegram_temp._service = LXCan_COMMAND_VALUE_RESPONSE;
		telegram_temp._value[0] = device_data.value[0];
		index = telegram_temp._value[0];
		value = &telegram_temp._value[1];
		data_table.object_table[index].value_read(value,value_len);
		telegram_temp.length = value_len+2;//发送数据长度 = 服务+对象索引+对象值
		_port.txActionList.Append(telegram_temp);
		break;
	case LXCan_COMMAND_VALUE_WRITE:
		//relay_write(device_data.value[0] , device_data.value[1]);
		physical_control(device_data.value[0],device_data.value[1]);
		//index = device_data.value[0];
		//value = &device_data.value[1];
		//value_len = device_data.length_value - 1;
		//data_table.object_table[index].value_write(value,value_len);
		//object_flag[index].Updata_flag = LXCan_ENABLE;//使能对象值更新标志

		break;
	
	case LXCan_COMMAND_SYS_READ:
		config_read(device_data);
	
		break;
	case LXCan_COMMAND_SYS_WRITE:
		config_write(device_data.value);	
		telegram_temp._buff_data[0] = 0;
		telegram_temp.CAN_ID = device_data.CAN_ID;
		telegram_temp._dest_add = device_data._source_add;//物理地址处理时，使用指令帧汇中的源地址作为回复帧的目的地址
		telegram_temp._source_add = data_table.physical_read();
		telegram_temp._service = LXCan_COMMAND_ACK;
		telegram_temp._value[0] = device_data.service;
		telegram_temp.length = 2;//发送数据长度 
		_port.txActionList.Append(telegram_temp);
		elog_set_output_enabled(false);//下发配置关闭日志输出
		break;
	case LXCan_COMMAND_SERVIER:
		break;
	default:
		break;
	}
	return 1;
}
//接收组地址处理
uint8_t LXCanDevice::receive_group(DeviceDataType device_data)
{
	//TelegramType telegram_temp;
	//uint8_t value_len;
	//uint8_t *value;
	//uint8_t index;

	

	switch(device_data.service)
	{
	case LXCan_COMMAND_ACK:
		commit_ack(device_data);
		break;
	case LXCan_COMMAND_HEATBEAT:
		break;
	case LXCan_COMMAND_VALUE_RESPONSE:
		break;
	case LXCan_COMMAND_VALUE_READ:
		//读对象值，系统直接回复
		value_read(device_data);
		break;
	case LXCan_COMMAND_VALUE_WRITE:
		//总控退出判断
		/*if(matercontrol(device_data))
			break;*/
		matercontrol(device_data);
		//总线写对象值到设备中
		value_write(device_data);
		break;
	case LXCan_COMMAND_SYS_READ:
		//无
		break;
	case LXCan_COMMAND_SYS_WRITE:
		//无
		break;
	case LXCan_COMMAND_REAST:
		LXCan_Device_Reset();
		break;
	case LXCan_COMMAND_SERVIER:
		break;
	default:
		break;

	}
	return 1;
}

uint8_t LXCanDevice::matercontrol(DeviceDataType device_data)
{
	static TelegramType telegram_temp;
	LXCanObject **object_buff;
	LXCanAddress **group_buff;
	uint8_t object_len = 0;
	uint8_t group_len = 0;
	uint16_t *assoction_index0;
	uint16_t *assoction_index;
	telegram_temp._repetition_flag = LXCan_DISABLE;
	telegram_temp._ack_flag = LXCan_DISABLE;
	telegram_temp._type_add = Group_Add;
	
	telegram_temp._source_add = data_table.physical_read();
	telegram_temp._priority = device_data._priority;

	telegram_temp._service = LXCan_COMMAND_VALUE_WRITE;
	object_len = data_table.search_object(device_data._dest_add,object_buff,assoction_index0);
	for (int j = 0; j < object_len; j++)
	{
		uint16_t objet_index = data_table.association_table[assoction_index0[j]].object_index_read();
		group_len = data_table.search_group(objet_index,group_buff,assoction_index);
		for (int i = 0; i < group_len; i++)
		{
			//LXCan_log_d("matercontrol","object:%d %d",objet_index,data_table.association_table[assoction_index[i]].assocication_reg_read(0));
			if(data_table.association_table[assoction_index[i]].assocication_reg_read(0) == MasterControl_GroupType)
			{
				uint16_t group_index = data_table.association_table[assoction_index[i]].group_index_read();
				if(data_table.address_table[group_index].read() != device_data._dest_add)
				{
					//LXCan_log_d("matercontrol","1");
					//总控输入设备总控值清零
					uint8_t fun_num = data_table.association_table[assoction_index[i]].object_reg_read(6);
					if(object_buff[i]->Write_flag_read() == LXCan_ENABLE)
					{
						if((fun_num == 1)||(fun_num == 2))//总控状态1，开小夜灯
						{
							//LXCan_log_d("matercontrol","2");
							telegram_temp._dest_add = data_table.address_table[group_index].read();
							telegram_temp._value[0] = 3;
							telegram_temp.length = 2;
							_port.txActionList.Append(telegram_temp);
							_port.rxActionList.Append(telegram_temp);
							return 1;
						}
					}
				}
			}
		}	
	}
	return 0;
}

void LXCanDevice::send(DeviceDataType device_data,uint8_t length)
{
	uint8_t frame_num = 0;
	TelegramType telegram_temp;
	if(length == 0)
		frame_num = 0;
	else if(length <= 7)
		frame_num = 1;
	else
		frame_num = ((length - 7) / 6) + 2;
	if(frame_num <= 1 )
	{
		//不分段
		telegram_temp.CAN_ID = device_data.CAN_ID;
		telegram_temp._service = device_data.service;
		telegram_temp._frame_flag = Frame_None;
		for (int i = 0; i < length; i++)
		{
			telegram_temp._value[i] = device_data.value[i];
		}
		telegram_temp.length = length;
		//LXCan_log_d("Device","id:%x",telegram_temp.CAN_ID);
		_port.txActionList.Append(telegram_temp);
	}
	else
	{
		//分段发送
	}
}
//1ms 周期性执行
void LXCanDevice::heart_task()
{
	TelegramType telegram_temp;
	telegram_temp._repetition_flag = LXCan_DISABLE;
	telegram_temp._ack_flag = LXCan_DISABLE;
	telegram_temp._dest_add = 0;
	telegram_temp._type_add = Physical_Add;
	telegram_temp._source_add = data_table.physical_read();
	telegram_temp._frame_flag = Frame_None;
	telegram_temp._service = LXCan_COMMAND_HEATBEAT;
	telegram_temp.length = 1;//发送数据长度 = 服务字节 
	_port.txActionList.Append(telegram_temp);
}
//1ms周期性执行 
void LXCanDevice::ack_task()
{
	TelegramType telegram_temp;
	for (int i = 0; i < data_table.Group_total_read(); i++)
	{
		if(telegram_confirmd_data[i].confirm_num > 1)
			telegram_confirmd_data[i].confirm_num--;
		else if(telegram_confirmd_data[i].confirm_num == 1)
		{
			if(telegram_confirmd_data[i].resend_num > 3)
			{
				telegram_confirmd_data[i].confirm_num = 0;
				telegram_confirmd_data[i].resend_num = 0;
				return ;
			}
			//开始重发
			telegram_temp.CAN_ID = telegram_confirmd_data[i].CAN_ID;
			telegram_temp._service = telegram_confirmd_data[i].service;
			telegram_temp.length = telegram_confirmd_data[i].length_value;
			for (int j = 0; j < telegram_confirmd_data[i].length_value; j++)
			{
				telegram_temp._value[j] = telegram_confirmd_data[i].value[j];

			}
			_port.txActionList.Append(telegram_temp);
		}
		telegram_confirmd_data[i].resend_num++;
	}
}

void LXCanDevice::system_task()
{

	LXCanAddress **group_buff;
	uint8_t group_num;
	uint16_t *assoction_buff;
	TelegramType telegram_temp;
	static uint32_t ack_time = 0;
	static uint32_t can_hearttime = 0;
	//系统消息处理
	//应用程序的发送请求
	for (int i = 0; i < data_table.Object_total_read(); i++)
	{
		if(object_flag[i].Transmission_flag == LXCan_ENABLE)
		{
			//对象的发送使能标志置位
			group_num = data_table.search_group(i,group_buff,assoction_buff);
			if(data_table.object_table[i].Communication_read() == LXCan_ENABLE)//判断对象有无通信属性
			{
				for (int j = 0; j < group_num; j++)//一个对象可能对应多个组地址
				{
					telegram_temp._repetition_flag = LXCan_DISABLE;
					telegram_temp._ack_flag = LXCan_ENABLE;
					telegram_temp._dest_add = group_buff[j]->read();
					telegram_temp._type_add = Group_Add;
					telegram_temp._source_add = data_table.physical_read();
					telegram_temp._frame_flag = Frame_None;
					telegram_temp._service = LXCan_COMMAND_VALUE_WRITE;
					data_table.object_table[i].value_read(telegram_temp._value,telegram_temp.length);
					telegram_temp.length += 1;//发送数据长度 = 服务字节 + 对象值长度
					_port.txActionList.Append(telegram_temp);

				}
			}
			
			object_flag[i].Transmission_flag = Idle_OK;
			object_flag[i].Data_Request_flag = LXCan_DISABLE;
		}
	}
	
	receive_task();
	if(millis() >= (ack_time+1))
	{
		ack_time = millis();
		ack_task();
		_port.send();

		//1s内没有配置文件重新下发，保存配置文件
		if(data_table.savedata_time >=1)
			data_table.savedata_time++;

		if(data_table.savedata_time > 1000)
		{
			elog_set_output_enabled(true);//下发完成，打开日志输出
			LXCan_log_d("task","超时备份开始");
			data_table.backup();
			data_table.savedata_time = 0;
			Device_state = DeviceState_Run;
			LXCan_Device_Reset();
		}
	}
	if((millis() - can_hearttime) > 1000*60*10 )//10min向网关发送心跳
	{
		can_hearttime = millis();
		heart_task();
	}
	if((Device_state == DeviceState_Physical) && (millis() > setphysical_last_time))
	{
		Device_state = DeviceState_Run;
		led_mode_set(Heartbeat_Led);
		LXCan_log_d("task","下发物理地址超时");
	}
}
//应用程序
void LXCanDevice::application_task()
{
	uint8_t funtion_index;
	uint8_t value[8];
	uint8_t value_len;

	if(Device_state != DeviceState_Run)//非运行状态应用程序不执行
		return ;
	//应用程序处理
	for (int i = 0; i < data_table.Object_total_read(); i++)
	{
		if(data_table.object_table[i].Write_flag_read()  == LXCan_ENABLE)//对象有写入属性，为接受对象
		{
			if(object_flag[i].Updata_flag == LXCan_ENABLE)//更新标志被置位
			{
				//funtion_index = data_table.association_table[object_flag[i].assoction_index].assocication_reg_read(0);//关联表的第一个组属性寄存器保存对象的功能号
				funtion_index = data_table.object_table[i].register_read(1);//对象表的第2个组属性寄存器保存对象的功能号
				if(funtion_index >= OBJECT_FUNCATION_SIZE)//功能号越界
					return ;
				value[0] = (uint8_t)i;//对象索引
				value[1] = (uint8_t)(i>>8);
				value[2] = (uint8_t)object_flag[i].assoction_index;//关联表索引
				value[3] = (uint8_t)(object_flag[i].assoction_index >> 8);
				data_table.object_table[i].value_read(&value[4],value_len);
				data_table.object_table[i].callback[funtion_index].call((uint8_t*)value);
				object_flag[i].Updata_flag = LXCan_DISABLE;//清楚更新标志
			}
		}
		else if(data_table.object_table[i].Read_flag_read() == LXCan_ENABLE)//对象有读取指令，为发送对象
		{
			LXCanAddress **group_buff;
			uint16_t *assoction_buff;
			//TelegramType telegram_temp;

			data_table.search_group(i,group_buff,assoction_buff);
			//funtion_index = data_table.association_table[assoction_buff[0]].assocication_reg_read(0);//关联表的第一个组属性寄存器保存对象的功能号
			funtion_index = data_table.object_table[i].register_read(1);//对象表的第2个组属性寄存器保存对象的功能号
			if(funtion_index >= OBJECT_FUNCATION_SIZE)//功能号越界
				return ;
			value[0] = (uint8_t)i;//对象索引
			value[1] = (uint8_t)(i>>8);
			value[2] = (uint8_t)assoction_buff[0];//关联索引
			value[3] = (uint8_t)(assoction_buff[0] >> 8);
			if(data_table.object_table[i].callback[funtion_index].call((uint8_t*)value))
			{
				//将应用函数写入对象值
				value_len = data_table.object_table[i].length_read();
				data_table.object_table[i].value_write(&value[4],value_len);
				object_flag[i].Data_Request_flag = LXCan_ENABLE;//置位发送标志
				object_flag[i].Transmission_flag = Transmit_Request;
			}
		}
		else if(data_table.object_table[i].ACK_flag_read() == LXCan_ENABLE)//接收回复指令
		{

		}
	}

}
void LXCanDevice::send_info(e_LXCanService service)
{
	TelegramType telegram_temp;
	telegram_temp.CAN_ID = 0;
	telegram_temp._buff_data[0] = 0;
	telegram_temp._source_add = data_table.physical_read();
	telegram_temp._service = service;
	telegram_temp._frame_flag = Frame_Firtst;
	telegram_temp._value[0] = 0;
	data_table.Device_mac_read(&telegram_temp._value[1]);
	telegram_temp.length = 8;
	_port.txActionList.Append(telegram_temp);//mac
	telegram_temp._frame_flag = Frame_End;
	telegram_temp._value[0] = 1;
	telegram_temp._value[1] = (uint8_t)data_table.category;
	telegram_temp._value[2] = (uint8_t)(data_table.category>>8);
	telegram_temp._value[3] = data_table.physical_read();
	telegram_temp.length = 5;
	_port.txActionList.Append(telegram_temp);//设备固号+物理地址
}
uint16_t u8Tou16(uint8_t* val)
{
	uint16_t temp  = 0;
	temp = val[0];
	temp <<= 8;
	temp |= val[1];
	return temp;
}
void u16Tou8(uint16_t data, uint8_t* val)
{
	val[0] = (uint8_t)(data >> 8);
	val[1] = (uint8_t)data;
}
void LXCanDevice::config_write(uint8_t *data)
{
	e_LXCanConfigCmd cmd;
	uint8_t *value;
	cmd = (e_LXCanConfigCmd)data[0];
	value = &data[1];
	switch (cmd)
	{
	case LXCan_CONFIG_INFO_PHYSICAL:
		data_table.physical_write(value[0]);
		break;
	case LXCan_CONFIG_INFO_VERSION_SOFTWARE:
		data_table.version_software_write(value);
		break;
	case LXCan_CONFIG_INFO_VERSION_FIRMWARE:
		data_table.version_firmware_write(value);
		break;
	case LXCan_CONFIG_INFO_MAC:
		data_table.Device_mac_write(value);
		break;
	case LXCan_CONFIG_OBJ_TOTAL:
		data_table.Object_total_write(u8Tou16(value));
		break;
	case LXCan_CONFIG_OBJ_DPT_CONFIG:
#warning "索引值要改为16位"
		data_table.object_table[value[0]].dpt_write((e_LXCanDPT_ID)value[1]);
		data_table.object_table[value[0]].config_write(value[2]);
		break;
	case LXCan_CONFIG_OBJ_VAL_DPT:
		data_table.object_table[value[0]].dpt_write((e_LXCanDPT_ID)value[1]);
		break;
	case LXCan_CONFIG_OBJ_CONFIG:
		data_table.object_table[value[0]].config_write(value[1]);
		break;
	case LXCan_CONFIG_OBJ_REGISTER:
		data_table.object_table[value[0]].register_write(value[1],value[2]);
		break;
	case LXCan_CONFIG_GROUP_TOTAL:
		data_table.Group_total_write(u8Tou16(value));
		break;
	case LXCan_CONFIG_GROUP_ADD:
		LXCanGroupAddrType add;
		add = value[1];
		add <<= 8;
		add |= value[2];
		data_table.address_table[value[0]].write(add);
		break;
	case LXCan_CONFIG_RELATE_TOTAL:
		data_table.Association_total_write(u8Tou16(value));
		break;
	case LXCan_CONFIG_RELATE_INDICATOR:
		data_table.association_table[value[0]].group_index_write(*((uint16_t*)(&value[1])));
		data_table.association_table[value[0]].object_index_write(*((uint16_t*)(&value[3])));
		break;
	case LXCan_CONFIG_RELATE_GROUP_INDEX:
		data_table.association_table[value[0]].group_index_write(*((uint16_t*)(&value[1])));
		break;
	case LXCan_CONFIG_RELATE_OBJECT_INDEX:
		data_table.association_table[value[0]].group_index_write(*((uint16_t*)(&value[1])));
		break;
	case LXCan_CONFIG_RELATE_REGISTER:
		data_table.association_table[value[0]].assocication_reg_write(value[1],value[2]);
		break;
	case LXCan_CONFIG_RELATE_OBJ_REGISTER:
		data_table.association_table[value[0]].object_reg_write(value[1],value[2]);
		break;
	default:
		break;
	
	}
	//配置信息自动保存标志
	data_table.savedata_time = 1;
}
//如果读取数据需要索引，从data指针中提取
void LXCanDevice::config_read(DeviceDataType device_data)
{
	TelegramType telegram_temp;
	e_LXCanConfigCmd cmd;
	uint8_t *data ;
	uint16_t temp;
	cmd = (e_LXCanConfigCmd)device_data.value[0];
	data = telegram_temp._value;
	for (int i = 0; i < device_data.length_value; i++)
	{
		data[i] = device_data.value[i+1];
	}
	telegram_temp.CAN_ID = device_data.CAN_ID;
	telegram_temp._source_add = data_table.physical_read();
	telegram_temp._dest_add = 0;

	telegram_temp._service = device_data.service;
	switch (cmd)
	{
	case LXCan_CONFIG_INFO_PHYSICAL:
		data[0] = data_table.physical_read();
		telegram_temp.length = 3;
		break;
	case LXCan_CONFIG_INFO_VERSION_SOFTWARE:
		data_table.version_software_read(data);
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_INFO_VERSION_FIRMWARE:
		data_table.version_firmware_read(data);
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_INFO_MAC:
		data_table.Device_mac_read(data);
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_OBJ_TOTAL:
		data[0] = data_table.Object_total_read();
		telegram_temp.length = 3;
		break;
	case LXCan_CONFIG_OBJ_DPT_CONFIG:
		data[1] = data_table.object_table[data[0]].dpt_read();
		data[2] = data_table.object_table[data[0]].config_read();
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_OBJ_VAL_DPT:
		data[1] = data_table.object_table[data[0]].dpt_read();
		telegram_temp.length = 4;
		break;
	case LXCan_CONFIG_OBJ_CONFIG:
		data[1] = data_table.object_table[data[0]].config_read();
		telegram_temp.length = 4;
		break;
	case LXCan_CONFIG_OBJ_REGISTER:
		data[2] = data_table.object_table[data[0]].register_read(data[1]);
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_GROUP_TOTAL:
		data[0] = data_table.Group_total_read();
		telegram_temp.length = 3;
		break;
	case LXCan_CONFIG_GROUP_ADD:
		LXCanGroupAddrType add;
		add = data_table.address_table[data[1]].read();
		data[1] = add >> 8;
		data[2] = (uint8_t)add;
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_RELATE_TOTAL:
		data[0] = data_table.Association_total_read();
		telegram_temp.length = 3;
		break;
	case LXCan_CONFIG_RELATE_INDICATOR:
		#warning "索引值要改为16位"
		temp = data_table.association_table[data[0]].group_index_read();
		data[1] = (uint8_t)temp;
		data[2] = (uint8_t)(temp >> 8);
		temp = data_table.association_table[data[0]].object_index_read();
		data[3] = (uint8_t)temp;
		data[4] = (uint8_t)(temp >> 8);
		telegram_temp.length = 7;
		break;
	case LXCan_CONFIG_RELATE_GROUP_INDEX:
		temp = data_table.association_table[data[0]].group_index_read();
		data[1] = (uint8_t)temp;
		data[2] = (uint8_t)(temp >> 8);
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_RELATE_OBJECT_INDEX:
		temp = data_table.association_table[data[0]].object_index_read();
		data[1] = (uint8_t)temp;
		data[2] = (uint8_t)(temp >> 8);
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_RELATE_REGISTER:
		data[2] = data_table.association_table[data[0]].assocication_reg_read(data[1]);
		telegram_temp.length = 5;
		break;
	case LXCan_CONFIG_RELATE_OBJ_REGISTER:
		data[2] = data_table.association_table[data[0]].object_reg_read(data[1]);
		telegram_temp.length = 5;
		break;
	default:
		break;

	}
	//发送数据
	_port.txActionList.Append(telegram_temp);
}

//使用组地址查询对象值，值回复组地址索引到的第一个对象的值，但组地址对应多个对象时，后面的对象不回复
uint8_t LXCanDevice::value_read(DeviceDataType device_data)
{
	TelegramType telegram_temp;
	LXCanObject **object_buff;
	uint8_t object_len = 0;
	uint8_t value_len = 0;
	telegram_temp._repetition_flag = LXCan_DISABLE;
	telegram_temp._ack_flag = LXCan_DISABLE;
	telegram_temp._type_add = Group_Add;
	telegram_temp._dest_add = device_data._dest_add;//组地址处理时，使用和接受数据同样的组地址进行回复
	telegram_temp._source_add = data_table.physical_read();
	telegram_temp._priority = device_data._priority;

	telegram_temp._service = LXCan_COMMAND_VALUE_RESPONSE;
	object_len = data_table.search_object(device_data._dest_add,object_buff);
	if(object_len > SEARCH_OBJECT_SIZE)
		return 0;
	//for (int i = 0; i < object_len; i++)
	//{
	//	object_buff[i]->value_read(telegram_temp._value);
	//	telegram_temp.length = object_buff[i]->length_read()+1;//can数据长度 = 服务指令+数据长度
	//	_port.txActionList.Append(telegram_temp);
	//}
	object_buff[0]->value_read(telegram_temp._value,value_len);
	telegram_temp.length = value_len + 1;//can数据长度 = 服务指令+数据长度
	_port.txActionList.Append(telegram_temp);
	
	return 1;
}
uint8_t LXCanDevice::value_write(DeviceDataType device_data)
{
	LXCanObject **object_buff;
	uint8_t object_len = 0;
	uint8_t object_index;
	uint8_t value_len;
	uint16_t *assoction_index;
	object_len = data_table.search_object(device_data._dest_add,object_buff,assoction_index);
	if (object_len > SEARCH_OBJECT_SIZE)
	{
		//索引到的对象数比缓存池大
		return 0;
	}
	for (int i = 0; i < object_len; i++)
	{
		value_len = device_data.length_value;
		object_buff[i]->value_write(device_data.value,value_len);
		object_index = data_table.association_table[assoction_index[i]].object_index_read();
		object_flag[object_index].Updata_flag = LXCan_ENABLE;
		object_flag[object_index].assoction_index = assoction_index[i];
		LXCan_log_d("ReceiveData","Group:%2d,Object[%2d]:%d",device_data._dest_add,object_index,device_data.value[0]);
	}
	return 1;
}

uint8_t LXCanDevice::commit_ack(DeviceDataType device_data)
{
	LXCanObject **object_buff;
	uint8_t object_len = 0;
	uint8_t object_index;
	uint8_t value_len;
	uint16_t *assoction_index;
	object_len = data_table.search_object(device_data._dest_add,object_buff,assoction_index);
	if (object_len > SEARCH_OBJECT_SIZE)
	{
		//索引到的对象数比缓存池大
		return 0;
	}
	else if( object_len == 0)
		return 0;
	for (int i = 0; i < object_len; i++)
	{
		value_len = device_data.length_value;
		object_buff[i]->value_write(&device_data.value[1],value_len);
		object_index = data_table.association_table[assoction_index[i]].object_index_read();
		object_flag[object_index].Ack_flag = LXCan_ENABLE;
		object_flag[object_index].assoction_index = assoction_index[i];
	}
	/********************************************************************/
	uint16_t group_index;
	//回复指令数据备份
	receive_ack_data[group_index].length_value = device_data.length_value;
	for (int i = 0; i < device_data.length_value; i++)
	{
		receive_ack_data[group_index].value[i] = device_data.value[i];
	}
	//回复确认
	telegram_confirmd_data[group_index].confirm_num = 0;

	return 1;
}

void LXCanDevice::physical_control(uint8_t index , uint8_t val)
{
	relay_write(index , val);
	TelegramType ack_data;
	uint16_t object_index = 0xff;
	LXCanAddress **group_buff;
	uint16_t *association_buff;
	//检索端口对应的对象
	for(int i = 0 ; i<data_table.Object_total_read();i++ )
	{
		if(data_table.object_table[i].register_read(0) == index)
		{
			object_index = i;
			break;
		}
	}
	if(object_index < data_table.Object_total_read())
	{
		uint8_t num;
		num = data_table.search_group(object_index,group_buff,association_buff);
		for (uint8_t i = 0; i < num; i++)
		{
			ack_data._buff_data[0] = 0;
			ack_data._repetition_flag = LXCan_DISABLE;
			ack_data._ack_flag = LXCan_DISABLE;
			ack_data._dest_add = group_buff[i]->read();
			ack_data._type_add = Group_Add;
			ack_data._source_add = data_table.physical_read();
			ack_data._priority = LXCAN_PRIORITY_NORMAL_VALUE;
			ack_data._service = LXCan_COMMAND_ACK;
			ack_data._value[0] = LXCan_COMMAND_VALUE_WRITE;
			ack_data._value[1] = index;
			ack_data._value[2] = val;
			ack_data.length = 4;
			_port.txActionList.Append(ack_data);
		}
	}
	
}

void LXCanDevice::apply_cfg()
{
	uint8_t mac[6];
	data_table.Device_mac_read(mac);

	TelegramType telegram_temp;
	telegram_temp.CAN_ID = 0;
	telegram_temp._buff_data[0] = 0;
	telegram_temp._repetition_flag = LXCan_DISABLE;
	telegram_temp._ack_flag = LXCan_DISABLE;
	telegram_temp._type_add = Physical_Add;
	telegram_temp._dest_add = 0x00;//
	telegram_temp._source_add = data_table.physical_read();
	telegram_temp._priority = 0;
	telegram_temp._service = LXCan_COMMAND_RUNSTATE;
	
	telegram_temp._frame_flag = Frame_Firtst;
	telegram_temp._value[0] = 0;
	telegram_temp._value[1] = DeviceState_ApplyCfg;
	telegram_temp._value[2] = (uint8_t)data_table.category;
	telegram_temp._value[3] = (uint8_t)(data_table.category>>8);
	telegram_temp._value[4] = mac[0];
	telegram_temp._value[5] = mac[1];
	telegram_temp._value[6] = mac[2];
	telegram_temp.length = 8;
	_port.txActionList.Append(telegram_temp);

	telegram_temp._frame_flag = Frame_End;
	telegram_temp._value[0] = 1;
	telegram_temp._value[1] = mac[3];
	telegram_temp._value[2] = mac[4];
	telegram_temp._value[3] = mac[5];
	telegram_temp.length = 5;
	_port.txActionList.Append(telegram_temp);
}
