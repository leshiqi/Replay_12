#ifndef __LXCANOBJECT_H
#define __LXCANOBJECT_H
#include "LXCanConfig.h"
#include "LXCanTelegram.h"
#include "LXCanDPT.h"
#include "FunctionPointer.h"
#define LXCAN_COM_OBJECT_OK       0
#define LXCAN_COM_OBJECT_ERROR    255
// Values returned by the KnxDevice member functions :
enum e_LXCanDeviceStatus {
	LXCAN_DEVICE_OK = 0,
	LXCAN_DEVICE_NOT_IMPLEMENTED = 254,
	LXCAN_DEVICE_ERROR = 255
};
enum OBJECT_CONFIG_TYPE
{
	Priority,
	Communication,
	Read,
	Write,
	Monery,
	Transimit
};

//typedef struct 
//{
//	 e_LXCanDPT_ID Dpt_ID;        //数据类型
//	 union 
//	 {
//		 uint8_t Config;
//		 struct 
//		 {
//			 uint8_t Transmission_Priority : 2;
//			 uint8_t Communication_flag : 1;
//			 uint8_t Read_flag : 1;
//			 uint8_t Write_flag : 1;
//			 uint8_t Value_flag : 1;
//			 uint8_t Transimit_flag : 1;
//		 };
//	 };
//	union {
//		uint8_t value;
//		uint16_t shortvalue;
//		uint32_t longvalue;
//		uint8_t value_buff[4];
//	};
//	uint8_t _register[OBJECT_REGISTER_SIZE];//对象的固有属性
//}ObjectDescriptor;

class LXCanMemery
{
private:
	uint8_t buff[OBJECT_MEMORY_DATA_SIZE];
	uint16_t _head;
	
public:
	LXCanMemery()
	   {
		   for(uint16_t i=0;i<OBJECT_MEMORY_DATA_SIZE;i++)
			   buff[i] = 0;
		   _head = 0;
	   }
	uint8_t* malloc(uint8_t _size){
		uint8_t *_data =  &buff[_head];
		_head += _size;
		return _data;
	}
	uint8_t free(uint8_t*)
	{
		return 1;//目前对象值内存固定分配，不释放
	}
	uint32_t addr_read(){
		return (uint32_t)buff;
	}
	void init(uint8_t * data){
		for (int i = 0; i < OBJECT_MEMORY_DATA_SIZE; i++)
		{
			buff[i] = data[i];
		}
	}

};
enum e_LXCanMemoryType{
	CAN_Value_ROM,
	CAN_Value_Flash
};
enum e_LXCanTransmissionPriority{
	LXCAN_OBJECT_PRIORITY_SYSTEM = 0  ,
	LXCAN_OBJECT_PRIORITY_HIGH    ,
	LXCAN_OBJECT_PRIORITY_ALARM   ,
	LXCAN_OBJECT_PRIORITY_NORMAL
};

typedef FunctionPointerArg1<unsigned char, unsigned char*> Object_callback_t;

class LXCanObject{
private:
	e_LXCanDPT_ID Dpt_ID;        //数据类型
	union 
	{
		uint8_t Config;
		struct 
		{
			uint8_t Transmission_Priority : 2;
			uint8_t Communication_flag : 1;
			uint8_t Read_flag : 1;
			uint8_t Write_flag : 1;
			uint8_t ACK_flag : 1;
			uint8_t Value_menory : 1;
			uint8_t Transimit_flag : 1;
		};
	};
	uint8_t* value_buff;
	uint8_t _register[OBJECT_REGISTER_SIZE];//对象的固有属性
	uint8_t value_len;
	static LXCanMemery datamemery;
public:
	Object_callback_t callback[OBJECT_FUNCATION_SIZE];
	uint16_t scene_group;
	LXCanObject()
	{
		value_buff = NULL;
		Dpt_ID = LXCAN_DPT_1_001;
		Config = 0;
		value_len = 0;
		for(uint8_t i=0; i<OBJECT_REGISTER_SIZE ; i++)
			_register[i] = 0;
	}
	e_LXCanDPT_ID dpt_read(){
		return Dpt_ID;
	}
	void dpt_write(e_LXCanDPT_ID id){
		

		Dpt_ID = id;
		value_len =  LXCanDPTFormatToLengthBit[LXCanDPTIdToFormat[id]];
		value_len = ((value_len -1)/8) + 1;
		value_buff = datamemery.malloc(value_len);
	}
	e_LXCanTransmissionPriority Transmission_read(){
		return (e_LXCanTransmissionPriority)Transmission_Priority;
	}
	void Transmission_write(e_LXCanTransmissionPriority pri){
		Transmission_Priority = pri;
	}
	e_FunctionalState Communication_read(){
		return (e_FunctionalState) Communication_flag;
	}
	void Communication_write(e_FunctionalState newflag){
		Communication_flag = newflag;
	}
	e_FunctionalState Read_flag_read(){
		return (e_FunctionalState)Read_flag;
	}
	void Read_flag_write(e_FunctionalState newflag){
		Read_flag = newflag;
	}
	e_FunctionalState Write_flag_read(){
		return (e_FunctionalState)Write_flag;
	}
	void Write_flag_write(e_FunctionalState newflag){
		Write_flag = newflag;
	}
	e_FunctionalState ACK_flag_read(){
		return (e_FunctionalState)ACK_flag;
	}
	void ACK_flag_write(e_FunctionalState newflag){
		ACK_flag = newflag;
	}
	e_LXCanMemoryType Value_memory_read(){
		return (e_LXCanMemoryType)Value_menory;
	}
	void Value_menory_write(e_LXCanMemoryType newmemory){
		Value_menory = newmemory;
	}
	e_FunctionalState Transimit_read()	{
		return (e_FunctionalState) Transimit_flag;
	}
	void Transimit_write(e_FunctionalState newstate){
		Transimit_flag = newstate;
	}

	uint8_t config_read(){
		return Config;
	}
	void config_write(uint8_t data){
		Config = data;
	}
	uint8_t length_read(){
		return value_len;
	}
	uint8_t register_read(uint8_t index){
		if(index < OBJECT_REGISTER_SIZE)
			return _register[index];
		return 0;
	}
	void register_write(uint8_t index,uint8_t data){
		if(index < OBJECT_REGISTER_SIZE)
			_register[index] = data;
	}
	void register_write(uint8_t len,uint8_t *data){
		if(len > OBJECT_REGISTER_SIZE)
			len = OBJECT_REGISTER_SIZE;
		for (uint8_t i=0 ; i<len ; i++)
			_register[i] = data[i];	
	}
	uint32_t valpointer_read(){
		return (uint32_t)value_buff;
	}
	void valpointer_write(uint32_t pointer){
		value_buff = (uint8_t*)pointer;
	}
static	uint32_t memeryadd_read(){
		return datamemery.addr_read();
	}
static void memery_init(uint8_t* data){
		datamemery.init(data);
}
	void value_read(uint8_t* value,uint8_t &length);
	uint8_t value_write(uint8_t *value,uint8_t length);
	template <typename T> 
	e_LXCanDeviceStatus value_read( T& resultValue);
	template <typename T> 
	e_LXCanDeviceStatus value_write(T originValue);

};
typedef void (*Flash_callback_type)(uint32_t WriteAddr,uint16_t *pBuffer,uint16_t NumToWrite);	

extern Flash_callback_type LXCAN_FLASH_Read;
extern Flash_callback_type LXCAN_FLASH_Write;
void object_test();
#endif
