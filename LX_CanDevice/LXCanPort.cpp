#include "LXCanPort.h"
//#include "LXCanDevice.h"
LXCanTelegram LXCanPort::telegram;
ActionRingBuffer<TelegramType, ACTIONS_RECEIVE_QUEUE_SIZE> LXCanPort::rxActionList = ActionRingBuffer<TelegramType, ACTIONS_RECEIVE_QUEUE_SIZE>();
ActionRingBuffer<TelegramType, ACTIONS_SEND_QUEUE_SIZE>    LXCanPort::txActionList = ActionRingBuffer<TelegramType, ACTIONS_SEND_QUEUE_SIZE>();
e_CanTxState LXCanPort::tx_state = TX_INIT;
e_CanRxState LXCanPort::rx_state = RX_INIT;
LXCanPhysicalAddrType LXCanPort::Physical_Add = 0;
LXCanPort::LXCanPort(CAN* port)
{
	this->_canport = port;
	//this->Physical_Add = addr;
}

void LXCanPort::init()
{
	_canport->begin(BSP_CAN_125KBPS);
	_canport->attach_interrupt_rx0(LXCanPort::receive);
	tx_state = TX_IDLE;
	rx_state = RX_IDLE_WAITING_FOR_CTRL_FIELD;
}

void LXCanPort::receive()
{
	TelegramType RX_Telegram;
	telegram.begin(&RX_Telegram);
	telegram.clear();
	telegram.ID_write(Rx0_Message.ExtId);
	telegram.buff_write(Rx0_Message.DLC,Rx0_Message.Data);
	//LXCan_log_d("LXCanPort Receive","ID:%x,len:%d,data[0-7]:%x,%x,%x,%x,%x,%x,%x",Rx0_Message.ExtId,Rx0_Message.DLC,Rx0_Message.Data[0],Rx0_Message.Data[1],Rx0_Message.Data[2],Rx0_Message.Data[3],Rx0_Message.Data[4],Rx0_Message.Data[5],Rx0_Message.Data[6],Rx0_Message.Data[7]);
	//LXCan_log_d("LXCanPort Receive","ID.add_type:%d dest:%d , source:%d",telegram.TypeAdd_read() ,telegram.dest_read(),telegram.source_read());
	if(Group_Add == telegram.TypeAdd_read())
	{	
		rxActionList.Append(RX_Telegram);
		//״̬����
		rx_state_write(RX_CAN_TELEGRAM_RECEPTION_STARTED);
		
	}
	else
	{
		if(( (LXCanPhysicalAddrType)telegram.dest_read() == Physical_Add) || ((LXCanPhysicalAddrType)telegram.dest_read() == 0xff))
		{
			telegram.ID_write(Rx0_Message.ExtId);
			telegram.buff_write(Rx0_Message.DLC,Rx0_Message.Data);
			rxActionList.Append(RX_Telegram);
			//״̬����
			rx_state_write(RX_CAN_TELEGRAM_RECEPTION_STARTED);
		}
		else
			rx_state_write(RX_CAN_TELEGRAM_RECEPTION_NOT_ADDRESSED);
	}

}

void LXCanPort::send()
{
	TelegramType TX_Telegram;
	telegram.begin(&TX_Telegram);
	//if(tx_state_read() == TX_TELEGRAM_SENDING_ONGOING)
	{
		if(txActionList.Pop(TX_Telegram))
		{
			_canport->write(TX_Telegram.CAN_ID,TX_Telegram._buff_data,TX_Telegram.length);
			if(LXCan_ENABLE == telegram.ACK_read())
				tx_state_write(TX_WAITING_ACK);
			else
				tx_state_write(TX_IDLE);
			telegram.clear();
		}
	}
}

e_CanTxState LXCanPort::tx_state_read()
{
	return tx_state;
}

void LXCanPort::tx_state_write(e_CanTxState newstate)
{
	tx_state = newstate;
}

e_CanRxState LXCanPort::rx_state_read()
{
	return rx_state;
}

void LXCanPort::rx_state_write(e_CanRxState newstate)
{
	rx_state = newstate;
}
//CAN can1(CAN1,&PB8,&PB9);
//LXCanPort PortTest(&can1,0x13);
//void Portwrite_test()
//{
//	uint8_t buff[] = {1,2,3,4,5,6,7,8};
//	PortTest.init();
//	TelegramType telegramdata;
//	LXCanTelegram class_telegram;
//	class_telegram.begin(&telegramdata);
//	class_telegram.ID_write(0x1234);
//	class_telegram.buff_write(6,buff);
//	PortTest.tx_state_write(TX_TELEGRAM_SENDING_ONGOING);
//	PortTest.txActionList.Append(telegramdata);
//	PortTest.txActionList.Append(telegramdata);
//	PortTest.send();
//}
