#ifndef __LXCANDATABASE_H_
#define __LXCANDATABASE_H_
#include "LXCanConfig.h"
#include "LXCanObject.h"
#include "LXCanAddress.h"
#include "LXCanAssocicationl.h"
enum e_LXCanTableType{
	LXCan_Flash_Info,
	LXCan_Flash_Group,
	LXCan_Flaah_Object,
	LXCan_Flash_Association,
};
enum e_LXCanTime{
	Time_None,
	Time_Day,
	Time_Night
};
enum e_LXCanCardState{
	NO_Card,
	Add_Card
};
enum e_EnergyMode{
	OFF_Energy,
	ON_Energy
};
enum e_NightLightMode{
	OFF_NightLight,
	ON_NightLight
};
//关联属性类型
//组类型
enum e_GroupType_AssocicationReg0{
	Light_GroupType,
	Dimming_GroupType,
	CurtainOpen_GroupType,
	CurtainClose_GroupType,
	GauzeOpen_GroupType,
	GauzeClose_GroupType,
	AirConditioner_GroupType,
	Fan_GroupType,
	RoomService_GroupType,
	MasterControl_GroupType,
	Scene_GroupType,
	Other_GroupType
};
enum e_Card__AssocicationReg1
{
	Vaild_Card,
	InVaild_Card,
	UnCard
};
enum e_TimeMode__AssocicationReg2
{
	DayValid,
	NightValid,
	AllDayValid
};
enum e_Memory__AssocicationReg3
{
	DisMemory,
	EnMemory
};
enum e_NightLight__AssocicationReg4
{
	DisNightLight,
	EnNightLight
};
typedef struct 
{
	e_LXCanTime time_flag;
	e_LXCanCardState card_flag;
	e_EnergyMode energy_flag;
	e_NightLightMode nightlight_flag;
}SystemState_Type;
class LXCanTable
{
public:
	static const uint16_t category = DEVICE_CATEGORY;
	uint16_t savedata_time;
	LXCanTable();
	void init();
	void backup();
	void backup(e_LXCanTableType table_type);
	uint8_t recovery();
	uint8_t recovery(e_LXCanTableType table_type);
	uint8_t search_object(LXCanGroupAddrType addr,class LXCanObject **(&target_object));
	uint8_t search_object(LXCanGroupAddrType addr,class LXCanObject **(&target_object) ,uint16_t *(&assoction_index));
	uint8_t search_group(class LXCanObject *object,class LXCanAddress **(&target_group));
	uint8_t search_group(uint16_t object_index,class LXCanAddress **(&target_group),uint16_t *(&assoction_buff));

	LXCanPhysicalAddrType physical_read(){
		return Device_Physical;
	}
	void physical_write(LXCanPhysicalAddrType addr){
		Device_Physical = addr;
		backup(LXCan_Flash_Info);
	}
	void version_firmware_read(uint8_t* version)
	{
		for (int i = 0; i < 3; i++)
		{
			 version[i] = version_firmware[i];
		}
	}
	void version_firmware_write(uint8_t* version)
	{
		for (int i = 0; i < 3; i++)
		{
			version_firmware[i] = version[i];
		}
	}
	void version_software_read(uint8_t* version)
	{
		for (int i = 0; i < 3; i++)
		{
			version[i] = version_software[i];
		}
	}
	void version_software_write(uint8_t* version)
	{
		for (int i = 0; i < 3; i++)
		{
			version_software[i] = version[i];
		}
	}
	void Device_mac_read(uint8_t* mac)
	{
		for (int i = 0; i < 6; i++)
		{
			mac[i] = Device_mac[i];
		}
	}
	void Device_mac_write(uint8_t* mac)
	{
		for (int i = 0; i < 6; i++)
		{
			Device_mac[i] = mac[i];
		}
	}
	uint8_t Device_mac_check(uint8_t* mac)
	{
		for (int i = 0; i < 6; i++)
		{
			if(mac[i] != Device_mac[i])
				return 0;
		}
		return 1;
	}
	uint16_t Object_total_read(){
		return object_total;
	}
	void Object_total_write(uint16_t num){
		object_total = num;
	}
	uint16_t Group_total_read(){
		return address_total;
	}
	void Group_total_write(uint16_t num){
		address_total = num;
	}
	uint16_t Association_total_read(){
		return association_total;
	}
	void Association_total_write(uint16_t num){
		association_total = num;
	}
	class LXCanAddress address_table[GROUPADD_SIZE];
	class LXCanObject object_table[OBJECT_SIZE];
	class LXCanAssocication association_table[ASSOCICATION_SIZE];
	SystemState_Type systemstate;
private:
	class LXCanObject* object_buff[SEARCH_OBJECT_SIZE];//搜索对象缓存
	class LXCanAddress* address_buff[SEARCH_GROUP_SIZE];//搜索地址缓存
	uint16_t assoction_index_buff[SEARCH_ASSOCTION_SIZE];//搜索关联表索引缓存
	LXCanPhysicalAddrType Device_Physical;
	uint8_t version_software[3];
	uint8_t version_firmware[3];
	uint8_t Device_mac[6];

	uint16_t address_total;
	uint16_t object_total;
	uint16_t association_total;
	uint16_t buff_temp[1024];
};
typedef union 
{
	#pragma anon_unions
	struct 
	{
		uint8_t dpt_id;
		uint8_t config;
		uint8_t reg[OBJECT_REGISTER_SIZE];
		uint32_t datapointer;
	};
	uint16_t buff[(OBJECT_REGISTER_SIZE/2)+3];
}_object_flash;
typedef union 
{
#pragma anon_unions
	LXCanGroupAddrType addr;
	uint16_t buff[1];
}_group_flash;
typedef union 
{
#pragma anon_unions
	struct 
	{
		uint8_t group_index;
		uint8_t object_index;
		uint8_t assocication_reg[ASSOCICATION_REGISTER_SIZE];
		uint8_t object_reg[ASSOCICATION_OBJECT_REGISTER_SIZE];
	};
	uint16_t buff[(ASSOCICATION_REGISTER_SIZE + ASSOCICATION_OBJECT_REGISTER_SIZE)/2+1];
}_association_flash;
typedef union{
	struct 
	{
		uint8_t version_software[4];
		uint8_t version_firmware[4];

		uint16_t physical;
		uint16_t address_length;
		uint16_t object_length;
		uint16_t association_length;
	};
	uint16_t buff[8];
}_info_flash;
void Database_test();

#endif












