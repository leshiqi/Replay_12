#ifndef __LXCANADDRESS_H_
#define __LXCANADDRESS_H_
#include "LXCanConfig.h"

class LXCanAddress
{
private:
	LXCanGroupAddrType addr; 
public:
	LXCanAddress()
	{
		;
	}
	LXCanGroupAddrType read()
	{
		return addr;
	}
	void write(LXCanGroupAddrType newadd)
	{
		this->addr = newadd;
	}
};
#endif

