#ifndef __LXCANASSOCICATION_H
#define __LXCANASSOCICATION_H
#include "LXCanConfig.h"

class LXCanAssocication{
private:
	uint16_t group_index;
	uint16_t object_index;
	uint8_t assocication_reg[ASSOCICATION_REGISTER_SIZE];
	uint8_t object_reg[ASSOCICATION_OBJECT_REGISTER_SIZE];
public:
	LXCanAssocication()
	{
		;
	}
	uint16_t group_index_read()
	{
		return group_index;
	}
	void group_index_write(uint16_t index)
	{
		group_index = index;
	}
	uint16_t object_index_read()
	{
		return object_index;
	}
	void object_index_write(uint16_t index)
	{
		object_index = index;
	}
	uint8_t assocication_reg_read(uint8_t index)
	{
		if(index < ASSOCICATION_REGISTER_SIZE)
			return assocication_reg[index];
		return 0;
	}
	void assocication_reg_write(uint8_t index,uint8_t data)
	{
		if(index < ASSOCICATION_REGISTER_SIZE)
		{
			assocication_reg[index] = data;
		}
	}
	uint8_t object_reg_read(uint8_t index)
	{
		if(index < ASSOCICATION_OBJECT_REGISTER_SIZE)
			return object_reg[index];
		return 0;
	}
	void object_reg_write(uint8_t index, uint8_t data)
	{
		if(index < ASSOCICATION_OBJECT_REGISTER_SIZE)
			object_reg[index] = data;
	}
};

#endif

