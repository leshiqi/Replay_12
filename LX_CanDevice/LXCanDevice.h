#ifndef __LXCANDEVICE_H
#define __LXCANDEVICE_H
#include "LXCanConfig.h"
#include "LXCanObject.h"
#include "LXCanTelegram.h"
#include "ActionRingBuffer.h"
#include "LXCanPort.h"
#include "LXCabDatabase.h"
#define SetPhysicaWrite 10000   //设置物理地址时间 10s
enum e_TransmitStete{
	Idle_OK,
	Idle_Error,
	Transmitting,
	Transmit_Request
};

//设备运行状态
enum e_DeviceState{
	DeviceState_Run,
	DeviceState_Debug,
	DeviceState_Physical,
	DeviceState_Boot,
	DeviceState_ApplyCfg
};

typedef struct 
{
	uint8_t Transmission_flag : 2;
	uint8_t Data_Request_flag : 1;
	uint8_t Updata_flag : 1;
	uint8_t Ack_flag : 1;
	uint8_t : 3;
	uint8_t assoction_index;//对象所在关联表的索引
}Comms_Flag;

typedef struct 
{
#pragma anon_unions
	union {
		uint32_t CAN_ID;
		struct {
			uint32_t : 6;
			uint32_t  _repetition_flag : 1; //重复位
			uint32_t  _ack_flag  : 1;    //应达位
			uint32_t  _dest_add : 10;  //目的地址
			uint32_t  _type_add : 1;  //物理，组地址
			uint32_t  _source_add : 8;    //源地址
			uint32_t  _priority : 2;     //优先级
			uint32_t : 3;
		};
	};
	e_LXCanService service;
	uint8_t length_value;
	uint8_t *value;
}DeviceDataType;
typedef struct 
{
	LXCanGroupAddrType addr;
	uint8_t length_value;
	uint8_t value[7];
}DeviceReciiveAckType;
typedef struct 
{
#pragma anon_unions
	union {
		uint32_t CAN_ID;
		struct {
uint32_t : 6;
			uint32_t  _repetition_flag : 1; //重复位
			uint32_t  _ack_flag  : 1;    //应达位
			uint32_t  _dest_add : 10;  //目的地址
			uint32_t  _type_add : 1;  //物理，组地址
			uint32_t  _source_add : 8;    //源地址
			uint32_t  _priority : 2;     //优先级
uint32_t : 3;
		};
	};
	e_LXCanService service;
	uint8_t length_value;
	uint8_t value[7];
	uint8_t confirm_num;
	uint8_t resend_num;
}TelegramConfirmType;
typedef uint8_t (*Object_Funcation_Type)(uint8_t *);


class LXCanDevice{
private:
	Comms_Flag object_flag[OBJECT_SIZE];
	uint32_t setphysical_last_time;

	uint8_t receive_physical(DeviceDataType device_data);
	uint8_t receive_group(DeviceDataType device_data);
	uint8_t matercontrol(DeviceDataType device_data);
	void config_write(uint8_t *data);
	void config_read(DeviceDataType device_data);
	uint8_t value_read(DeviceDataType device_data);
	uint8_t value_write(DeviceDataType device_data);
	uint8_t commit_ack(DeviceDataType device_data);
public:
	e_DeviceState Device_state;
	LXCanDevice(CAN* port);
	DeviceReciiveAckType receive_ack_data[GROUPADD_SIZE];//回复数据接收缓存
	TelegramConfirmType telegram_confirmd_data[GROUPADD_SIZE];//发送数据等待回复确认备份
	LXCanTable data_table;
	LXCanPort _port;
	uint8_t receive_task();
	void init();
	void send(DeviceDataType device_data,uint8_t length);
	void heart_task();
	void ack_task();
	void system_task();
	void application_task();
	void object_Callback_init(uint8_t object_index,uint8_t fun_index,Object_Funcation_Type fun)
	{
		data_table.object_table[object_index].callback[fun_index].attach(fun);
	}
	void send_info(e_LXCanService);
	void physical_control(uint8_t index , uint8_t val);
	void apply_cfg();
};

#endif


