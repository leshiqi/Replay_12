
#ifndef __LXCANPORT_H
#define __LXCANPORT_H
#include "LXCanConfig.h"
#include "LXCanTelegram.h"
#include "LXCanObject.h"
#include "ActionRingBuffer.h"
enum type_LXCanTpUartMode { NORMAL,
	BUS_MONITOR };

// Definition of the TP-UART events sent to the application layer
enum e_LXCanEvent { 
	LXCAN_EVENT_RESET = 0,                // 接收到复位数据
	LXCAN_EVENT_RECEIVED_TELEGRAM,        // 接受到一个新的LXCAN报文
	LXCAN_EVENT_TELEGRAM_RECEPTION_ERROR, // 一个新的CAN报文接受失败
	LXCAN_EVENT_STATE_INDICATION          // 一个新的接收状态
};

// RX states
enum e_CanRxState {
	RX_INIT,                                  // 等待初始化
	RX_IDLE_WAITING_FOR_CTRL_FIELD,           // 空闲
	RX_CAN_TELEGRAM_RECEPTION_STARTED,        // 开始接收
	RX_CAN_TELEGRAM_RECEPTION_ADDRESSED,      // 正确接受
	RX_CAN_TELEGRAM_RECEPTION_NOT_ADDRESSED   // 地址错误
};

// Transmission states
enum e_CanTxState {
	TX_INIT,                     // The TX part is awaiting init execution
	TX_IDLE,                     // Idle, no transmission ongoing kongxian 空闲
	TX_TELEGRAM_SENDING_ONGOING, // EIB telegram transmission ongoing 正在传输
	TX_WAITING_ACK               // Telegram transmitted, waiting for ACK/NACK
};
class LXCanPort {
private:
	CAN* _canport;
	static LXCanPhysicalAddrType Physical_Add;
	static e_CanTxState tx_state;
	static e_CanRxState rx_state;
	static LXCanTelegram telegram;
public:
	static ActionRingBuffer<TelegramType, ACTIONS_RECEIVE_QUEUE_SIZE> rxActionList; // 接受数据的队列
	static ActionRingBuffer<TelegramType, ACTIONS_SEND_QUEUE_SIZE>    txActionList; // 发送数据的队列

	LXCanPort(CAN* port);
	void init();
	static void receive();
	void send();
	static e_CanTxState tx_state_read();
	static void tx_state_write(e_CanTxState newstate);
	static e_CanRxState rx_state_read();
	static void rx_state_write(e_CanRxState newstate);
	void physical_write(LXCanPhysicalAddrType addr){
		Physical_Add = addr;
	}
};

void Portwrite_test();
#endif

