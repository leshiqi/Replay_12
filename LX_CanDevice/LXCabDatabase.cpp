#include "LXCabDatabase.h"
Flash_callback_type LXCAN_FLASH_Read  = NULL;
Flash_callback_type LXCAN_FLASH_Write = NULL;

LXCanTable::LXCanTable()
{
	savedata_time = 0;
	Device_Physical = DEVICE_CATEGORY - ((DEVICE_CATEGORY / 100) * 100)+100;
}

void LXCanTable::init()
{
	if(LXCAN_FLASH_Read == NULL)
	{
		for(;;);
	}
	if(LXCAN_FLASH_Write == NULL)
	{
		for(;;);
	}
	if(recovery())
	{
		//从flash恢复数据
	}
	else
	{
		//初始化所有数据
		address_total = 0;
		object_total = 0;
		association_total = 0;
	}
	for (int i = 0; i < 10; i++)
	{
		object_buff[i] = NULL;
		address_buff[i] = NULL;
	}
}

void LXCanTable::backup()
{
	LXCan_log_i("DataBase","数据备份中...");
	backup(LXCan_Flash_Info);
	backup(LXCan_Flaah_Object);
	backup(LXCan_Flash_Group);
	backup(LXCan_Flash_Association);
}

void LXCanTable::backup(e_LXCanTableType table_type)
{
	_info_flash* info_data;
	_object_flash* object_data;
	_group_flash* group_data;
	_association_flash* association_data;

	int page_size = 1024 / sizeof(_object_flash);
	int index_page_start = 0;
	uint32_t flash_base = OBJECT_TABLE_BASE;

	switch (table_type)
	{
	case LXCan_Flash_Info:
		info_data = (_info_flash*)(&buff_temp[0]);
		for (int i = 0; i < 3; i++)
		{
			info_data->version_software[i] = version_software[i];
			info_data->version_firmware[i] = version_firmware[i];
		}
		info_data->address_length = address_total;
		info_data->object_length = object_total;
		info_data->association_length = association_total;
		info_data->physical = Device_Physical;
		LXCAN_FLASH_Write(DEVICE_INFO_BASE,buff_temp,sizeof(_info_flash));
		break;
	case LXCan_Flash_Group:
		//组地址表备份
		LXCan_log_i("地址表备份","地址表数量：%d",address_total);
		for (int i = 0; i < address_total; i++)
		{
			group_data = (_group_flash*)(&buff_temp[i]);
			group_data->addr = address_table[i].read();
			LXCan_log_i("GroupTable","index:%02d，Add:%02d",i,group_data->addr);
		}
		LXCAN_FLASH_Write(GROUPADD_TABLE_BASE,buff_temp,address_total*sizeof(_group_flash));
		break;
	case LXCan_Flaah_Object:
		//对象数据区备份
		LXCAN_FLASH_Write(OBJECT_DATA_BASE,(uint16_t*)LXCanObject::memeryadd_read(),OBJECT_MEMORY_DATA_SIZE);
		//对象表备份
		page_size = 1024 / sizeof(_object_flash);
		index_page_start = 0;
		flash_base = OBJECT_TABLE_BASE;

		LXCan_log_i("对象表备份","对象表数量：%d",object_total);
		for (int page = 0; page < (object_total / page_size); page++)
		{
			for (int i = 0; i < page_size; i++)
			{
				//使用数据指针对缓存区数据写入,避免拷贝
				object_data = (_object_flash *)(&buff_temp[i*(sizeof(_object_flash)/2)]);

				object_data->dpt_id = object_table[i+index_page_start].dpt_read();
				object_data->config =  object_table[i+index_page_start].config_read();
				object_data->datapointer =  object_table[i+index_page_start].valpointer_read();
				for (int j = 0; j < OBJECT_REGISTER_SIZE; j++)
				{
					object_data->reg[j] = object_table[i+index_page_start].register_read(j);
				}
			}
			LXCAN_FLASH_Write(flash_base,buff_temp,1024);
			flash_base += 1024;
			index_page_start += page_size;
		}
		for (int i = 0; i < object_total % page_size; i++)
		{
			//使用数据指针对缓存区数据写入,避免拷贝
			object_data = (_object_flash *)(&buff_temp[i*(sizeof(_object_flash)/2)]);

			object_data->dpt_id = object_table[i+index_page_start].dpt_read();
			object_data->config =  object_table[i+index_page_start].config_read();
			object_data->datapointer =  object_table[i+index_page_start].valpointer_read();
			for (int j = 0; j < OBJECT_REGISTER_SIZE; j++)
			{
				object_data->reg[j] = object_table[i+index_page_start].register_read(j);
			}
			LXCan_log_i("ObjectTable","index:%02x，Dpt:%02x,config:%02x,reg0:%02x,reg1:%02x,reg2:%02x,reg3:%02x,",i,object_data->dpt_id,object_data->config,object_data->reg[0],object_data->reg[1],object_data->reg[2],object_data->reg[3]);
		}
		LXCAN_FLASH_Write(flash_base,buff_temp,(object_total % page_size)*sizeof(_object_flash));
		break;
	case LXCan_Flash_Association:
		//关联表备份
		page_size = 1024 / sizeof(_association_flash);
		index_page_start = 0;
		flash_base = (uint32_t)ASSOCIATION_TABLE_BASE;
		LXCan_log_i("关联表备份","关联表数量：%d",association_total);
		for (int page = 0; page < (association_total / page_size); page++)
		{
			for (int i = 0; i < page_size; i++)
			{
				association_data = (_association_flash*)(&buff_temp[i*sizeof(_association_flash)/2]);
				association_data->group_index = association_table[i+index_page_start].group_index_read();
				association_data->object_index = association_table[i+index_page_start].object_index_read();
				for (int j = 0; j < ASSOCICATION_REGISTER_SIZE; j++)
				{
					association_data->assocication_reg[j] = association_table[i+index_page_start].assocication_reg_read(j);
				}
				for (int j = 0; j < ASSOCICATION_OBJECT_REGISTER_SIZE; j++)
				{
					association_data->object_reg[j] = association_table[i+index_page_start].object_reg_read(j);
				}
			}
			LXCAN_FLASH_Write(flash_base,buff_temp,1024);
			flash_base += 1024;
			index_page_start += page_size;
		}
		for (int i = 0; i < association_total % page_size ; i++)
		{
			association_data = (_association_flash*)(&buff_temp[i*sizeof(_association_flash)/2]);
			association_data->group_index = association_table[i+index_page_start].group_index_read();
			association_data->object_index = association_table[i+index_page_start].object_index_read();
			for (int j = 0; j < ASSOCICATION_REGISTER_SIZE; j++)
			{
				association_data->assocication_reg[j] = association_table[i+index_page_start].assocication_reg_read(j);
			}
			for (int j = 0; j < ASSOCICATION_OBJECT_REGISTER_SIZE; j++)
			{
				association_data->object_reg[j] = association_table[i+index_page_start].object_reg_read(j);
			}
			LXCan_log_i("AssocitationTable","index:%02d,group_index:%02d,object_index:%02d,obj_reg[0-4]:%02x|%02x|%02x|%02x|%02x|,reg[0-4]::%02x|%02x|%02x|%02x|%02x|",
				i,association_data->group_index,association_data->object_index,
				association_data->object_reg[0],association_data->object_reg[1],association_data->object_reg[2],association_data->object_reg[3],association_data->object_reg[4],
				association_data->assocication_reg[0],association_data->assocication_reg[1],association_data->assocication_reg[2],association_data->assocication_reg[3],association_data->assocication_reg[4]);
		}
		LXCAN_FLASH_Write(flash_base,buff_temp,(association_total % page_size)*sizeof(_association_flash));
		break;
	default:
		break;
	}
}

uint8_t LXCanTable::recovery()
{
	//必须先回复系统信息
	LXCan_log_i("DataBase","数据恢复开始...");
	recovery(LXCan_Flash_Info);
	recovery(LXCan_Flaah_Object);
	recovery(LXCan_Flash_Group);
	return recovery(LXCan_Flash_Association);
	
}

uint8_t LXCanTable::recovery(e_LXCanTableType table_type)
{
	_info_flash *info_data;
	_object_flash *object_data;
	_group_flash *group_data;
	_association_flash *association_data;

	int page_size = 0;
	int index_page_start = 0;
	uint32_t flash_base = 0;
	//判断flash中有无备份数据
	{
		LXCAN_FLASH_Read(DEVICE_INFO_BASE,buff_temp,sizeof(_info_flash)/2);
		 if((0xffff == buff_temp[0])&&(0xffff == buff_temp[1]))
			 return 0;
	}
	
	switch (table_type)
	{
	case LXCan_Flash_Info:
		LXCAN_FLASH_Read(DEVICE_INFO_BASE,buff_temp,sizeof(_info_flash)/2);
		info_data = (_info_flash *)(&buff_temp[0]);
		if((info_data->address_length > GROUPADD_SIZE) || (info_data->object_length > OBJECT_SIZE) || (info_data->association_length > ASSOCICATION_SIZE))
		{
			address_total = 0;
			object_total = 0;
			address_total = 0;
			LXCan_log_e("DataBase","数据恢复出错，请重新擦除flash");
			return 0;
		}
		else
		{
			address_total = info_data->address_length;
			object_total = info_data->object_length;
			association_total = info_data->association_length;
		}
		
		//恢复数据到RAM中
		for (int i = 0; i < 3; i++)
		{
			version_firmware[i] = info_data->version_firmware[i];
			version_software[i] = info_data->version_software[i];
		}
		Device_Physical = info_data->physical;
		break;
	case LXCan_Flash_Group:
		LXCan_log_i("地址表恢复","地址表数量：%d",address_total);
		//从flash读取数据到缓存区
		LXCAN_FLASH_Read(GROUPADD_TABLE_BASE,buff_temp,address_total*sizeof(_group_flash)/2);
		for (int i = 0; i < address_total; i++)
		{
			group_data = (_group_flash*)(&buff_temp[i]);

			address_table[i].write(group_data->addr);
			LXCan_log_i("GroupTable","index:%02d，Add:%02d",i,group_data->addr);
		}
		break;
	case LXCan_Flaah_Object:
		LXCan_log_i("对象表恢复","对象表数量：%d",object_total);
		//对象数据区复原
		LXCAN_FLASH_Read(OBJECT_DATA_BASE,buff_temp,OBJECT_MEMORY_DATA_SIZE/2);
		LXCanObject::memery_init((uint8_t*)buff_temp);
		//对象表复原
		page_size = 1024 / (sizeof(_object_flash));//每个扇区存储的对象数量
		flash_base = OBJECT_TABLE_BASE;
		index_page_start = 0;
		for (int i = 0; i < object_total / page_size; i++)//前 n 个满扇区
		{
			LXCAN_FLASH_Read(flash_base,buff_temp,1024);//从flash读取数据到缓存区
			for (int j = 0; j < page_size; j++)
			{
				//使用数据指针对缓存区数据解析,避免拷贝
				object_data = (_object_flash *)(&buff_temp[j*(sizeof(_object_flash)/2)]);
				//////////////////////////
				object_table[index_page_start+j].dpt_write((e_LXCanDPT_ID)object_data->dpt_id);
				object_table[index_page_start+j].config_write(object_data->config);
				for (int k = 0; k < OBJECT_REGISTER_SIZE; k++)
				{
					object_table[index_page_start+j].register_write(k,object_data->reg[k]); 
				}
			}
			flash_base += 1024;
			index_page_start += page_size;
		}
		//最后一个存储扇区
		LXCAN_FLASH_Read(flash_base,buff_temp,(object_total % page_size)*sizeof(_object_flash));//从flash读取数据到缓存区
		for (int j = 0; j < object_total % page_size; j++)
		{
			//使用数据指针对缓存区数据解析,避免拷贝
			object_data = (_object_flash *)(&buff_temp[j*(sizeof(_object_flash)/2)]);
			//////////////////////////
			object_table[index_page_start+j].dpt_write((e_LXCanDPT_ID)object_data->dpt_id);
			object_table[index_page_start+j].config_write(object_data->config);
			for (int k = 0; k < OBJECT_REGISTER_SIZE; k++)
			{
				object_table[index_page_start+j].register_write(k,object_data->reg[k]); 
			}
			LXCan_log_i("ObjectTable","index:%02x，Dpt:%02x,config:%02x,reg0:%02x,reg1:%02x,reg2:%02x,reg3:%02x,",j,object_data->dpt_id,object_data->config,object_data->reg[0],object_data->reg[1],object_data->reg[2],object_data->reg[3]);
		}
		break;
	case LXCan_Flash_Association:
		
		page_size = 1024 / sizeof(_association_flash);
		LXCan_log_i("关联表恢复","关联表数量：%d page:%d",association_total , page_size);
		flash_base = ASSOCIATION_TABLE_BASE;
		index_page_start = 0;
		//前 n 个满扇区
		for (int i = 0; i < association_total / page_size; i++) 
		{
			LXCAN_FLASH_Read(flash_base,buff_temp,1024);
			for (int j = 0; j < page_size; j++)
			{
				association_data = (_association_flash *)(&buff_temp[j*(sizeof(_association_flash)/2)]);

				association_table[index_page_start + j].group_index_write(association_data->group_index);
				association_table[index_page_start + j].object_index_write(association_data->object_index);
				for (int k = 0; k < ASSOCICATION_REGISTER_SIZE; k++)
				{
					association_table[index_page_start + j].assocication_reg_write(k,association_data->assocication_reg[k]);
				}
				for (int k = 0; k < ASSOCICATION_OBJECT_REGISTER_SIZE; k++)
				{
					association_table[index_page_start + j].object_reg_write(k,association_data->object_reg[k]);
				}
			}
			flash_base += 1024;
			index_page_start += page_size;
		}
		//最后一个存储扇区
		LXCAN_FLASH_Read(flash_base,buff_temp,(association_total % page_size)*sizeof(_association_flash));
		for (int j = 0; j < association_total % page_size; j++)
		{
			association_data = (_association_flash *)(&buff_temp[j*(sizeof(_association_flash)/2)]);

			association_table[index_page_start + j].group_index_write(association_data->group_index);
			association_table[index_page_start + j].object_index_write(association_data->object_index);
			for (int k = 0; k < ASSOCICATION_REGISTER_SIZE; k++)
			{
				association_table[index_page_start + j].assocication_reg_write(k,association_data->assocication_reg[k]);
			}
			for (int k = 0; k < ASSOCICATION_OBJECT_REGISTER_SIZE; k++)
			{
				association_table[index_page_start + j].object_reg_write(k,association_data->object_reg[k]);
			}
			LXCan_log_i("AssocitationTable","index:%02d,group_index:%02d,object_index:%02d,obj_reg[0-4]:%02x|%02x|%02x|%02x|%02x|,reg[0-4]::%02x|%02x|%02x|%02x|%02x|",
				j,association_data->group_index,association_data->object_index,
				association_data->object_reg[0],association_data->object_reg[1],association_data->object_reg[2],association_data->object_reg[3],association_data->object_reg[4],
				association_data->assocication_reg[0],association_data->assocication_reg[1],association_data->assocication_reg[2],association_data->assocication_reg[3],association_data->assocication_reg[4]);
		}
		break;
	default:
		break;
	}
	return 1;
}

uint8_t LXCanTable::search_object(LXCanGroupAddrType addr,class LXCanObject **(&target_object))
{
	int addr_index = 0xff;
	int object_num = 0;
	for (int i = 0; i < address_total; i++)
	{
		if(address_table[i].read() == addr)
		{
			addr_index = i;
			break;
		}
	}
	if(addr_index == 0xff)
		return 0;
	for (int i = 0; i < association_total; i++)
	{
		if(association_table[i].group_index_read() == addr_index)
		{
			object_buff[object_num++] = &object_table[association_table[i].object_index_read()];
		}
	}
	target_object = object_buff;
	return object_num;
}

uint8_t LXCanTable::search_object(LXCanGroupAddrType addr,class LXCanObject **(&target_object),uint16_t *(&assoction_index))
{
	int addr_index = 0xff;
	int object_num = 0;
	for (int i = 0; i < address_total; i++)
	{
		if(address_table[i].read() == addr)
		{
			addr_index = i;
			break;
		}
	}
	if(addr_index == 0xff)
		return 0;
	for (int i = 0; i < association_total; i++)
	{
		if(association_table[i].group_index_read() == addr_index)
		{
			assoction_index_buff[object_num] = i;
			object_buff[object_num] = &object_table[association_table[i].object_index_read()];
			object_num++;
		}
	}
	target_object = object_buff;
	assoction_index = assoction_index_buff;
	return object_num;
}

uint8_t LXCanTable::search_group(class LXCanObject *object,class LXCanAddress **(&target_address))
{
	int object_index = 0;
	int address_num = 0;
	for (int i = 0; i < object_total; i++)
	{
		if(object == &object_table[i])
		{
			object_index = i;
			break;
		}
	}
	for (int i = 0; i < association_total; i++)
	{
		if (association_table[i].object_index_read() == object_index)
		{
			address_buff[address_num++] = &address_table[association_table[i].group_index_read()];
		}
	}
	target_address = address_buff;
	return address_num;
}


uint8_t LXCanTable::search_group(uint16_t object_index,class LXCanAddress **(&target_group),uint16_t *(&assoction_buff))
{
	int address_num = 0;
	for (int i = 0; i < association_total; i++)
	{
		if (association_table[i].object_index_read() == object_index)
		{
			address_buff[address_num] = &address_table[association_table[i].group_index_read()];
			assoction_index_buff[address_num] = i;
			address_num++;
		}
	}
	target_group = address_buff;
	assoction_buff = assoction_index_buff;
	return address_num;
}
uint8_t fun0(uint8_t* data)
{
	LXCan_log_d("Test","fun0,object:%d,value:%d",data[0],data[1]);
	return 1;
}
uint8_t fun1(uint8_t* data)
{
	LXCan_log_d("Test","fun1,object:%d,value:%d",data[0],data[1]);
	return 1;
}

uint8_t fun2(uint8_t* data)
{
	LXCan_log_d("Test","fun2,object:%d,value:%d",data[0],data[1]);
	return 1;
}
uint8_t fun3(uint8_t* data)
{
	LXCan_log_d("Test","fun3,object:%d,value:%d",data[0],data[1]);
	return 1;
}
uint8_t fun4(uint8_t* data)
{
	LXCan_log_d("Test","fun4,object:%d,value:%d",data[0],data[1]);
	return 1;
}

void Database_test()
{
	unsigned char value_temp = 0;
	uint8_t *value = &value_temp;
	uint8_t value_len = 0;
	static LXCanTable data_table;
	LXCanObject **object_buff;
	LXCanAddress **group_buff;
	uint16_t *association_buff;
	LXCan_log_d("Test","object_flash：%d",sizeof(_object_flash));
	LXCan_log_d("Test","group_flash：%d",sizeof(_group_flash));
	LXCan_log_d("Test","associcatiopn_flash：%d",sizeof(_association_flash));
	LXCan_log_d("Test","info_flash：%d",sizeof(_info_flash));
	data_table.Object_total_write(5);
	data_table.Group_total_write(5);
	data_table.Association_total_write(6);
	for (int i = 0; i < data_table.Group_total_read(); i++)
	{
		data_table.address_table[i].write(i+1);
	}
	for (int i = 0; i < data_table.Object_total_read(); i++)
	{
		value_temp++ ;
		data_table.object_table[i].dpt_write(LXCAN_DPT_5_001);
		data_table.object_table[i].value_write(&value_temp,1);
		data_table.object_table[i].callback[0].attach(fun0);
		data_table.object_table[i].callback[1].attach(fun1);
		data_table.object_table[i].callback[2].attach(fun2);
		data_table.object_table[i].callback[3].attach(fun3);
		data_table.object_table[i].callback[4].attach(fun4);
	}
	data_table.association_table[0].object_index_write(0);
	data_table.association_table[0].group_index_write(0);

	data_table.association_table[1].object_index_write(1);
	data_table.association_table[1].group_index_write(2);

	data_table.association_table[2].object_index_write(2);
	data_table.association_table[2].group_index_write(1);

	data_table.association_table[3].object_index_write(3);
	data_table.association_table[3].group_index_write(3);

	data_table.association_table[4].object_index_write(4);
	data_table.association_table[4].group_index_write(3);

	data_table.association_table[5].object_index_write(4);
	data_table.association_table[5].group_index_write(4);

	LXCan_log_d("Test","object_length:%d",data_table.Object_total_read());
	LXCan_log_d("Test","group_length:%d",data_table.Group_total_read());
	LXCan_log_d("Test","associtation_length:%d",data_table.Association_total_read());
	//delay_ms(500);
	for (int i = 0; i < data_table.Object_total_read(); i++)
	{
		data_table.object_table[i].value_read(value_temp);
		data_table.object_table[i].value_read(value,value_len);
		LXCan_log_d("Test","object [%d] len:%d value:%d",i,data_table.object_table[i].length_read(),value_temp);
		LXCan_log_d("Test","object-[%d] len:%d value:%d",i,value_len,value[0]);
	}
	for (int i = 0; i < data_table.Group_total_read(); i++)
	{
		LXCan_log_d("Test","Group[%d] :%d",i,data_table.address_table[i].read());
	}
	for (int i = 0; i < data_table.Association_total_read(); i++)
	{
		LXCan_log_d("Test","Assocication[%d], object_index:%d,group_index:%d",i,data_table.association_table[i].object_index_read(),data_table.association_table[i].group_index_read());
	}
	uint8_t fun_data[2] = {0,12};
	for(int j = 1; j < data_table.Group_total_read(); j++)
	{
		uint8_t group;
		group = data_table.address_table[j].read();
		LXCan_log_d("Test","Search Group[%d]:%d",j,group);
		for (int i = 0; i < data_table.search_object(group,object_buff,association_buff); i++)
		{
			fun_data[0] = data_table.association_table[association_buff[i]].object_index_read();
			object_buff[i]->callback[0].call((uint8_t*)fun_data);
			object_buff[i]->callback[1].call((uint8_t*)fun_data);
			object_buff[i]->callback[2].call((uint8_t*)fun_data);
			object_buff[i]->callback[3].call((uint8_t*)fun_data);
			object_buff[i]->callback[4].call((uint8_t*)fun_data);
		}
	}
	for (int i = 0; i < data_table.Object_total_read(); i++)
	{
		LXCanObject *object;
		object = &data_table.object_table[i];
		LXCan_log_d("Test","Search object[%d]",i);
		for (int j = 0; j < data_table.search_group(i,group_buff,association_buff); j++)
		{
			LXCan_log_d("Test","group[%d]:%d",data_table.association_table[association_buff[j]].group_index_read(),data_table.address_table[data_table.association_table[association_buff[j]].group_index_read()].read());
		}
		LXCan_log_d("Test","Search2 object[%d]",i);
		for (int j = 0; j < data_table.search_group(object,group_buff); j++)
		{
			LXCan_log_d("Test","group:%d",group_buff[j]->read());
		}
	}
}
