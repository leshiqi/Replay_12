  #include "LXCanTelegram.h"
LXCanTelegram::LXCanTelegram()
{
	//clear();
}

void LXCanTelegram::begin(TelegramType *data)
{
	this->Telegram_data = data;
	//clear();
}
void LXCanTelegram::clear()
{
	Telegram_data->CAN_ID = 0;
	Telegram_data->length = 0;
	Telegram_data->_priority = LXCAN_PRIORITY_NORMAL_VALUE;
	for(uint8_t i=0;i<8;i++)
		Telegram_data->_buff_data[i] = 0;
}
uint32_t LXCanTelegram::ID_read()
{
	return Telegram_data->CAN_ID; 
}
void LXCanTelegram::ID_write(uint32_t ID)
{
	Telegram_data->CAN_ID = ID & 0x1fffffff;
}
e_FunctionalState LXCanTelegram::repetition_read()
{
	return (e_FunctionalState)Telegram_data->_repetition_flag;
}
void LXCanTelegram::repetition_write(e_FunctionalState newstatte)
{
	Telegram_data->_repetition_flag = (uint32_t)newstatte;
}
e_FunctionalState LXCanTelegram::ACK_read()
{
	return (e_FunctionalState)Telegram_data->_ack_flag;
}
void LXCanTelegram::ACK_write(e_FunctionalState newstate)
{
	Telegram_data->_ack_flag = (uint32_t)newstate;
}
LXCanGroupAddrType LXCanTelegram::dest_read()
{
	return Telegram_data->_dest_add;
}
void LXCanTelegram::dest_write(LXCanGroupAddrType addr)
{
	Telegram_data->_dest_add = addr & 0x03ff;
}
e_TypeAdd LXCanTelegram::TypeAdd_read()
{
	return (e_TypeAdd)Telegram_data->_type_add;
}
void LXCanTelegram::TypeAdd_write(e_TypeAdd newtype)
{
	Telegram_data->_type_add = (uint32_t)newtype;
}
LXCanPhysicalAddrType LXCanTelegram::source_read()
{
	return (LXCanPhysicalAddrType) Telegram_data->_source_add;
}
void LXCanTelegram::source_write(LXCanPhysicalAddrType addr)
{
	Telegram_data->_source_add = addr;
}
e_LXCanPriority LXCanTelegram::priority_read()
{
	return (e_LXCanPriority)Telegram_data->_priority;
}
void LXCanTelegram::priority_write(e_LXCanPriority newpriority)
{
	Telegram_data->_priority = (uint32_t)newpriority;
}
uint8_t LXCanTelegram::buff_read(uint8_t index)
{
	if(index > 7)
		index = 7;
	return Telegram_data->_buff_data[index];
}
void LXCanTelegram::buff_read(uint8_t &len,uint8_t*data)
{
	len =Telegram_data->length;
	for(uint8_t i=0 ; i<len ; i++)
		data[i] =Telegram_data->_buff_data[i];
}
void LXCanTelegram::buff_write(uint8_t len,uint8_t *data)
{
	if(len > 8)
		len = 8;
	Telegram_data->length = len;
	for(uint8_t i=0 ; i < Telegram_data->length ; i++)
		Telegram_data->_buff_data[i] = data[i];
}
void LXCanTelegram::buff_write(uint8_t index, uint8_t data)
{
	if(index > 7)
		index = 7;
	Telegram_data->_buff_data[index] = data;
}
e_LXCanService LXCanTelegram::service_read()
{
	return (e_LXCanService)Telegram_data->_service;
}
void LXCanTelegram::servicr_write(e_LXCanService newservice)
{
	Telegram_data->_service = newservice;
}
e_LXCanFrameType LXCanTelegram::frame_read()
{
	return (e_LXCanFrameType)Telegram_data->_frame_flag;
}

void LXCanTelegram::frame_write(e_LXCanFrameType newframe)
{
	Telegram_data->_frame_flag = (uint32_t)newframe;
}

void LXCanTelegram::value_read(uint8_t &len,uint8_t*val)
{
	len = Telegram_data->length-1;
	for(uint8_t i=0 ; i<len ; i++)
		val[i] = Telegram_data->_buff_data[i+1];
}

void LXCanTelegram::value_write(uint8_t len,uint8_t*val)
{
	if(len > 7)
		len = 7;
	Telegram_data->length = len+1;
	for (uint8_t i=0 ; i<len ; i++)
		Telegram_data->_buff_data[i+1] = val[i];
}

uint8_t LXCanTelegram::datalength_read()
{
	return Telegram_data->length;
}

void LXCanTelegram::datalength_write(uint8_t len)
{
	if(len > 8)
		Telegram_data->length = 8;
	else
		Telegram_data->length = len;
}
/**********************************************************************************************************************/
//��������
#ifdef DEBUG
LXCanTelegram TelegramTest;
void dataTobin(uint32_t data)
{
	char* str_temp = "|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|\n";
	char str_bin[] = "|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |\n";
	for(uint8_t i=0 ; i<32 ; i++)
	{
		if(0x01 & (data >> i))
			str_bin[95-3*i] = '1';
		else
			str_bin[95-3*i] = '0';
	}
	uart1.printf("Data Hex:%x\n",data);
	uart1.put_string(str_temp);
	uart1.put_string(str_bin);
	uart1.printf("\n");
	//LXCan_log_d("Tset","DATA HEX:%X,Bin:%s",data,str_bin);
}
void TestWrite()
{
	uint8_t temp_u8;
	uint16_t temp_u16;
	TelegramType data;
	TelegramTest.begin(&data);
	TelegramTest.clear();
	temp_u8 = 1;
	LXCan_log_d("TestID","priority");
	TelegramTest.priority_write((e_LXCanPriority)temp_u8);
	LXCan_log_d("ID_Source","write:%x,read:%x",temp_u8,TelegramTest.priority_read());
	dataTobin(TelegramTest.ID_read());

	temp_u8 = 0xff;
	LXCan_log_d("TestID","priority");
	TelegramTest.priority_write((e_LXCanPriority)temp_u8);
	LXCan_log_d("ID_Source","write:%x,read:%x",temp_u8,TelegramTest.priority_read());
	dataTobin(TelegramTest.ID_read());

	TelegramTest.clear();
	temp_u8 = 1;
	LXCan_log_d("TestID","source");
	TelegramTest.source_write(temp_u8);
	LXCan_log_d("ID_Source","write:%x,read:%x",temp_u8,TelegramTest.source_read());
	dataTobin(TelegramTest.ID_read());

	temp_u8 = 0xff;
	LXCan_log_d("TestID","source");
	TelegramTest.source_write(temp_u8);
	LXCan_log_d("ID_Source","write:%x,read:%x",temp_u8,TelegramTest.source_read());
	dataTobin(TelegramTest.ID_read());

	TelegramTest.clear();
	temp_u8 = 1;
	LXCan_log_d("TestID","type_add");
	TelegramTest.TypeAdd_write((e_TypeAdd)temp_u8);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u8,TelegramTest.TypeAdd_read());
	dataTobin(TelegramTest.ID_read());

	TelegramTest.clear();
	temp_u16 = 1;
	LXCan_log_d("TestID","dest");
	TelegramTest.dest_write(temp_u16);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u16,TelegramTest.dest_read());
	dataTobin(TelegramTest.ID_read());
	temp_u16 = 0xffff;
	LXCan_log_d("TestID","dest");
	TelegramTest.dest_write(temp_u16);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u16,TelegramTest.dest_read());
	dataTobin(TelegramTest.ID_read());

	TelegramTest.clear();
	temp_u8 = 1;
	LXCan_log_d("TestID","ack_flag");
	TelegramTest.ACK_write((e_FunctionalState)temp_u8);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u8,TelegramTest.ACK_read());
	dataTobin(TelegramTest.ID_read());

	TelegramTest.clear();
	temp_u8 = 1;
	LXCan_log_d("TestID","repetition");
	TelegramTest.repetition_write((e_FunctionalState)temp_u8);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u8,TelegramTest.repetition_read());
	dataTobin(TelegramTest.ID_read());

	TelegramTest.clear();
	temp_u8 = 1;
	LXCan_log_d("TestData","frame");
	TelegramTest.frame_write((e_LXCanFrameType)temp_u8);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u8,TelegramTest.frame_read());
	dataTobin(TelegramTest.buff_read(0));
	temp_u8 = 3;
	LXCan_log_d("TestData","frame");
	TelegramTest.frame_write((e_LXCanFrameType)temp_u8);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u8,TelegramTest.frame_read());
	dataTobin(TelegramTest.buff_read(0));

	TelegramTest.clear();
	temp_u8 = 1;
	LXCan_log_d("TestData","service");
	TelegramTest.servicr_write((e_LXCanService)temp_u8);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u8,TelegramTest.service_read());
	dataTobin(TelegramTest.buff_read(0));
	temp_u8 = 15;
	LXCan_log_d("TestData","service");
	TelegramTest.servicr_write((e_LXCanService)temp_u8);
	LXCan_log_d("ID_type_add","write:%x,read:%x ",temp_u8,TelegramTest.service_read());
	dataTobin(TelegramTest.buff_read(0));

	
	uint8_t buff[9] = {1,2,3,4,5,6,7,8,9};
	uint8_t buff2[9] = {0};
	uint8_t len;
	TelegramTest.clear();
	TelegramTest.buff_write(9,(uint8_t*)buff);
	TelegramTest.buff_read(len,buff2);
	LXCan_log_d("TestData ","len:%d data[0-7]:%d,%d,%d,%d,%d,%d,%d,%d ",len,buff2[0],buff2[1],buff2[2],buff2[3],buff2[4],buff2[5],buff2[6],buff2[7]);
	temp_u8 = 0x11;
	TelegramTest.buff_write(1,temp_u8);
	LXCan_log_d("TestData_index ","write:%x,read:%x",temp_u8,TelegramTest.buff_read(1));
	temp_u8 = 0x77;
	TelegramTest.buff_write(7,temp_u8);
	LXCan_log_d("TestData_index ","write:%x,read:%x",temp_u8,TelegramTest.buff_read(7));
	temp_u8 = 0x77;
	TelegramTest.buff_write(17,temp_u8);
	LXCan_log_d("TestData_index ","write:%x,read:%x",temp_u8,TelegramTest.buff_read(7));

	TelegramTest.clear();
	TelegramTest.value_write(9,(uint8_t*)buff);
	TelegramTest.value_read(len,buff2);
	LXCan_log_d("TestData ","value len:%d data[0-7]:%d,%d,%d,%d,%d,%d,%d,%d ",len,buff2[0],buff2[1],buff2[2],buff2[3],buff2[4],buff2[5],buff2[6],buff2[7]);
	TelegramTest.buff_read(len,buff2);
	LXCan_log_d("TestData ","buff len:%d data[0-7]:%d,%d,%d,%d,%d,%d,%d,%d  ",len,buff2[0],buff2[1],buff2[2],buff2[3],buff2[4],buff2[5],buff2[6],buff2[7]);

	temp_u8 = 1;
	TelegramTest.clear();
	TelegramTest.datalength_write(temp_u8);
	LXCan_log_d("TestLength","write:%d , read: %d",temp_u8,TelegramTest.datalength_read());
	temp_u8 = 8;
	TelegramTest.clear();
	TelegramTest.datalength_write(temp_u8);
	LXCan_log_d("TestLength","write:%d , read: %d",temp_u8,TelegramTest.datalength_read());
	temp_u8 = 17;
	TelegramTest.clear();
	TelegramTest.datalength_write(temp_u8);
	LXCan_log_d("TestLength","write:%d , read: %d",temp_u8,TelegramTest.datalength_read());
}
#endif

