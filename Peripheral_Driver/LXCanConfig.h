#ifndef __LXCANCONFIG_H
#define __LXCANCONFIG_H
#include "LX_OS.h"
#include "led.h"
//#include "weak.h"

typedef uint8_t  LXCanPhysicalAddrType;
typedef uint16_t LXCanGroupAddrType;

#define DEVICE_CATEGORY     402

#define CAN_ACK_CONFIRM_NUM  3 //发送失败重发次数
#define CAN_ACK_CONFIRM_TIME  10 //等待回复时间

#define ASSOCICATION_SIZE   20 //关联数量
#define OBJECT_SIZE       20  //对象数量
#define GROUPADD_SIZE     20 //组对象数量
//#define GROUP_REGISTER_SIZE  10   //对象寄存器数量  必须为2的倍数
#define OBJECT_REGISTER_SIZE  10  //对象固有属性寄存器大小
//#define TARGETDOBJINDEX_MAX   10

#define OBJECT_FUNCATION_SIZE  8 //对象的功能回调函数数组大小

#define SEARCH_OBJECT_SIZE 10  //索引对象缓存大小
#define SEARCH_GROUP_SIZE  10  //索引组地址缓存大小
#define SEARCH_ASSOCTION_SIZE 10 //索引关联缓存大小

#define ACTIONS_RECEIVE_QUEUE_SIZE 16 //接受消息缓存队列长度
#define ACTIONS_SEND_QUEUE_SIZE 16    //发送消息缓存队列长度
#define ASSOCICATION_REGISTER_SIZE 10 //关联组固有属性数量
#define ASSOCICATION_OBJECT_REGISTER_SIZE 10 //关联组对象属性数量
#define OBJECT_MEMORY_DATA_SIZE    100//对象缓存池大小

#define LXCAN_FLASH_BASE         (STM32_FLASH_BASE+(50*1024))

#define DEVICE_INFO_BASE         (LXCAN_FLASH_BASE + (0*1024) )
#define OBJECT_TABLE_BASE		 (LXCAN_FLASH_BASE + (2*1024) )
#define OBJECT_DATA_BASE		 (LXCAN_FLASH_BASE + (1*1024) )
#define GROUPADD_TABLE_BASE      (LXCAN_FLASH_BASE + (4*1024) )
#define ASSOCIATION_TABLE_BASE   (LXCAN_FLASH_BASE + (6*1024) )
   
#include "LXCanTelegram.h"
#include "ActionRingBuffer.h"
//#include "LXCanObject.h"
#define LXCan_log_a(tag, ...)    elog_a(tag, __VA_ARGS__)
#define LXCan_log_e(tag, ...)    elog_e(tag, __VA_ARGS__)
#define LXCan_log_w(tag, ...)    elog_w(tag, __VA_ARGS__)
#define LXCan_log_i(tag, ...)    elog_i(tag, __VA_ARGS__)
#define LXCan_log_d(tag, ...)    elog_d(tag, __VA_ARGS__)
#define LXCan_log_v(tag, ...)    elog_v(tag, __VA_ARGS__)

#define LXCan_Device_Reset()     NVIC_SystemReset()
#define LXCan_Close_Interrupt()  asm("CPSID I") //关中断
#define LXCan_Open_Interfupt()   asm("CPSIE I") //开中断

//typedef unsigned char uint8_t;
//typedef unsigned short uint16_t;
//typedef unsigned long uint32_t;
//#define uint8_t unsigned char
//#define uint16_t unsigned short
//#define uint32_t unsigned long
#endif
