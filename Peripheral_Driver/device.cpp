#include "device.h"

void exit_scene(uint16_t group);
CAN can1(CAN1,&PB8,&PB9);
LXCanDevice device(&can1);

void fundata_read(uint8_t *data, FunDataType &fundata)
{
	uint16_t time_unit;
	fundata.object_index = *((uint16_t*)(&data[0]));
	fundata.associtation_index = *((uint16_t*)(&data[2]));
	fundata.value = data[4];
	fundata.port = device.data_table.object_table[fundata.object_index].register_read(0);//对象寄存器0表示端口号
	fundata.relay_fun = (e_Relay_fun)device.data_table.association_table[fundata.associtation_index].object_reg_read(0);//对象组属性寄存器0表示功能号
	switch (device.data_table.association_table[fundata.associtation_index].object_reg_read(1))
	{
	case 0://分钟
		time_unit = 60 * 1000;
		break;
	case 1://秒
		time_unit = 1000;
		break;
	case 2://毫秒
		time_unit = 1;
		break;
	default:
		break;
	}
	fundata.on_time = time_unit * device.data_table.association_table[fundata.associtation_index].object_reg_read(2);

	fundata.off_time = time_unit * device.data_table.association_table[fundata.associtation_index].object_reg_read(3);
	
}
void sysflag_read(uint8_t *data, SysFlagType &sysflag)
{
	uint8_t associtation_index = 0;
	associtation_index = *((uint16_t*)(&data[2]));
	sysflag.group_type = (e_GroupType_AssocicationReg0)device.data_table.association_table[associtation_index].assocication_reg_read(0);
	sysflag.card_relation = (e_Card__AssocicationReg1)device.data_table.association_table[associtation_index].assocication_reg_read(1);
	sysflag.time_mode  = (e_TimeMode__AssocicationReg2)device.data_table.association_table[associtation_index].assocication_reg_read(2);
	sysflag.memory_mode = (e_Memory__AssocicationReg3)device.data_table.association_table[associtation_index].assocication_reg_read(3);
	sysflag.night_light_mode = (e_NightLight__AssocicationReg4)device.data_table.association_table[associtation_index].assocication_reg_read(4);
}
uint8_t sysflag_judge(FunDataType fundata , SysFlagType sysflag)
{
	//检测插卡
	if(sysflag.card_relation == Vaild_Card)
	{
		if(device.data_table.systemstate.card_flag == NO_Card)
			return 1;
	}
	else if(sysflag.card_relation == InVaild_Card)
	{
		if(device.data_table.systemstate.card_flag == Add_Card)
			return 1;
	}
	//白夜模式
	if (sysflag.time_mode == DayValid)
	{
		if(device.data_table.systemstate.time_flag == Time_Night)
			return 1;
	}
	else if(sysflag.time_mode == NightValid)
	{
		if(device.data_table.systemstate.time_flag == Time_Day)
			return 1;
	}
	return 0;
}
uint8_t matercontrol(FunDataType fundata , SysFlagType sysflag)
{
	if(fundata.value == 1)//进入总控1
	{
		if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
			device.data_table.association_table[fundata.associtation_index].object_reg_write(5,relay_read(fundata.port));
		//if(sysflag.night_light_mode == DisNightLight)
		relay_off(fundata.port);
	}
	else if(fundata.value == 2)//进入总控2
	{
		//if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
		//	device.data_table.association_table[fundata.associtation_index].object_reg_write(5,relay_read(fundata.port));
		//if(sysflag.night_light_mode == EnNightLight)
		//	relay_off(fundata.port);
	}
	else if(fundata.value == 3)//只退出总控，不执行其他操作
	{
		device.data_table.association_table[fundata.associtation_index].object_reg_write(6,0);//总控状态保存
	}
	else if(fundata.value == 0)//退出总控
	{
		if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
		{
			if(device.data_table.association_table[fundata.associtation_index].object_reg_read(5))
				relay_on(fundata.port);
			else
				relay_off(fundata.port);
		}
		else
		{
			if(device.data_table.association_table[fundata.associtation_index].object_reg_read(4))
				relay_on(fundata.port);
			else
				relay_off(fundata.port);
		}

	}
	device.data_table.association_table[fundata.associtation_index].object_reg_write(6,fundata.value);//总控状态保存
	return 1;
}
uint8_t BathroomFan(uint8_t *data)
{
	//卫生间风扇自身不处理，和卫生间风扇关联。
	uint8_t fan_port = 0xff;
	uint16_t object_index;
	LXCanAddress **group_buff;
	LXCanObject **object_buff;
	uint16_t *assoction_index;

	FunDataType fundata;
	uint8_t temp_data[5];

	uint16_t fan_association_buff[10];//排风扇关联表缓存池
	uint8_t fan_group_num;
	uint8_t light_group_num;
	for (int i = 0; i < device.data_table.Object_total_read(); i++)
	{
		if(device.data_table.object_table[i].register_read(1) == (uint8_t)fun_BathroomFan)
		{
			//fan_port = device.data_table.object_table[i].register_read(0);
			fan_group_num = device.data_table.search_group(i , group_buff , assoction_index);
			for (int k = 0; k < fan_group_num; k++)
			{
				fan_association_buff[k] = assoction_index[k];
			}
			//elog_d("fan","fan_num:%d ass:%d",fan_group_num , fan_association_buff[0]);
			object_index = *((uint16_t*)(&data[0]));
			light_group_num = device.data_table.search_group(object_index , group_buff , assoction_index);
			//elog_d("fan","object_index:%d num:%d",object_index , light_group_num);
			for (int k = 0; k < fan_group_num; k++)
			{
				for (int j = 0; j < light_group_num; j++)
				{
					uint8_t fan_group_index = device.data_table.association_table[fan_association_buff[k]].group_index_read();
					if(device.data_table.address_table[fan_group_index].read() == group_buff[j]->read())
					{
						//fan_association_index = fan_association_buff[k];
						*((uint16_t*)(&temp_data[0])) = i;//排风扇对象索引
						*((uint16_t*)(&temp_data[2])) = fan_association_buff[k];//排风扇关联表
						temp_data[4] = data[4];
						fan_port = device.data_table.object_table[i].register_read(0);
						goto SEARCH_OK;
					}
					//elog_d("fan","fan_group[%d]:%d light_group[%d]:%d", k ,device.data_table.association_table[fan_association_buff[k]].group_index_read(),j,group_buff[j]->read());
				}
			}
			//elog_d("FAN" , "objec_index:%d  val:%d" , i , data[4]);
		}
	}
	return 0;
	//检索成功
SEARCH_OK:
	//elog_d("FAN" , "objec_index:%d  val:%d" , fundata.object_index , fundata.value);
	fundata_read( temp_data , fundata);
	if(!fundata.value)
	{
		LXCanGroupAddrType fan_group = device.data_table.address_table[ device.data_table.association_table[fundata.associtation_index].group_index_read() ].read();
		uint8_t object_num = device.data_table.search_object(fan_group , object_buff);
		//elog_d("fan" , "obiect_num:%d",object_num);
		for (int i = 0; i < object_num; i++)
		{
			if(object_buff[i]->register_read(1) == (uint8_t)fun_BathroomLight)
			{
				if(relay_read(object_buff[i]->register_read(0)))
				{
					//有其他卫生间灯亮
					//if(object_buff[i]->register_read(0) != device.data_table.object_table[object_index].register_read(0))//除了正在操作的继电器
					return 0;
				}
			}
		}
		relay_delay_off(fan_port , fundata.off_time);
	}
	else
	{
		relay_on(fan_port);
	}
	return 1;
}
uint8_t fun0_Light(uint8_t *data)
{
	FunDataType fundata;
	SysFlagType sysflag;
	//DeviceDataType ack_data;
	//uint8_t ack_value[8];
	fundata_read( data , fundata);
	sysflag_read( data , sysflag);
	elog_d("fun0","relay_fun:%d port:%d",fundata.relay_fun,fundata.port);
	if(sysflag_judge(fundata , sysflag))
		return 0;
	if(sysflag.group_type == MasterControl_GroupType)
	{
		elog_d("fun0","MasterControl value:%d",fundata.value);
		matercontrol(fundata , sysflag);
		goto ACK_DATA_SEND;
	}
	else if(sysflag.group_type == Scene_GroupType)//场景
	{
		if(fundata.value)
		{
			if((device.data_table.object_table[fundata.object_index].scene_group != 0xffff)&&(device.data_table.object_table[fundata.object_index].scene_group != device.data_table.address_table[device.data_table.association_table[fundata.associtation_index].group_index_read()].read()))//进入不同场景
				exit_scene(device.data_table.object_table[fundata.object_index].scene_group);
			fundata.value = device.data_table.association_table[fundata.associtation_index].object_reg_read(4);
			device.data_table.object_table[fundata.object_index].scene_group = device.data_table.address_table[device.data_table.association_table[fundata.associtation_index].group_index_read()].read();
		}
		else//退出场景，不操作
		{
			device.data_table.object_table[fundata.object_index].scene_group = 0xffff;
			return 0;
		}
	}
	else
	{
		//本组非场景组，检测本组端口有无在其他场景组中有设置
		//		TelegramType telegram_temp;
		if(device.data_table.object_table[fundata.object_index].scene_group != 0xffff)
		{
			exit_scene(device.data_table.object_table[fundata.object_index].scene_group);
			device.data_table.object_table[fundata.object_index].scene_group = 0xffff;
		}
	}
	
	switch (fundata.relay_fun)
	{
	case R_Switch:
		if(fundata.value)
			relay_on(fundata.port);
		else
			relay_off(fundata.port);
		break;
	case R_DelayOn:
		//if(fundata.value)
			relay_delay_on(fundata.port,fundata.on_time);
		/*else
			relay_off(fundata.port);*/
		break;
	case R_DelayOff:
		//if(fundata.value)
		/*	relay_on(fundata.port);
		else*/
			relay_delay_off(fundata.port,fundata.on_time);
		break;
	case R_Togggle:
		relay_toggle(fundata.port);
		break;
	case R_Blink:

		break;
	case R_TurnOffDelay:
		relay_turnoff_delay(fundata.port,fundata.on_time);
		break;
	default:
		break;
	}
ACK_DATA_SEND:
	
	return 1;
}
//卫生间风扇
uint8_t fun1_BathroomFan(uint8_t *data)
{
	FunDataType fundata;
	SysFlagType sysflag;
	fundata_read( data , fundata);
	sysflag_read( data , sysflag);
	elog_d("fun1" , "object_index:%d" , fundata.object_index);
	if(sysflag.group_type == MasterControl_GroupType)
	{
		if(fundata.value == 1)//进入总控1
		{
			if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
				device.data_table.association_table[fundata.associtation_index].object_reg_write(5,relay_read(fundata.port));
			if(sysflag.night_light_mode == DisNightLight)
				fundata.value = 0;
		}
		else if(fundata.value == 2)//进入总控2
		{
			if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
				device.data_table.association_table[fundata.associtation_index].object_reg_write(5,relay_read(fundata.port));
			if(sysflag.night_light_mode == EnNightLight)
				fundata.value = 0;
		}
		else if(fundata.value == 0)//退出总控
		{
			if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
			{
				if(device.data_table.association_table[fundata.associtation_index].object_reg_read(5))
					fundata.value = 1;
				else
					fundata.value = 0;
			}
		}
	}
	else if(sysflag.group_type == Scene_GroupType)
	{
		fundata.value = device.data_table.association_table[fundata.associtation_index].object_reg_read(5);
	}

	//检测插卡
	if(sysflag.card_relation == Vaild_Card)
	{
		if(device.data_table.systemstate.card_flag == NO_Card)
			return 0;
	}
	else if(sysflag.card_relation == InVaild_Card)
	{
		if(device.data_table.systemstate.card_flag == Add_Card)
			return 0;
	}
	switch (fundata.relay_fun)
	{
	case R_Switch:
		if(fundata.value)
			relay_on(fundata.port);
		else
			relay_off(fundata.port);
		break;
	case R_DelayOn:
		if(fundata.value)
			relay_delay_on(fundata.port,fundata.on_time);
		break;
	case R_DelayOff:
		if(fundata.value)
			relay_delay_off(fundata.port,fundata.on_time);
		break;
	case R_Togggle:
		if(fundata.value)
			relay_toggle(fundata.port);
		break;
	case R_Blink:

		break;
	case R_TurnOffDelay:
		if(fundata.value)
			relay_turnoff_delay(fundata.port,fundata.on_time);
		break;
	default:
		break;
	}
	return 1;
}
//卫生间灯
uint8_t fun2_BathroomLight(uint8_t *data)
{
	FunDataType fundata;
	SysFlagType sysflag;
	//DeviceDataType ack_data;  
	//uint8_t ack_value[8];
	//uint8_t fan_port;
	struct FanData_Type
	{
		uint16_t object_index;
		uint16_t association_index;
		uint8_t val;
	}fan_data;
	fundata_read( data , fundata);
	sysflag_read( data , sysflag);
	fan_data.object_index = fundata.object_index;
	fundata.associtation_index = fundata.associtation_index;
	elog_d("fun2" , "object_index:%d" , fundata.object_index);
	if(sysflag_judge(fundata , sysflag))
		return 0;
	if(sysflag.group_type == MasterControl_GroupType)
	{
		elog_d("fun2","MasterControl value:%d",fundata.value);
		matercontrol(fundata , sysflag);
		goto ACK_DATA_SEND;
	}
	else if(sysflag.group_type == Scene_GroupType)//场景
	{
		if(fundata.value)
		{
			if((device.data_table.object_table[fundata.object_index].scene_group != 0xffff)&&(device.data_table.object_table[fundata.object_index].scene_group != device.data_table.address_table[device.data_table.association_table[fundata.associtation_index].group_index_read()].read()))//进入不同场景
				exit_scene(device.data_table.object_table[fundata.object_index].scene_group);
			fundata.value = device.data_table.association_table[fundata.associtation_index].object_reg_read(4);
			device.data_table.object_table[fundata.object_index].scene_group = device.data_table.address_table[device.data_table.association_table[fundata.associtation_index].group_index_read()].read();
		}
		else//退出场景，不操作
		{
			device.data_table.object_table[fundata.object_index].scene_group = 0xffff;
			return 0;
		}
	}
	else
	{
		//本组非场景组，检测本组端口有无在其他场景组中有设置
		//		TelegramType telegram_temp;
		if(device.data_table.object_table[fundata.object_index].scene_group != 0xffff)
		{
			exit_scene(device.data_table.object_table[fundata.object_index].scene_group);
			device.data_table.object_table[fundata.object_index].scene_group = 0xffff;
		}
	}

	/*for (int i = 0; i < device.data_table.Object_total_read(); i++)
	{
		if(device.data_table.object_table[i].register_read(1) == (uint8_t)fun_BathroomFan)
		{
			fan_port = device.data_table.object_table[i].register_read(0);
			break;
		}
	}*/
	switch (fundata.relay_fun)
	{
	case R_Switch:
		if(fundata.value)
		{
			relay_on(fundata.port);
			//relay_on(fan_port);
			fan_data.val = 1;
			BathroomFan((uint8_t*)(&fan_data));
		}
		else
		{
			//	//判断其他卫生间灯状态
			//	for (int i = 0; i < device.data_table.Object_total_read(); i++)
			//	{
			//		if(device.data_table.object_table[i].register_read(1) == (uint8_t)fun_BathroomLight)
			//		{
			//			if(relay_read(device.data_table.object_table[i].register_read(0)))
			//			{
			//				//有其他卫生间灯亮
			//				fan_value = 1;
			//				break;
			//			}
			//		}
			//	}
			//	/*if(fan_value == 0)
			//	relay_off(fan_port);*/
			//fun1_BathroomFan(&fan_value);

			fan_data.val = 0;
			BathroomFan((uint8_t*)(&fan_data));
			relay_off(fundata.port);
		}
		break;
	case R_DelayOn:
		if(fundata.value)
		{
			relay_delay_on(fundata.port,fundata.on_time);
			fan_data.val = 1;
			BathroomFan((uint8_t*)(&fan_data));
		}
		break;
	case R_DelayOff:
		if(fundata.value)
		{
			fan_data.val = 0;
			BathroomFan((uint8_t*)(&fan_data));
			relay_delay_off(fundata.port,fundata.on_time);
		}
		
		break;
	case R_Togggle:
		/*relay_toggle(fundata.port);*/
		if(fundata.value)
		{
			if(!relay_read(fundata.port))
			{
				relay_on(fundata.port);
				fan_data.val = 1;
				BathroomFan((uint8_t*)(&fan_data));
			}
			else
			{
				relay_off(fundata.port);
				fan_data.val = 0;
				BathroomFan((uint8_t*)(&fan_data));
			}
		}
		
		break;
	case R_Blink:

		break;
	case R_TurnOffDelay:

		break;
	default:
		break;
	}
	ACK_DATA_SEND:
	return 1;
}
//小夜灯
uint8_t fun3_NightLight(uint8_t *data)
{
	FunDataType fundata;
	SysFlagType sysflag;
	/*DeviceDataType ack_data;
	uint8_t ack_value[8];*/
	fundata_read( data , fundata);
	sysflag_read( data , sysflag);
	if(sysflag_judge(fundata , sysflag))
		return 0;
	if(sysflag.group_type == MasterControl_GroupType)
	{
		elog_d("fun3","MasterControl value:%d",fundata.value);
		if(fundata.value == 1)//进入总控1
		{
			/*if(sysflag.night_light_mode ==EnNightLight)
				relay_on(fundata.port);*/
		}
		else if(fundata.value == 2)//进入总控2
		{
			if(sysflag.night_light_mode ==EnNightLight)
				relay_on(fundata.port);
		}
		else if(fundata.value == 0)//退出总控
		{
			relay_off(fundata.port);
		}
		return 1;
	}
	else if(sysflag.group_type == Light_GroupType)
	{
		if(fundata.value)
		{
			relay_toggle(fundata.port);
		}
	}
	return 0;
}
//窗帘
uint8_t fun4_Curtrain_open(uint8_t *data)
{
	FunDataType fundata;
	SysFlagType sysflag;
	/*DeviceDataType ack_data;
	uint8_t ack_value[8];*/
	uint8_t lock_port;//窗帘功能默认开互锁使能
	fundata_read( data , fundata);
	sysflag_read( data , sysflag);
	if(sysflag_judge(fundata , sysflag))
		return 0;

	if(sysflag.group_type == Scene_GroupType)
	{
		fundata.value = device.data_table.association_table[fundata.associtation_index].object_reg_read(5);
	}

	lock_port = device.data_table.object_table[fundata.object_index].register_read(3);
	//relay_fun = (e_Relay_fun)device.data_table.association_table[associtation_index].object_reg_read(0);//对象组属性寄存器0表示功能号
	if((sysflag.group_type == CurtainOpen_GroupType) || (sysflag.group_type == GauzeOpen_GroupType))
	{
		if((fundata.value == 0x01)||(fundata.value == 0x82))//短按
		{
			relay_off(lock_port);
			if(relay_read(fundata.port))
				relay_off(fundata.port);
			else
				relay_turnoff_delay(fundata.port,fundata.on_time);
		}
		else if(fundata.value == 0x81)
		{
			relay_off(lock_port);
			relay_turnoff_delay(fundata.port,fundata.on_time);
		}
		else if(fundata.value == 0x80)
		{
			relay_off(lock_port);
			relay_off(fundata.port);
		}
	}
	return 1;
}
uint8_t fun5_Curtrain_close(uint8_t *data)
{
	FunDataType fundata;
	SysFlagType sysflag;
	/*DeviceDataType ack_data;
	uint8_t ack_value[8];*/
	uint8_t lock_port;//窗帘功能默认开互锁使能
	fundata_read( data , fundata);
	sysflag_read( data , sysflag);
	if(sysflag_judge(fundata , sysflag))
		return 0;
	if(sysflag.group_type == Scene_GroupType)
	{
		fundata.value = device.data_table.association_table[fundata.associtation_index].object_reg_read(5);
	}
	lock_port = device.data_table.object_table[fundata.object_index].register_read(3);
	//relay_fun = (e_Relay_fun)device.data_table.association_table[associtation_index].object_reg_read(0);//对象组属性寄存器0表示功能号
	if((sysflag.group_type == CurtainClose_GroupType) || (sysflag.group_type == GauzeClose_GroupType))
	{
		if((fundata.value == 0x01)||(fundata.value == 0x82))//短按
		{
			relay_off(lock_port);
			if(relay_read(fundata.port))
				relay_off(fundata.port);
			else
				relay_turnoff_delay(fundata.port,fundata.on_time);
		}
		else if(fundata.value == 0x81)
		{
			relay_off(lock_port);
			relay_turnoff_delay(fundata.port,fundata.on_time);
		}
		else if(fundata.value == 0x80)
		{
			relay_off(lock_port);
			relay_off(fundata.port);
		}
	}
	return 1;
}
uint8_t fun6_ControlLight(uint8_t *data)
{
	FunDataType fundata;
	SysFlagType sysflag;
	//DeviceDataType ack_data;
	//uint8_t ack_value[8];
	fundata_read( data , fundata);
	sysflag_read( data , sysflag);
	elog_d("fun0","relay_fun:%d port:%d",fundata.relay_fun,fundata.port);
	if(sysflag_judge(fundata , sysflag))
		return 0;
	if(sysflag.group_type == MasterControl_GroupType)
	{
		elog_d("fun0","MasterControl value:%d",fundata.value);
		if(fundata.value == 1)//进入总控1
		{
			if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
				device.data_table.association_table[fundata.associtation_index].object_reg_write(5,relay_read(fundata.port));
			//if(sysflag.night_light_mode == DisNightLight)
			relay_off(fundata.port);
		}
		else if(fundata.value == 2)//进入总控2
		{
			//if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
			//	device.data_table.association_table[fundata.associtation_index].object_reg_write(5,relay_read(fundata.port));
			//if(sysflag.night_light_mode == EnNightLight)
			//	relay_off(fundata.port);
		}
		else if(fundata.value == 0)//退出总控
		{
			//if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
			//{
			//	if(device.data_table.association_table[fundata.associtation_index].object_reg_read(5))
			//		relay_on(fundata.port);
			//	else
			//		relay_off(fundata.port);
			//}
			//else
			//{
			//	if(device.data_table.association_table[fundata.associtation_index].object_reg_read(4))
			//		relay_on(fundata.port);
			//	else
			//		relay_off(fundata.port);
			//}
			relay_on(fundata.port);//受控灯具退出总控强制开启
		}
		device.data_table.association_table[fundata.associtation_index].object_reg_write(6,fundata.value);//总控状态保存
		return 1;
	}
	//	else if(sysflag.group_type == Scene_GroupType)//场景
	//	{
	//		if(fundata.value)
	//		{
	//			if((device.data_table.object_table[fundata.object_index].scene_group != 0xffff)&&(device.data_table.object_table[fundata.object_index].scene_group != device.data_table.address_table[device.data_table.association_table[fundata.associtation_index].group_index_read()].read()))//进入不同场景
	//				exit_scene(device.data_table.object_table[fundata.object_index].scene_group);
	//			fundata.value = device.data_table.association_table[fundata.associtation_index].object_reg_read(4);
	//			device.data_table.object_table[fundata.object_index].scene_group = device.data_table.address_table[device.data_table.association_table[fundata.associtation_index].group_index_read()].read();
	//		}
	//		else//退出场景，不操作
	//		{
	//			device.data_table.object_table[fundata.object_index].scene_group = 0xffff;
	//			return 0;
	//		}
	//	}
	//	else
	//	{
	//		//本组非场景组，检测本组端口有无在其他场景组中有设置
	////		TelegramType telegram_temp;
	//		if(device.data_table.object_table[fundata.object_index].scene_group != 0xffff)
	//		{
	//			exit_scene(device.data_table.object_table[fundata.object_index].scene_group);
	//			device.data_table.object_table[fundata.object_index].scene_group = 0xffff;
	//		}
	//	}
	//	//检测插卡
	//	if(sysflag.card_relation == Vaile_Card)
	//	{
	//		if(device.data_table.systemstate.card_flag == NO_Card)
	//			return 0;
	//	}
	//	else if(sysflag.card_relation == InVaile_Card)
	//	{
	//		if(device.data_table.systemstate.card_flag == Add_Card)
	//			return 0;
	//	}
	//	//白夜模式
	//	if (sysflag.time_mode == DayValid)
	//	{
	//		if(device.data_table.systemstate.time_flag == Time_Night)
	//			return 0;
	//	}
	//	else if(sysflag.time_mode == NightValid)
	//	{
	//		if(device.data_table.systemstate.time_flag == Time_Day)
	//			return 0;
	//	}
	//	
	//	switch (fundata.relay_fun)
	//	{
	//	case R_Switch:
	//		if(fundata.value)
	//			relay_on(fundata.port);
	//		else
	//			relay_off(fundata.port);
	//		break;
	//	case R_DelayOn:
	//		//if(fundata.value)
	//			relay_delay_on(fundata.port,fundata.on_time);
	//		/*else
	//			relay_off(fundata.port);*/
	//		break;
	//	case R_DelayOff:
	//		//if(fundata.value)
	//		/*	relay_on(fundata.port);
	//		else*/
	//			relay_delay_off(fundata.port,fundata.on_time);
	//		break;
	//	case R_Togggle:
	//		relay_toggle(fundata.port);
	//		break;
	//	case R_Blink:
	//
	//		break;
	//	case R_TurnOffDelay:
	//		relay_turnoff_delay(fundata.port,fundata.on_time);
	//		break;
	//	default:
	//		break;
	//	}
	return 0;
}
//通用
uint8_t fun7_Currency(uint8_t *data)
{
	FunDataType fundata;
	SysFlagType sysflag;
	/*DeviceDataType ack_data;
	uint8_t ack_value[8];*/
	uint8_t lock_flag;
	uint8_t lock_port;//窗帘功能默认开互锁使能
	fundata_read( data , fundata);
	sysflag_read( data , sysflag);
	if(sysflag_judge(fundata , sysflag))
		return 0;
	if(sysflag.group_type == MasterControl_GroupType)
	{
		if(fundata.value == 1)//进入总控1
		{
			if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
				device.data_table.association_table[fundata.associtation_index].object_reg_write(5,relay_read(fundata.port));
			if(sysflag.night_light_mode == DisNightLight)
				relay_off(fundata.port);
		}
		else if(fundata.value == 2)//进入总控2
		{
			if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
				device.data_table.association_table[fundata.associtation_index].object_reg_write(5,relay_read(fundata.port));
			if(sysflag.night_light_mode == EnNightLight)
				relay_off(fundata.port);
		}
		else if(fundata.value == 0)//退出总控
		{
			if(sysflag.memory_mode == EnMemory)//有记忆保存端口数值
			{
				if(device.data_table.association_table[fundata.associtation_index].object_reg_read(5))
					relay_on(fundata.port);
				else
					relay_off(fundata.port);
			}
		}
		return 1;
	}
	else if(sysflag.group_type == Scene_GroupType)
	{
		fundata.value = device.data_table.association_table[fundata.associtation_index].object_reg_read(5);
	}
	lock_flag = device.data_table.object_table[fundata.object_index].register_read(2);
	lock_port = device.data_table.object_table[fundata.object_index].register_read(3);

	//relay_off(fundata.port);

	//互锁端口
	if (lock_flag)
	{
		relay_off(lock_port);
	}

	switch (fundata.relay_fun)
	{
	case R_Switch:
		if(fundata.value)
			relay_on(fundata.port);
		else
			relay_off(fundata.port);
		break;
	case R_DelayOn:
		if(fundata.value)
			relay_delay_on(fundata.port,fundata.on_time);
		break;
	case R_DelayOff:
		if(fundata.value)
			relay_delay_off(fundata.port,fundata.off_time);
		break;
	case R_Togggle:
		if(fundata.value)
			relay_toggle(fundata.port);
		break;
	case R_TurnOffDelay:
		if(fundata.value)
			relay_turnoff_delay(fundata.port,fundata.off_time);
		break;
	default:
		break;
	}
	return 1;
}
Object_Funcation_Type  device_fun[OBJECT_FUNCATION_SIZE] = {fun0_Light , fun1_BathroomFan , fun2_BathroomLight , fun3_NightLight , fun4_Curtrain_open ,fun5_Curtrain_close ,fun6_ControlLight, fun7_Currency};
LXCanPhysicalAddrType physical_read()
{
	LXCanPhysicalAddrType addr;
//	addr = physical_dial();
	return addr;
}
struct FANData_Type
{
	//uint16_t assocication_index;
	uint8_t port;
	uint8_t  value;
	uint16_t on_time;
	uint16_t off_time;
	uint64_t count_time;
	static uint8_t count;
}fan_data[FAN_MAX_INDEX];
uint8_t FANData_Type::count = 0;
void fan_init()
{
	LXCanAddress **group_buff;
	uint16_t *assoction_index;
	uint8_t fan_index  = 0;
	FANData_Type::count = 0;
	for (int i = 0; i < device.data_table.Object_total_read(); i++)
	{
		if(device.data_table.object_table[i].register_read(1) == (uint8_t)fun_BathroomFan)
		{
			device.data_table.search_group( i ,group_buff , assoction_index);
			fan_data[fan_index].port = device.data_table.object_table[ i ].register_read(0);//对象寄存器0表示端口号
			uint16_t time_unit;
			switch (device.data_table.association_table[assoction_index[0]].object_reg_read(1))
			{
			case 0://分钟
				time_unit = 60 * 1000;
				break;
			case 1://秒
				time_unit = 1000;
				break;
			case 2://毫秒
				time_unit = 1;
				break;
			default:
				break;
			}
			fan_data[fan_index].on_time = time_unit * device.data_table.association_table[assoction_index[0]].object_reg_read(2);
			fan_data[fan_index].off_time = time_unit * device.data_table.association_table[assoction_index[0]].object_reg_read(3);
			fan_data[fan_index].count_time = millis() + fan_data[fan_index].off_time;
			fan_data[fan_index].value = 0;
			FANData_Type::count++;
			if(fan_index++ >= FAN_MAX_INDEX)
				break;
		}
	}
}
void device_init()
{
	//外设物理端口初始化
	relay_init();
	key_init();
	led_init();
	//读取物理地址
	device.init();
	//对象回调函数初始化
	for (int i = 0; i < OBJECT_SIZE; i++)
	{
		for (int j = 0; j < OBJECT_FUNCATION_SIZE; j++)
		{
			device.object_Callback_init(i,j,device_fun[j]);
		}	
	}
	fan_init();
}
void fan_auto()
{
	if(device.data_table.systemstate.card_flag != Add_Card)
		return;
	for (int i = 0; i < FANData_Type::count; i++)
	{
		if(millis() > fan_data[i].count_time)
		{
			if(fan_data[i].value)
			{
				fan_data[i].value = 0;
				relay_write(fan_data[i].port , 1);
				fan_data[i].count_time = millis() + fan_data[i].off_time;
			}
			else
			{
				fan_data[i].value = 1;
				relay_write(fan_data[i].port , 0);
				fan_data[i].count_time = millis() + fan_data[i].on_time;
			}
		}
	}
}

void device_test()
{
	device.data_table.Object_total_write(13);
	device.data_table.Group_total_write(13);
	device.data_table.Association_total_write(13);
	device.data_table.physical_write(1);

	//对象表13
	for (int i = 0; i <13; i++)
	{
		device.data_table.object_table[i].dpt_write(LXCAN_DPT_1_014);
		device.data_table.object_table[i].config_write(0xdf);
		device.data_table.object_table[i].register_write(0,i);
		device.data_table.object_table[i].register_write(1,(uint8_t)fun_Light);
	}
	
	
	//地址表
	for (int i = 0; i < 13; i++)
	{
		device.data_table.address_table[i].write(i+1);
	}

	//关联表
	for (int i = 0; i < 13; i++)
	{
		device.data_table.association_table[i].object_index_write(i);
		device.data_table.association_table[i].group_index_write(i);
		device.data_table.association_table[i].assocication_reg_write(0,0);
		device.data_table.association_table[i].assocication_reg_write(1,UnCard);
		device.data_table.association_table[i].assocication_reg_write(2,AllDayValid);
		device.data_table.association_table[i].assocication_reg_write(3,DisMemory);
		device.data_table.association_table[i].assocication_reg_write(4,DisNightLight);
		device.data_table.association_table[i].object_reg_write(0,0);
	}
	//device.data_table.object_table[1].register_write(1,2);//对象1 卫生间灯
	//device.data_table.object_table[2].register_write(1,2);//对象2 卫生间灯
	//device.data_table.object_table[3].register_write(1,2);//对象3 卫生间灯
	//device.data_table.object_table[0].register_write(1,1);//对象0 排风扇

	device.data_table.object_table[1].register_write(1,4);//对象1 窗帘
	device.data_table.object_table[1].register_write(2,1);//对象1 互锁使能
	device.data_table.object_table[1].register_write(3,2);//对象1 互锁端口2

	device.data_table.object_table[2].register_write(1,4);//对象1 窗帘
	device.data_table.object_table[2].register_write(2,1);//对象1 互锁使能
	device.data_table.object_table[2].register_write(3,1);//对象1 互锁端口1

	device.data_table.association_table[1].object_reg_write(0,1);
	device.data_table.association_table[1].object_reg_write(1,0x01);
	device.data_table.association_table[1].object_reg_write(2,0xf4);
	device.data_table.association_table[1].object_reg_write(3,0x0b);
	device.data_table.association_table[1].object_reg_write(4,0xb8);

	device.data_table.association_table[2].object_reg_write(0,1);
	device.data_table.association_table[2].object_reg_write(1,0x05);
	device.data_table.association_table[2].object_reg_write(2,0xdc);
	device.data_table.association_table[2].object_reg_write(3,0x0b);
	device.data_table.association_table[2].object_reg_write(4,0xb8);

	device.data_table.backup();
}

void exit_scene(uint16_t group)
{
	TelegramType telegram_temp;
	telegram_temp.CAN_ID = 0;
	telegram_temp._buff_data[0] = 0;
	telegram_temp._repetition_flag = LXCan_DISABLE;
	telegram_temp._ack_flag = LXCan_ENABLE;
	telegram_temp._dest_add = group;
	telegram_temp._type_add = Group_Add;
	telegram_temp._source_add = device.data_table.physical_read();
	telegram_temp._frame_flag = Frame_None;
	telegram_temp._service = LXCan_COMMAND_VALUE_WRITE;
	telegram_temp._value[0] = 0;
	telegram_temp.length = 2;//发送数据长度 = 服务字节 + 对象值长度
	device._port.txActionList.Append(telegram_temp);
}

