#include "led.h"
GPIO LED(GPIOB,GPIO_Pin_7);
uint64_t led_time;
e_LedMode led_mode;
const uint16_t led_mode_time[4][2] = {500 , 500 ,
									  800 , 200 , 
									  200 , 800 ,
									  50 ,  50 ,
									 
};
void led_init()
{
	LED.mode(OUTPUT_OD);
	led_time = millis();
	led_mode = Heartbeat_Led;
}
void led_task()
{
	uint8_t index = 0;
	if(LED.read())
		index = 1;
	else
		index = 0;
	if((millis()-led_time) > led_mode_time[led_mode][index])
	{
		led_time = millis();
		LED.toggle();
	}
}
void led_mode_set(e_LedMode mode)
{
	led_mode = mode;
}

