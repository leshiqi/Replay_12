#ifndef __KEY_H_
#define __KEY_H_
#include "LX_OS.h"

#define  KEY_SHORT_TIME 20
#define  KEY_LONE_TIME  2000
#define  KEYNUM 14
enum e_KeyState
{
	KeyNo,
	KeyFall,
	KeyClick,
	KeyShort,
	KeyLong,
	KeyRise
};
typedef struct  
{
	e_KeyState state;
	e_KeyState action;
	uint16_t time;
	
}KeyData_type;
void key_init();
void key_task();
e_KeyState key_read(uint8_t index);
e_KeyState set_key_read();
#endif



