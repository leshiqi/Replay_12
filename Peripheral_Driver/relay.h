#ifndef __RELAY_H
#define __RELAY_H
#include "LX_OS.h"

enum e_Relay_fun{
	R_Switch,
	R_DelayOn,
	R_DelayOff,
	R_Togggle,
	R_Blink,
	R_TurnOffDelay
};
#define  RELAYNUM  13
void relay_init();
void relay_task();
void relay_write(uint8_t index,bool val);
void relay_on(uint8_t index);
void relay_off(uint8_t index);
void relay_toggle(uint8_t index);
void relay_delay_on(uint8_t index,uint16_t time);
void relay_delay_off(uint8_t index,uint16_t time);
void relay_turnoff_delay(uint8_t index,uint16_t offtime);
uint8_t relay_read(uint8_t index);

#endif

