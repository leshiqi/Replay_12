#ifndef __DEVICE_H
#define __DEVICE_H
#include "LXCanDevice.h"
#include "relay.h"
#include "key.h"
#include "led.h"
void device_callback();
void device_init();
void fan_auto();
#define FAN_MAX_INDEX   5
enum e_Device_fun_ObjectReg1{
	fun_Light,
	fun_BathroomFan,
	fun_BathroomLight,
	fun_NightLight,
	fun_Curtrain,
	fun_Other
};
typedef struct  
{
	uint8_t object_index;
	uint8_t associtation_index;
	uint8_t value;
	uint8_t port;
	uint32_t on_time;
	uint32_t off_time;
	e_Relay_fun relay_fun;
}FunDataType;
typedef struct 
{
	e_GroupType_AssocicationReg0 group_type;
	uint8_t card_relation;
	uint8_t time_mode;
	uint8_t memory_mode;
	uint8_t night_light_mode;
}SysFlagType;
void device_test();
extern LXCanDevice device;
#endif

