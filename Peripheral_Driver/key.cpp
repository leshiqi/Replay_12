#include "key.h"
#include "relay.h"

GPIO x_port[] = {PA8 , PA11 , PA12 , PA15};
GPIO y_port[] = {PB12 , PB13 , PB14 , PB15};
static KeyData_type key_data[KEYNUM];
void key_init()
{
	for (int i = 0; i < 4; i++)
	{
		x_port[i].mode(OUTPUT_OD);
		y_port[i].mode(INPUT_PU);	

		x_port[i].set();
	}
	for (int i = 0; i < KEYNUM; i++)
	{
		key_data[i].state = KeyNo;
		key_data[i].action = KeyNo;
		key_data[i].time = 0;
	}
}
uint16_t  key_value()
{
	uint16_t val = 0;
	const uint16_t mask[] = {0x0001 , 0x0010, 0x0100 , 0x1000 , 0x0002 , 0x0020 , 0x0200, 0x2000 ,0x0004, 0x0040, 0x0400, 0x4000 , 0x0008 , 0x0080 , 0x0800 , 0x8000};
	for (int i = 0; i < 4; i++)
	{
		x_port[i].reset();
		for (int j = 0; j < 4; j++)
		{
			if( !y_port[j].read())//低电平有效
				val &= mask[i*4 +j];
		}
		x_port[i].set();
	}
	return val;
}
uint8_t key_value(uint8_t index)
{
	uint8_t val;
	x_port[index%4].reset();
	val = !y_port[index/4].read();//低电平有效
	x_port[index%4].set();
	return val;
}
e_KeyState key_scan(uint8_t index , uint8_t val)
{
	switch(key_data[index].state)
	{
		/*无按键状态*/
	case KeyNo:
		{
			if(val)
			{
				key_data[index].state = KeyFall;
				key_data[index].time = 0;
			}
			else
			{
				key_data[index].state = KeyNo;
			}
			
			break;
		}
		/*消抖状态*/
	case KeyFall:
		{
			if(val)
			{
				if(++key_data[index].time > KEY_SHORT_TIME)
				{
					key_data[index].state = KeyClick;
					key_data[index].action = KeyClick;
				}
			}
			else
			{
				key_data[index].state = KeyNo;
				key_data[index].time = 0;
			}
			break;
		}
		/*单击状态*/
	case KeyClick:
		{
			if(val)
			{
				if(++key_data[index].time > KEY_LONE_TIME)
				{
					key_data[index].state = KeyLong;
					key_data[index].action = KeyLong;
				}
			}
			else
			{
				key_data[index].state = KeyRise;
				key_data[index].action = KeyShort;//短按
			}
			break;
		}
		/*长按状态*/
	case KeyLong:
		{
			if(!val)
			{
				key_data[index].state = KeyRise;
				key_data[index].action = KeyRise;
			}
			break;
		}
		/*抬起状态*/
	case KeyRise:
		{
			key_data[index].state = KeyNo;
			key_data[index].time = 0;
			break;
		}
	default: break;
	}
	return key_data[index].state;
}
//1ms周期执行
void key_task()
{
	for (int i = 0; i < 14; i++)
	{
		key_scan(i,key_value(i));
	}
}
e_KeyState key_read(uint8_t index)
{
	e_KeyState retuen_val;
	retuen_val = key_data[index].action;
	key_data[index].action = KeyNo;
	return retuen_val;
}
e_KeyState set_key_read()
{
  return key_read(13);
}

