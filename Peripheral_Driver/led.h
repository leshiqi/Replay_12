#ifndef  __LED_H_
#define  __LED_H_
#include "LX_OS.h"
enum e_LedMode{
	Heartbeat_Led,
	Debug_Led,
	PhysicalAdd_Led ,
	Download_Led
	
};
void led_init();
void led_task();
void led_mode_set(e_LedMode mode);

#endif


